/*
 * Copyright (C) 2013 Willem Mulder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.classicloader;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.message.MessageFormatMessageFactory;

import net.canarymod.Canary;
import net.canarymod.commandsys.CommandDependencyException;
import net.canarymod.plugin.Plugin;
import net.visualillusionsent.utils.PropertiesFile;

import com.minecraftonline.classicloader.classicapi.etc;
import com.minecraftonline.classicloader.classicapi.bridge.EventToHookAdapter;

public class ClassicLoader extends Plugin {

    // Use our own log handling for now, until Logman improves
    private static final Logger PLUGIN_LOG = LogManager.getLogger(ClassicLoader.class.getPackage().getName(),
                                                                  new MessageFormatMessageFactory());
    private static ClassicLoader instance;
    private final PropertiesFile properties = getConfig();
    public final boolean LOAD_NMS = properties.getBoolean("enable-notchcode", false);
    public final boolean DEBUG = properties.getBoolean("debug", false);
    public final boolean DUMP_CLASSES = properties.getBoolean("dump-classes", false);

    {
        instance = this;
    }

    @Override
    public boolean enable() {
        try {
            Canary.commands().registerCommands(new Commands(), this, false);
        } catch (CommandDependencyException ex) {
            PLUGIN_LOG.error("Unexpected exception", ex);
            return false;
        }

        PLUGIN_LOG.debug("Initializing Classic systems...");
        etc.getInstance().loadData();

        PLUGIN_LOG.debug("Registering hooks...");
        Canary.hooks().registerListener(new PluginLoadListener(), this);
        Canary.hooks().registerListener(new ClassicCommandListener(), this);
        Canary.hooks().registerListener(new EventToHookAdapter(etc.getLoader()), this);

        etc.getLoader().loadPreloadPlugins();
        return true;
    }

    @Override
    public void disable() {
        etc.getLoader().unloadPlugins();
    }

    public static Logger getPluginLogger() {
        return PLUGIN_LOG;
    }

    public static ClassicLoader getInstance() {
        return instance;
    }
}
