package com.minecraftonline.classicloader.classicapi;

import net.canarymod.Canary;
import net.canarymod.api.world.CanaryChunk;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/**
 * Chunk.java - Interface to chunks, especially for generating chunks.
 *
 * @author phi
 */
@RecodeProxy(net.canarymod.api.world.Chunk.class)
public class Chunk {

    public final net.canarymod.api.world.Chunk chunk;

    /**
     * Instantiated this wrapper around OChunk
     *
     * @param chunk the OChunk to wrap
     */
    public Chunk(net.minecraft.world.chunk.Chunk chunk) {
        this.chunk = chunk.getCanaryChunk();
    }

    public Chunk(net.canarymod.api.world.Chunk chunk) {
        this.chunk = chunk;
    }

    /**
     * Generates a new chunk for the specified world. This does not register the
     * generated chunk at the world.
     *
     * @param world
     * @param x
     * @param z
     * @return new chunk
     */
    public static Chunk getNewChunk(net.minecraft.world.World world, int x, int z) {
        return Wrapper.wrap(Canary.factory().getObjectFactory().newChunk(world.getCanaryWorld(), x, z));
    }

    /**
     * Generates a new chunk for the specified world. This does not register the
     * generated chunk at the world.
     *
     * @param world
     * @param blocks
     * @param x
     * @param z
     * @return new chunk
     */
    public static Chunk getNewChunk(net.minecraft.world.World world, byte[] blocks, int x, int z) {
        net.minecraft.block.Block[] blockTypes = new net.minecraft.block.Block[blocks.length];
        for (int i = 0; i < blocks.length; i++) {
            blockTypes[i] = net.minecraft.block.Block.e(blocks[i]);
        }
        return Wrapper.wrap(new net.minecraft.world.chunk.Chunk(world, blockTypes, x, z).getCanaryChunk());
    }

    /**
     * Regenerates the world according to the world seed.
     *
     * @param world
     * @param x
     * @param z
     * @return new chunk
     */
    public static Chunk regenerateChunk(net.minecraft.world.World world, int x, int z) {
        return Wrapper.wrap(world.getCanaryWorld().getChunkProvider().regenerateChunk(x, z));
    }

    /**
     * Returns whether this chunk is loaded
     *
     * @return true if chunk is loaded
     */
    public boolean isLoaded() {
        return chunk.isLoaded();
    }

    /**
     * Returns this chunks's world
     *
     * @return world
     */
    public World getWorld() {
        return Wrapper.wrap(chunk.getDimension());
    }

    /**
     * Gets the x location of the chunk.
     *
     * @return x
     */
    public int getX() {
        return chunk.getX();
    }

    /**
     * Gets the z location of the chunk.
     *
     * @return z
     */
    public int getZ() {
        return chunk.getZ();
    }

    /**
     * Sets the block type at the specified location
     *
     * @param x
     * @param y
     * @param z
     * @param id block type
     * @return true if successful
     */
    public boolean setBlockIdAt(int x, int y, int z, int id) {
        chunk.setBlockTypeAt(x, y, z, id);
        return true;
    }

    /**
     * Returns the block type at the specified location
     *
     * @param x
     * @param y
     * @param z
     * @return block type
     */
    public int getBlockIdAt(int x, int y, int z) {
        return chunk.getBlockTypeAt(x, y, z);
    }

    /**
     * Sets the block data at the specified location
     *
     * @param x
     * @param y
     * @param z
     * @param data block data
     */
    public void setBlockDataAt(int x, int y, int z, int data) {
        chunk.setBlockDataAt(x, y, z, data);
    }

    /**
     * Returns the block data at the specified location
     *
     * @param x
     * @param y
     * @param z
     * @return block data
     */
    public int getBlockDataAt(int x, int y, int z) {
        return chunk.getBlockDataAt(x, y, z);
    }

    /**
     * Gets the chunk's biome data byte array
     *
     * @return biomedata
     */
    public byte[] getBiomeData() {
        return chunk.getBiomeByteData();
    }

    /**
     * Sets the chunk's biome data (needs to be byte[256])
     *
     * @param biomedata
     */
    public void setBiomeData(byte[] biomedata) {
        chunk.setBiomeData(biomedata);
    }

    /**
     * resends chunk data to clients
     */
    public void update() {
        Canary.getServer().getConfigurationManager().sendPacketToAllInWorld(getWorld().getRecodeHandle().getFqName(),
                Canary.factory().getPacketFactory().chunkData(chunk, true, 0));
    }

    /**
     * gets the wrapped chunk
     *
     * @return chunk
     */
    public net.minecraft.world.chunk.Chunk getChunk() {
        return ((CanaryChunk) chunk).getHandle();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Chunk) {
            Chunk other = (Chunk) obj;
            return getX() == other.getX() && getZ() == other.getZ();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return (getX() & 0xffff) + ((getZ() & 0xffff) << 16);
    }

    @Override
    public String toString() {
        return String.format("Chunk[x=%d, z=%d]", getX(), getZ());
    }

    public net.canarymod.api.world.Chunk getRecodeHandle() {
        return chunk;
    }
}
