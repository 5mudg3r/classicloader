package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.world.blocks.CanaryWorkbench;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import com.minecraftonline.classicloader.classicapi.bridge.ContainerProxy;
import net.minecraft.inventory.ContainerWorkbench;

@RecodeProxy(net.canarymod.api.world.blocks.Workbench.class)
public class Workbench extends ItemArray<ContainerProxy> {

    private final net.canarymod.api.world.blocks.Workbench workbench;

    public Workbench(ContainerWorkbench block) {
        super(block, ContainerProxy.of(block.getInventory()));
        workbench = (net.canarymod.api.world.blocks.Workbench) block.getInventory();
    }

    public Workbench(net.canarymod.api.world.blocks.Workbench bench) {
        super(((CanaryWorkbench) bench).getContainer(), ContainerProxy.of(bench));
        workbench = bench;
    }

    @Override
    public void update() {
        workbench.update();
    }

    @Deprecated
    public void update(Player player) {
        this.update(); // We have an update method now!
    }

    @Override
    public String getName() {
        return container.getName();
    }

    @Override
    public void setName(String value) {
        container.setName(value);
    }

    public net.canarymod.api.world.blocks.Workbench getRecodeHandle() {
        return workbench;
    }
}
