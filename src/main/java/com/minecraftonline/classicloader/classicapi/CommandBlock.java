package com.minecraftonline.classicloader.classicapi;

import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.tileentity.TileEntityCommandBlock;

/**
 * Interface for the OTileEntityCommandBlock class
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(net.canarymod.api.world.blocks.CommandBlock.class)
public class CommandBlock extends ComplexBlockBase<TileEntityCommandBlock> implements MessageReceiver {

    public CommandBlock(TileEntityCommandBlock base) {
        super(base);
    }

    public CommandBlock(net.canarymod.api.world.blocks.CommandBlock commandBlock) {
        super(commandBlock);
    }

    /**
     * Sets this command block's command
     *
     * @param command the command to execute when this block is activated
     */
    public void setCommand(String command) {
        getRecodeHandle().setCommand(command);
    }

    /**
     * Returns this command block's command
     *
     * @return
     */
    public String getCommand() {
        return getRecodeHandle().getCommand();
    }

    /**
     * Run this command block's command
     */
    public void runCommand() {
        getRecodeHandle().activate();
    }

    /**
     * Sets the text that appears before a command block's command in chat.
     * Default is '
     *
     * @'
     *
     * @param prefix
     */
    public void setPrefix(String prefix) {
        getRecodeHandle().setName(prefix);
    }

    /**
     * Returns the text that appears before a command block's command in chat
     * Default is '
     *
     * @'
     *
     * @return
     */
    public String getPrefix() {
        return getRecodeHandle().getName();
    }

    @Override
    public String getName() {
        return getRecodeHandle().getName();
    }

    @Override
    public void notify(String message) {
        getRecodeHandle().notice(message);
    }

    @Override
    public net.canarymod.api.world.blocks.CommandBlock getRecodeHandle() {
        return (net.canarymod.api.world.blocks.CommandBlock) super.getRecodeHandle();
    }
}
