package com.minecraftonline.classicloader.classicapi;

import java.util.Collection;
import java.util.HashSet;
import net.canarymod.Canary;
import net.canarymod.api.nbt.BaseTag;
import net.canarymod.api.nbt.CompoundTag;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/**
 * Interface for the net.minecraft.nbt.NBTTagCompound class
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(CompoundTag.class)
public class NBTTagCompound extends NBTBase {

    /**
     * Creates a wrapper around an net.minecraft.nbt.NBTTagCompound
     *
     * @param baseTag
     */
    public NBTTagCompound(net.minecraft.nbt.NBTTagCompound baseTag) {
        super(baseTag);
    }

    /**
     * Creates a new NBTTagCompound
     */
    public NBTTagCompound() {
        this(Canary.factory().getNBTFactory().newCompoundTag(""));
    }

    /**
     * Creates a new NBTTagCompound
     *
     * @param name the name of the tag
     */
    public NBTTagCompound(String name) {
        this(Canary.factory().getNBTFactory().newCompoundTag(name));
    }

    public NBTTagCompound(CompoundTag compoundTag) {
        super(compoundTag);
    }

    @Override
    public net.minecraft.nbt.NBTTagCompound getBaseTag() {
        return (net.minecraft.nbt.NBTTagCompound) super.getBaseTag();
    }

    /**
     * Returns the tags in this CompoundTag.
     *
     * @return
     */
    public Collection<NBTBase> getValues() {
        Collection<BaseTag> recodeValues = getRecodeHandle().values();
        Collection<NBTBase> classicValues = new HashSet<NBTBase>(recodeValues.size());
        for (BaseTag recodeValue : recodeValues) {
            classicValues.add(new NBTBase(recodeValue));
        }
        return classicValues;
    }

    /**
     * Returns a tag within this CompoundTag
     *
     * @param name the name of the tag to return
     * @return
     */
    public NBTBase getValue(String name) {
        return new NBTBase(getRecodeHandle().get(name));
    }

    /**
     * Returns whether or not this CompoundTag has a tag in it
     *
     * @param name the name of the tag to check
     * @return
     */
    public boolean hasTag(String name) {
        return getRecodeHandle().containsKey(name);
    }

    /**
     * Adds an NBTTag
     *
     * @param name the name of the tag
     * @param tag the tag to add
     */
    public void add(String name, NBTBase tag) {
        getRecodeHandle().put(name, tag.getRecodeHandle());
    }

    /**
     * Adds a new NBTTagByte
     *
     * @param name the name of the NBTByteTag
     * @param b the value of the NBTByteTag
     */
    public void add(String name, byte b) {
        getRecodeHandle().put(name, b);
    }

    /**
     * Adds a new NBTTagShort
     *
     * @param name the name of the NBTTagShort
     * @param s the value of the NBTTagShort
     */
    public void add(String name, short s) {
        getRecodeHandle().put(name, s);
    }

    /**
     * Adds a new NBTTagInt
     *
     * @param name the name of the new NBTTagInt
     * @param i the value of the NBTTagInt
     */
    public void add(String name, int i) {
        getRecodeHandle().put(name, i);
    }

    /**
     * Adds a new NBTTagLong
     *
     * @param name the name of the new NBTTagLong
     * @param l the value of the new NBTTagLong
     */
    public void add(String name, long l) {
        getRecodeHandle().put(name, l);
    }

    /**
     * Adds a new NBTTagFloat
     *
     * @param name the name of the new NBTTagFloat
     * @param f the value of the new NBTTagFloat
     */
    public void add(String name, float f) {
        getRecodeHandle().put(name, f);
    }

    /**
     * Adds a new NBTTagDouble
     *
     * @param name the name of the new NBTTagDouble
     * @param d the value of the new NBTTagDouble
     */
    public void add(String name, double d) {
        getRecodeHandle().put(name, d);
    }

    /**
     * Adds a new NBTTagString
     *
     * @param name the name of the new NBTTagString
     * @param s the value of the new NBTTagString
     */
    public void add(String name, String s) {
        getRecodeHandle().put(name, s);
    }

    /**
     * Adds a new NBTTagByteArray
     *
     * @param name the name of the new NBTTagByteArray
     * @param b the value of the new NBTTagByteArray
     */
    public void add(String name, byte[] b) {
        getRecodeHandle().put(name, b);
    }

    /**
     * Adds a new NBTTagIntArray
     *
     * @param name the name of the new NBTTagIntArray
     * @param i the value of the new NBTTagIntArray
     */
    public void add(String name, int[] i) {
        getRecodeHandle().put(name, i);
    }

    /**
     * Adds a new NBTTagInt with a value of either 0 or 1
     *
     * @param name the name of the new NBTTagInt
     * @param b determines the value of the new NBTTagInt (true=1, false=0)
     */
    public void add(String name, boolean b) {
        getRecodeHandle().put(name, b);
    }

    /**
     * Returns the byte value of a tag
     *
     * @param tagName the name of the tag to get the value of
     * @return
     */
    public byte getByte(String tagName) {
        return getRecodeHandle().getByte(tagName);
    }

    /**
     * Returns the short value of a tag
     *
     * @param tagName the name of the tag to get the value of
     * @return
     */
    public short getShort(String tagName) {
        return getRecodeHandle().getShort(tagName);
    }

    /**
     * Returns the int value of a tag
     *
     * @param tagName the name of the tag to get the value of
     * @return
     */
    public int getInt(String tagName) {
        return getRecodeHandle().getInt(tagName);
    }

    /**
     * Returns the long value of a tag
     *
     * @param tagName the name of the tag to get the value of
     * @return
     */
    public long getLong(String tagName) {
        return getRecodeHandle().getLong(tagName);
    }

    /**
     * Returns the float value of a tag
     *
     * @param tagName the name of the tag to get the value of
     * @return
     */
    public float getFloat(String tagName) {
        return getRecodeHandle().getFloat(tagName);
    }

    /**
     * Returns the double value of a tag
     *
     * @param tagName the name of the tag to get the value of
     * @return
     */
    public double getDouble(String tagName) {
        return getRecodeHandle().getDouble(tagName);
    }

    /**
     * Returns the String value of a tag
     *
     * @param tagName the name of the tag to get the value of
     * @return
     */
    public String getString(String tagName) {
        return getRecodeHandle().getString(tagName);
    }

    /**
     * Returns the byte array value of a tag
     *
     * @param tagName the name of the tag to get the value of
     * @return
     */
    public byte[] getByteArray(String tagName) {
        return getRecodeHandle().getByteArray(tagName);
    }

    /**
     * Returns the int array value of a tag
     *
     * @param tagName the name of the tag to get the value of
     * @return
     */
    public int[] getIntArray(String tagName) {
        return getRecodeHandle().getIntArray(tagName);
    }

    /**
     * Returns an NBTTagCompound within this NBTTagCompound
     *
     * @param tagName the name of the tag to get the value of
     * @return
     */
    public NBTTagCompound getNBTTagCompound(String tagName) {
        return new NBTTagCompound(getRecodeHandle().getCompoundTag(tagName));
    }

    public NBTTagList getNBTTagList(String tagName) {
        return new NBTTagList(getRecodeHandle().getListTag(tagName));
    }

    /**
     * Returns whether or not the tag specified is 0 (0=false, !0=true)
     *
     * @param tagName
     * @return
     */
    public boolean getBoolean(String tagName) {
        return getRecodeHandle().getBoolean(tagName);
    }

    /**
     * Removes a tag from this NBTTagCompound
     *
     * @param name the name of the tag to remove
     */
    public void removeTag(String name) {
        getRecodeHandle().remove(name);
    }

    /**
     * Returns whether or not this NBTTagCompound has anything in it
     *
     * @return
     */
    public boolean isEmpty() {
        return getRecodeHandle().isEmpty();
    }

    @Override
    public String toString() {
        return String.format("NBTTag[type=%s, name=%s, value=%s]", getTagName(getType()), getName(), getValues());
    }

    @Override
    public CompoundTag getRecodeHandle() {
        return (CompoundTag) super.getRecodeHandle();
    }

    public static NBTTagCompound[] fromRecodeArray(CompoundTag[] recode) {
        NBTTagCompound[] classic = new NBTTagCompound[recode.length];
        for (int i = 0; i < recode.length; i++) {
            classic[i] = new NBTTagCompound(recode[i]);
        }
        return classic;
    }

    public static CompoundTag[] toRecodeArray(NBTTagCompound[] classic) {
        CompoundTag[] recode = new CompoundTag[classic.length];
        for (int i = 0; i < classic.length; i++) {
            recode[i] = classic[i].getRecodeHandle();
        }
        return recode;
    }
}
