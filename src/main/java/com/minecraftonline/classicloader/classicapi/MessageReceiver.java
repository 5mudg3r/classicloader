package com.minecraftonline.classicloader.classicapi;

public interface MessageReceiver {

    String getName();

    void notify(String message);
}
