package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.world.blocks.Jukebox;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.block.BlockJukebox.TileEntityJukebox;

/**
 * JukeBox/RecordPlayer interface
 *
 * @author Drathus42
 */
@RecodeProxy(Jukebox.class)
public class JukeBox extends ComplexBlockBase<TileEntityJukebox> {

    public JukeBox(TileEntityJukebox jukebox) {
        super(jukebox);
    }

    public JukeBox(Jukebox jukebox) {
        super(jukebox);
    }

    /**
     * Check if a record is present in the JukeBox
     *
     * @return true if record present, false if no record present
     */
    public boolean hasRecord() {
        return getRecodeHandle().getDisc() != null;
    }

    /**
     * Get the ID of the record in the JukeBox (if any)
     *
     * @return Item ID number of record or -1 if no record present
     */
    public int getDiscID() {
        return hasRecord() ? getDisc().getItemId() : -1;
    }

    /**
     * Get the item currently in the tileEntity (if any)
     *
     * @return The record <tt>Item</tt> or null if no record present
     */
    public Item getDisc() {
        net.canarymod.api.inventory.Item disc = getRecodeHandle().getDisc();
        return disc == null ? null : Wrapper.wrap(disc);
    }

    /**
     * Sets the item currently in the tileEntity.
     *
     * @param item The {@link Item} to be in the tileEntity, or <tt>null</yy> to
     * remove any item.
     */
    public void setDisc(Item item) {
        getRecodeHandle().setDisc(item == null ? null : item.getRecodeHandle());
    }

    @Override
    public Jukebox getRecodeHandle() {
        return (Jukebox) super.getRecodeHandle();
    }
}
