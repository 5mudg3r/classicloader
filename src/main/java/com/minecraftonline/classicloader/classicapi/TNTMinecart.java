package com.minecraftonline.classicloader.classicapi;

import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.item.EntityMinecartTNT;

@RecodeProxy(net.canarymod.api.entity.vehicle.TNTMinecart.class)
public class TNTMinecart extends Minecart {

    TNTMinecart(EntityMinecartTNT o) {
        super(o);
    }

    /**
     * Create a new Minecart with the given position. Call {@link #spawn()} to
     * spawn it in the world.
     *
     * @param world The world for the new minecart
     * @param x The x coordinate for the new minecart
     * @param y The y coordinate for the new minecart
     * @param z The z coordinate for the new minecart
     */
    public TNTMinecart(World world, double x, double y, double z) {
        this(new EntityMinecartTNT(world.getWorld(), x, y, z));
    }

    public TNTMinecart(net.canarymod.api.entity.vehicle.TNTMinecart cart) {
        super(cart);
    }

    @Override
    public EntityMinecartTNT getEntity() {
        return (EntityMinecartTNT) super.getEntity();
    }

    /**
     * Activates the TNT as if the cart passed over an activator rail.
     */
    public void activate() {
        // SRG this.getEntity().func_94105_c();
        this.getEntity().e();
    }

    /**
     * Immediately explodes the minecart.
     * <tt>power</tt> is normally calculated by squaring the cart's speed.
     *
     * @param power The power at which the cart should explode, squared
     */
    public void explode(double power) {
        getRecodeHandle().setPower((float) power);
        getRecodeHandle().detonate();
    }

    /**
     * Get the fuse time for this minecart's TNT. Starts at 80 by default, is -1
     * if not yet activated.
     *
     * @return The fuse time in ticks for the TNT if lit, -1 otherwise.
     */
    public int getFuseTime() {
        return getRecodeHandle().getFuse();
    }

    /**
     * Set the fuse time for this minecart's TNT. Starts at 80 by default.
     * Setting this to anything greater than -1 activates the fuse at the set
     * time, setting it to -1 extinguishes it.
     *
     * @param time The fuse time in ticks.
     */
    public void setFuseTime(int time) {
        getRecodeHandle().setFuse(time);
    }

    /**
     * Checks whether the fuse is lit. Equivalent to <tt>{@link #getFuseTime()}
     * > -1</tt>.
     *
     * @return <tt>true</tt> if the fuse is lit, <tt>false</tt> otherwise.
     */
    public boolean isFuseLit() {
        return getRecodeHandle().getFuse() > -1;
    }

    @Override
    public net.canarymod.api.entity.vehicle.TNTMinecart getRecodeHandle() {
        return (net.canarymod.api.entity.vehicle.TNTMinecart) super.getRecodeHandle();
    }
}
