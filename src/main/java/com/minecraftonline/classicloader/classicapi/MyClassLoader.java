package com.minecraftonline.classicloader.classicapi;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.CodeSource;
import java.security.cert.Certificate;
import java.util.Map;
import java.util.WeakHashMap;

import com.google.common.io.ByteStreams;

import org.apache.logging.log4j.message.MessageFormatMessageFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.minecraftonline.classicloader.asm.PluginClassTransformer;

/**
 * Class loader used so we can dynamically load classes. Normal class loader
 * doesn't close the .jar so you can't reload. This fixes that.
 *
 * @author James
 */
public class MyClassLoader extends URLClassLoader {

    private static final Map<MyClassLoader, Object> instances = new WeakHashMap<MyClassLoader, Object>();
    private static final Object PRESENT = new Object();
    private static final Logger log = LogManager.getLogger(new MessageFormatMessageFactory());
    private final URL baseURL;

    /**
     * Creates loader
     *
     * @param urls
     * @param loader
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public MyClassLoader(URL[] urls, ClassLoader loader) {
        super(urls, loader);
        baseURL = urls.length > 0 ? urls[0] : null;
        instances.put(this, PRESENT);
    }

    /**
     * Fix here.
     */
    public void close() {
        // If we run on Java 7 or 8, just call URLClassLoader.close()
        if (System.getProperty("java.version").startsWith("1.7") || System.getProperty("java.version").startsWith("1.8")) {
            try {
                URLClassLoader.class.getDeclaredMethod("close").invoke(this);
                return;
            } catch (Exception ex) {
                // Probably IOException, ignore.
            }
        }
        try {
            Class<?> clazz = java.net.URLClassLoader.class;
            java.lang.reflect.Field ucp = clazz.getDeclaredField("ucp");

            ucp.setAccessible(true);
            Object sun_misc_URLClassPath = ucp.get(this);
            java.lang.reflect.Field loaders = sun_misc_URLClassPath.getClass().getDeclaredField("loaders");

            loaders.setAccessible(true);
            Object java_util_Collection = loaders.get(sun_misc_URLClassPath);

            for (Object sun_misc_URLClassPath_JarLoader : ((java.util.Collection) java_util_Collection).toArray()) {
                try {
                    java.lang.reflect.Field loader = sun_misc_URLClassPath_JarLoader.getClass().getDeclaredField("jar");

                    loader.setAccessible(true);
                    Object java_util_jar_JarFile = loader.get(sun_misc_URLClassPath_JarLoader);

                    ((java.util.jar.JarFile) java_util_jar_JarFile).close();
                    if (instances.containsKey(this)) {
                        instances.remove(this);
                    }
                } catch (Throwable t) {
                    // if we got this far, this is probably not a JAR loader so
                    // skip it
                }
            }
        } catch (Throwable t) {
            // probably not a SUN VM
        }
    }

    /**
     * {@inheritDoc}
     *
     * This method is overridden to check other plugin class loaders as well.
     */
    @Override
    public Class<?> findClass(String name) throws ClassNotFoundException {
        Class<?> clazz = this.tryFindClass(name);
        if (clazz != null) {
            return clazz;
        }

        for (MyClassLoader cl : instances.keySet()) {
            clazz = cl.tryFindClass(name);
            if (clazz != null) {
                return clazz;
            }
        }

        throw new ClassNotFoundException(name);
    }

    /*
     * (no doc) The actual finding of the class in the current classloader
     */
    private Class<?> tryFindClass(String name) {
        String path = name.replace('.', '/').concat(".class");
        InputStream stream = getResourceAsStream(path);
        if (stream != null) {
            try {
                byte[] classBytes = ByteStreams.toByteArray(stream);
                classBytes = PluginClassTransformer.transform(classBytes, this, path);
                return this.defineClass(name, classBytes, 0, classBytes.length, new CodeSource(baseURL, (Certificate[]) null));
            } catch (IOException e) {
                log.warn("Exception while trying to load " + name, e);
            }
        }
        return null;
    }

    @Override
    public void addURL(URL url) {
        super.addURL(url);
    }
}
