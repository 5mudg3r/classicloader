package com.minecraftonline.classicloader.classicapi;

import net.canarymod.Canary;
import net.canarymod.api.nbt.ShortTag;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/**
 * Interface for the net.minecraft.server.NBTTagShort class
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(ShortTag.class)
public class NBTTagShort extends NBTBase {

    /**
     * Wraps an net.minecraft.server.NBTTagShort
     *
     * @param baseTag the tag to wrap
     */
    public NBTTagShort(net.minecraft.nbt.NBTTagShort baseTag) {
        super(baseTag);
    }

    /**
     * Creates a new NBTTagShort
     *
     * @param name the name of the tag
     */
    public NBTTagShort(String name) {
        this(Canary.factory().getNBTFactory().newShortTag((short) 0));
    }

    /**
     * Creates a new NBTTagShort
     *
     * @param name the name of the tag
     * @param value the value of the tag
     */
    public NBTTagShort(String name, short value) {
        this(Canary.factory().getNBTFactory().newShortTag(value));
    }

    public NBTTagShort(ShortTag shortTag) {
        super(shortTag);
    }

    @Override
    public net.minecraft.nbt.NBTTagShort getBaseTag() {
        return (net.minecraft.nbt.NBTTagShort) super.getBaseTag();
    }

    /**
     * Sets the value of this tag
     *
     * @return
     */
    public short getValue() {
        return getRecodeHandle().getValue();
    }

    /**
     * Sets the value of this tag
     *
     * @param value the new value
     */
    public void setValue(short value) {
        getRecodeHandle().setValue(value);
    }

    @Override
    public String toString() {
        return String.format("NBTTag[type=%s, name=%s, value=%d]", getTagName(getType()), getName(), getValue());
    }

    @Override
    public ShortTag getRecodeHandle() {
        return (ShortTag) super.getRecodeHandle();
    }

    public NBTTagShort[] fromRecodeArray(ShortTag[] recode) {
        NBTTagShort[] classic = new NBTTagShort[recode.length];
        for (int i = 0; i < recode.length; i++) {
            classic[i] = new NBTTagShort(recode[i]);
        }
        return classic;
    }

    public ShortTag[] toRecodeArray(NBTTagShort[] classic) {
        ShortTag[] recode = new ShortTag[classic.length];
        for (int i = 0; i < classic.length; i++) {
            recode[i] = classic[i].getRecodeHandle();
        }
        return recode;
    }
}
