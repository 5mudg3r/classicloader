package com.minecraftonline.classicloader.classicapi;

import net.canarymod.Canary;
import net.canarymod.api.nbt.IntTag;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/**
 * Interface for the net.minecraft.server.NBTTagInt class
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(IntTag.class)
public class NBTTagInt extends NBTBase {

    /**
     * Wraps an net.minecraft.server.NBTTagInt
     *
     * @param baseTag the tag to wrap
     */
    public NBTTagInt(net.minecraft.nbt.NBTTagInt baseTag) {
        super(baseTag);
    }

    /**
     * Creates a new NBTTagInt
     *
     * @param name the name of the tag
     */
    public NBTTagInt(String name) {
        this(name, 0);
    }

    /**
     * Creates a new NBTTagInt
     *
     * @param name the name of the tag
     * @param value the value of the tag
     */
    public NBTTagInt(String name, int value) {
        this(Canary.factory().getNBTFactory().newIntTag(value));
    }

    public NBTTagInt(IntTag intTag) {
        super(intTag);
    }

    @Override
    public net.minecraft.nbt.NBTTagInt getBaseTag() {
        return (net.minecraft.nbt.NBTTagInt) super.getBaseTag();
    }

    /**
     * Returns the value of this tag
     *
     * @return
     */
    public int getValue() {
        return getRecodeHandle().getValue();
    }

    /**
     * Sets the value of this tag
     *
     * @param value the new value
     */
    public void setValue(int value) {
        getRecodeHandle().setValue(value);
    }

    @Override
    public String toString() {
        return String.format("NBTTag[type=%s, name=%s, value=%d]", getTagName(getType()), getName(), getValue());
    }

    @Override
    public IntTag getRecodeHandle() {
        return (IntTag) super.getRecodeHandle();
    }

    public static NBTTagInt[] fromRecodeArray(IntTag[] recode) {
        NBTTagInt[] classic = new NBTTagInt[recode.length];
        for (int i = 0; i < recode.length; i++) {
            classic[i] = new NBTTagInt(recode[i]);
        }
        return classic;
    }

    public static IntTag[] toRecodeArray(NBTTagInt[] classic) {
        IntTag[] recode = new IntTag[classic.length];
        for (int i = 0; i < classic.length; i++) {
            recode[i] = classic[i].getRecodeHandle();
        }
        return recode;
    }
}
