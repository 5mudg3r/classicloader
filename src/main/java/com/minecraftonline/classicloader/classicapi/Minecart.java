package com.minecraftonline.classicloader.classicapi;

import java.util.HashMap;
import java.util.Map;
import net.canarymod.Canary;
import net.canarymod.api.entity.EntityType;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.entity.item.EntityMinecartEmpty;

/**
 * Minecart - Used for manipulating minecarts
 *
 * @author tw1nk
 */
@RecodeProxy(net.canarymod.api.entity.vehicle.Minecart.class)
public class Minecart extends BaseVehicle {

    /**
     * Type of minecart
     */
    public enum Type {

        /**
         * Base minecart.
         */
        Minecart(0, EntityType.EMPTYMINECART), //
        /**
         * Storage minecart. Has storage for items.
         */
        StorageCart(1, EntityType.CHESTMINECART), //
        /**
         * Powered minecart. Has storage for fuel.
         */
        PoweredMinecart(2, EntityType.FURNACEMINECART), //
        /**
         * TNT minecart.
         */
        TNTMinecart(3, EntityType.TNTMINECART), //
        /**
         * Mob spawner minecart.
         */
        MobSpawnerCart(4, EntityType.MOBSPAWNERMINECART), //
        /**
         * Hopper minecart.
         */
        HopperMinecart(5, EntityType.HOPPERMINECART);
        private final int id;
        private final EntityType type;
        private static final Map<Integer, Type> lookup = new HashMap<Integer, Type>();

        static {
            for (Type t : Type.values()) {
                lookup.put(t.getType(), t);
            }
        }

        private Type(int id, EntityType type) {
            this.id = id;
            this.type = type;
        }

        public int getType() {
            return id;
        }

        public static Type fromId(final int type) {
            return lookup.get(type);
        }
    }

    /**
     * Creates an interface for minecart.
     *
     * @param o
     */
    public Minecart(EntityMinecart o) {
        super(o);
    }

    public Minecart(net.canarymod.api.entity.vehicle.Minecart minecart) {
        super(minecart);
    }

    /**
     * Create a new Minecart at the given position
     *
     * @param x
     * @param y
     * @param z
     * @param type 0=Minecart, 1=StorageCart, 2=PoweredMinecart
     * @deprecated Use
     * {@link #Minecart(World, double, double, double, Minecart.Type)} instead.
     */
    @Deprecated
    public Minecart(double x, double y, double z, Type type) {
        this(etc.getServer().getDefaultWorld(), x, y, z, type);
    }

    /**
     * Create a new Minecart at the given position
     *
     * @param world The world for the new minecart
     * @param x The x coordinate for the new minecart
     * @param y The y coordinate for the new minecart
     * @param z The z coordinate for the new minecart
     * @param type The type for the new minecart
     * @deprecated The minecart system has had an overhaul. Use an appropriate
     * subclass, or the constructor without type for an empty cart.
     *
     */
    @Deprecated
    public Minecart(World world, double x, double y, double z, Type type) {
        this((net.canarymod.api.entity.vehicle.Minecart) Canary.factory().getEntityFactory().newVehicle(type.type,
                new net.canarymod.api.world.position.Location(world.getRecodeHandle(), x, y, z, 0f, 0f)));
        this.spawn();
    }

    /**
     * Create a new Minecart with the given position. Call {@link #spawn()} to
     * spawn it in the world.
     *
     * @param world The world for the new minecart
     * @param x The x coordinate for the new minecart
     * @param y The y coordinate for the new minecart
     * @param z The z coordinate for the new minecart
     */
    public Minecart(World world, double x, double y, double z) {
        this(new EntityMinecartEmpty(world.getWorld(), x, y, z));
    }

    /**
     * Returns the entity we're wrapping.
     *
     * @return
     */
    @Override
    public EntityMinecart getEntity() {
        return (EntityMinecart) super.getEntity();
    }

    /**
     * Sets the current amount of damage the minecart has taken. Decreases over
     * time. The cart breaks when this is over 40.
     *
     * @param damage This minecart's new damage value
     */
    public void setDamage(float damage) {
        // SRG getEntity().func_70492_c(damage);
        getEntity().a(damage);
    }

    @Deprecated
    public void setDamage(int damage) {
        this.setDamage((float) damage);
    }

    /**
     * Gets the current amount of damage the minecart has taken. Decreases over
     * time. The cart breaks when this is over 40.
     *
     * @return This minecart's current damage value
     * @deprecated Minecraft now measures damage in floats. Use
     * {@link #getDamageFloat()} instead.
     */
    @Deprecated
    public int getDamage() {
        return (int) this.getDamageFloat();
    }

    /**
     * Gets the current amount of damage the minecart has taken. Decreases over
     * time. The cart breaks when this is over 40.
     *
     * @return This minecart's current damage value
     */
    public float getDamageFloat() {
        // SRG return getEntity().func_70491_i();
        return getEntity().j();
    }

    /**
     * Returns the type of this minecart.
     *
     * @return type
     */
    public Type getType() {
        // SRG return Type.fromId(getEntity().func_94087_l());
        return Type.fromId(getEntity().m());
    }

    /**
     * Returns the storage for this minecart. Returns null if minecart is not a
     * storage or powered minecart.
     *
     * @return storage
     * @deprecated The minecart system has had an overhaul. Use an appropriate
     * subclass.
     */
    @Deprecated
    public StorageMinecart getStorage() {
        if (getType() == Type.StorageCart || getType() == Type.PoweredMinecart) {
            return new StorageMinecart(getEntity());
        }
        return null;
    }

    /**
     * Returns a new <tt>Minecart</tt> of the specified type. It would still
     * need to be spawned using {@link #spawn()}.
     *
     * @param world The world for the new cart
     * @param x The x-coordinate for the new cart
     * @param y The y-coordinate for the new cart
     * @param z The z-coordinate for the new cart
     * @param type The type of the new cart
     * @return A new minecart of the specified type.
     */
    public static Minecart fromType(World world, double x, double y, double z, Type type) {
        return Wrapper.wrap((net.canarymod.api.entity.vehicle.Minecart) Canary.factory().getEntityFactory().newVehicle(type.type,
                new net.canarymod.api.world.position.Location(world.getRecodeHandle(), x, y, z, 0f, 0f)));
    }

    @Override
    public net.canarymod.api.entity.vehicle.Minecart getRecodeHandle() {
        return (net.canarymod.api.entity.vehicle.Minecart) super.getRecodeHandle();
    }
}
