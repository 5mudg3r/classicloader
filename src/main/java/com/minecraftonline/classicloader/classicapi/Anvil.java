package com.minecraftonline.classicloader.classicapi;

import com.minecraftonline.classicloader.apt.RecodeProxy;
import com.minecraftonline.classicloader.classicapi.bridge.ContainerProxy;
import net.canarymod.api.world.blocks.CanaryAnvil;
import net.minecraft.inventory.ContainerRepair;
import net.minecraft.inventory.IInventory;

/**
 * An interface class to anvils.
 *
 * @author Willem Mulder (14mRh4X0r)
 */
@RecodeProxy(net.canarymod.api.world.blocks.Anvil.class)
public class Anvil extends InventoryCrafting<ContainerProxy> {

    private final net.canarymod.api.world.blocks.Anvil anvil;

    public Anvil(ContainerRepair container, IInventory result, int resultStartIndex) {
        super(container, ContainerProxy.of((net.canarymod.api.world.blocks.Anvil) container.getInventory()), result, resultStartIndex);
        this.anvil = (net.canarymod.api.world.blocks.Anvil) container.getInventory();
    }

    public Anvil(net.canarymod.api.world.blocks.Anvil anvil) {
        super(((CanaryAnvil) anvil).getContainer(), ContainerProxy.of(anvil), ((CanaryAnvil) anvil).getInventoryHandle(), 2);
        this.anvil = anvil;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update() {
        anvil.update();
    }

    /**
     * Get the name currently filled in in the name field.
     * @return the currently filled in name.
     */
    public String getToolName() {
        return anvil.getToolName();
    }

    /**
     * Sets the name filled in in the name field.
     * @param name the new name for the name field.
     */
    public void setToolName(String name) {
        anvil.setToolName(name);
    }

    /**
     * Returns the item that would be the result of the current anvil setup.
     * @return the item that the player would get.
     */
    public Item getResult() {
        return anvil.getResult() == null ? null : Wrapper.wrap(anvil.getResult());
    }

    /**
     * Sets the item that would be the result of the current anvil setup.
     * @param item the new item that the player would get.
     */
    public void setResult(Item item) {
        anvil.setResult(item.getRecodeHandle());
    }

    /**
     * Returns the cost of the repair/rename in XP levels.
     *
     * @return the XP cost to repair and/or rename the currently held item.
     */
    public int getXPCost() {
        return anvil.getXPCost();
    }

    /**
     * Sets the cost of the repair/rename in XP levels.
     *
     * @param level The XP level the repair/rename should cost.
     */
    public void setXPCost(int level) {
        anvil.setXPCost(level);
    }

    public net.canarymod.api.world.blocks.Anvil getRecodeHandle() {
        return anvil;
    }
}
