package com.minecraftonline.classicloader.classicapi;

import ca.grimoire.critical.FixMe;
import java.util.HashMap;
import java.util.Map;
import net.canarymod.Canary;
import net.canarymod.api.factory.ItemFactory;
import net.canarymod.api.inventory.CanaryEnchantment;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/**
 * Enchantment - a class used to access all enchantment related stuff
 *
 * @author Yariv
 *
 */
@RecodeProxy(net.canarymod.api.inventory.Enchantment.class)
public class Enchantment {

    private static final ItemFactory factory = Canary.factory().getItemFactory();

    /**
     * Type - used to identify enchantments
     *
     * @author Yariv
     *
     */
    public enum Type {

        Protection(0), //
        FireProtection(1), //
        FeatherFalling(2), //
        BlastProtection(3), //
        ProjectileProtection(4), //
        Respiration(5), //
        AquaAffinity(6), //
        Thorns(7), //
        Sharpness(16), //
        Smite(17), //
        BaneOfArthropods(18), //
        Knockback(19), //
        FireAspect(20), //
        Looting(21), //
        Efficiency(32), //
        SilkTouch(33), //
        Unbreaking(34), //
        Fortune(35), //
        ArrowDamage(48), //
        ArrowKnockback(49), //
        ArrowFire(50), //
        ArrowInfinite(51);
        private int id;
        private static Map<Integer, Type> map;

        @SuppressWarnings("LeakingThisInConstructor")
        private Type(int id) {
            this.id = id;
            add(id, this);
        }

        private static void add(int type, Type name) {
            if (map == null) {
                map = new HashMap<Integer, Type>();
            }

            map.put(type, name);
        }

        public int getType() {
            return id;
        }

        public static Type fromId(final int type) {
            return map.get(type);
        }
    }
    private net.canarymod.api.inventory.Enchantment enchantment;

    /**
     * Creates a new enchantment
     *
     * @param type
     * @param level
     */
    public Enchantment(Type type, int level) {
        enchantment = factory.newEnchantment((short) type.getType(), (short) level);
    }

    public Enchantment(net.canarymod.api.inventory.Enchantment enchantment) {
        this.enchantment = enchantment;
    }

    /**
     * Returns the OEnchantment object for this enchantment. Notice this isnt
     * the enchantment data, but the enchantment description.
     *
     * @return Returns the OEnchantment object for this enchantment
     */
    public net.minecraft.enchantment.Enchantment getEnchantment() {
        return ((CanaryEnchantment) enchantment).getHandle();
    }

    /**
     * Returns an OEnchantment object for a given enchantment type. Notice this
     * isn't the enchantment data, but the enchantment description.
     *
     * @return Returns the OEnchantment object for a given enchantment type
     */
    public static net.minecraft.enchantment.Enchantment getEnchantment(Type type) {
        return ((CanaryEnchantment) factory.newEnchantment((short) type.getType(), (short) 0)).getHandle();
    }

    /**
     * Type of enchantment.
     *
     * @return type
     */
    public Type getType() {
        return Type.fromId(enchantment.getType().getId());
    }

    /**
     * Set type of enchantment. Note that it does NOT check for validity of the
     * enchantment!!!
     *
     * @param type
     */
    public void setType(int type) {
        this.enchantment = factory.newEnchantment((short) type, (short) getLevel());
    }

    /**
     * Set type of enchantment. Note that it does NOT check for validity of the
     * enchantment!!!
     *
     * @param type
     */
    public void setType(Type type) {
        setType((short) type.getType());
    }

    /**
     * Gets level of enchantment.
     *
     * @return
     */
    public int getLevel() {
        return enchantment.getLevel();
    }

    /**
     * Sets level of enchantment. Note that it does NOT check for validity of
     * the enchantment!!!
     *
     * @param level
     */
    public void setLevel(int level) {
        enchantment.setLevel((short) level);
    }

    /**
     * Gets the weight of the enchantment or -1 if the enchantment is invalid.
     *
     * @return
     */
    public int getWeight() {
        return enchantment.getWeight();
    }

    /**
     * Gets the weight of an enchantment given its type or -1 if the enchantment
     * is invalid.
     *
     * @return
     */
    public static int getWeight(Type type) {
        return fromType(type).getWeight();
    }

    /**
     * Gets the minimum level of the enchantment or -1 if the enchantment is
     * invalid.
     *
     * @return
     */
    public int getMinLevel() {
        return enchantment.getMinEnchantmentLevel();
    }

    /**
     * Gets the minimum level of an enchantment given its type or -1 if the
     * enchantment is invalid.
     *
     * @return
     */
    public static int getMinLevel(Type type) {
        return fromType(type).getMinLevel();
    }

    /**
     * Gets the maximum level of the enchantment or -1 if the enchantment is
     * invalid.
     *
     * @return
     */
    public int getMaxLevel() {
        return enchantment.getMaxEnchantmentLevel();
    }

    /**
     * Gets the maximum level of an enchantment given its type or -1 if the
     * enchantment is invalid.
     *
     * @return
     */
    public static int getMaxLevel(Type type) {
        return fromType(type).getMaxLevel();
    }

    /**
     * Gets the minimum enchantability strength needed for the enchantment or -1
     * if the enchantment is invalid.
     *
     * @return
     */
    public int getMinEnchantability() {
        return enchantment.getMinEnchantability();
    }

    /**
     * Gets the minimum enchantability strength needed for the enchantment given
     * its type or -1 if the enchantment is invalid.
     *
     * @return
     */
    public static int getMinEnchantability(Type type, int level) {
        return new Enchantment(type, level).getMinEnchantability();
    }

    /**
     * Gets the maximum enchantability strength needed for the enchantment or -1
     * if the enchantment is invalid.
     *
     * @return
     */
    public int getMaxEnchantability() {
        return enchantment.getMaxEnchantability();
    }

    /**
     * Gets the maximum enchantability strength needed for the enchantment given
     * its type or -1 if the enchantment is invalid.
     *
     * @return
     */
    public static int getMaxEnchantability(Type type, int level) {
        return new Enchantment(type, level).getMaxEnchantability();
    }

    /**
     * Gets the modified damage for the enchantment for a given damage source or
     * -1 if the enchantment or damage source is invalid.
     *
     * @return
     */
    public int getDamage(net.minecraft.util.DamageSource damageSource) {
        return enchantment.getDamageModifier(damageSource.getCanaryDamageSource());
    }

    /**
     * Gets the modified damage for an enchantment for a given damage source or
     * -1 if the enchantment or damage source is invalid.
     *
     * @return
     */
    public static int getDamage(Type type, int level, net.minecraft.util.DamageSource damageSource) {
        return new Enchantment(type, level).getDamage(damageSource);
    }

    /**
     * Gets the modified living for the enchantment for a given living entity or
     * -1 if the enchantment or entity is invalid.
     *
     * @return
     */
    @FixMe("Implement in API methods when possible")
    public float getLivingFloat(net.minecraft.entity.EntityLivingBase entity) {
        //return enchantment.getDamageModifier((EntityLiving) entity.getCanaryEntity());
        // SRG return ((CanaryEnchantment) enchantment).getHandle().func_152376_a(getLevel(), entity.func_70668_bt());
        return ((CanaryEnchantment) enchantment).getHandle().a(getLevel(), entity.bd());
    }

    public int getLiving(net.minecraft.entity.EntityLivingBase entity) {
        return (int) getLivingFloat(entity);
    }

    /**
     * Gets the modified living for an enchantment for a given living entity or
     * -1 if the enchantment or entity is invalid.
     *
     * @return
     */
    public static float getLivingFloat(Type type, int level, net.minecraft.entity.EntityLivingBase entity) {
        return new Enchantment(type, level).getLivingFloat(entity);
    }

    public static int getLiving(Type type, int level, net.minecraft.entity.EntityLivingBase entity) {
        return (int) getLivingFloat(type, level, entity);
    }

    /**
     * Returns true if this enchantment can stack with another enchantment.
     *
     * @param enchantment The other enchantment
     * @return true if enchantments can stack
     */
    public boolean canStack(Enchantment enchantment) {
        return this.enchantment.canStack(enchantment.getRecodeHandle());
    }

    /**
     * Returns true if this enchantment can stack with another enchantment.
     *
     * @param type The other enchantment type
     * @return true if enchantments can stack
     */
    public boolean canStack(Type type) {
        return canStack(fromType(type));
    }

    /**
     * Returns true if two enchantments can stack with each other.
     *
     * @param type1
     * @param type2
     * @return true if enchantments can stack
     */
    public static boolean canStack(Type type1, Type type2) {
        return fromType(type1).canStack(type2);
    }

    /**
     * Checks if the enchantment is valid.
     *
     * @return true if valid
     */
    public boolean isValid() {
        return enchantment.isValid();
    }

    /**
     * Checks if an enchantment is valid.
     *
     * @param type The type of the enchantment
     * @param level The level of the enchantment
     * @return true if valid
     */
    public static boolean isValid(Type type, int level) {
        return new Enchantment(type, level).isValid();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Enchantment) {
            Enchantment other = (Enchantment) obj;
            return other.getType() == getType() && other.getLevel() == getLevel();
        }
        return false;
    }

    @Override
    public String toString() {
        return String.format("Enchantment[type=%s, level=%d]", getType(), getLevel());
    }

    static Enchantment fromType(Type type) {
        return new Enchantment(type, 0);
    }

    public net.canarymod.api.inventory.Enchantment getRecodeHandle() {
        return enchantment;
    }

    static Enchantment[] fromRecodeArray(net.canarymod.api.inventory.Enchantment[] recode) {
        if (recode == null) {
            return null;
        }
        Enchantment[] classic = new Enchantment[recode.length];
        for (int i = 0; i < recode.length; i++) {
            classic[i] = new Enchantment(recode[i]);
        }
        return classic;
    }

    static net.canarymod.api.inventory.Enchantment[] toRecodeArray(Enchantment[] classic) {
        if (classic == null) {
            return null;
        }
        net.canarymod.api.inventory.Enchantment[] recode = new net.canarymod.api.inventory.Enchantment[classic.length];
        for (int i = 0; i < classic.length; i++) {
            recode[i] = classic[i].getRecodeHandle();
        }
        return recode;
    }
}
