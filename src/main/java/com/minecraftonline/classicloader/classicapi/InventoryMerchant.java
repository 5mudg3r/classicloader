package com.minecraftonline.classicloader.classicapi;

import com.minecraftonline.classicloader.Util;
import com.minecraftonline.classicloader.classicapi.bridge.IInventoryContainer;

public class InventoryMerchant extends ItemArray<IInventoryContainer> {

    private final net.minecraft.inventory.InventoryMerchant merchInv;

    public InventoryMerchant(net.minecraft.inventory.Container oContainer, net.minecraft.inventory.InventoryMerchant container) {
        super(oContainer, new IInventoryContainer(container));
        this.merchInv = container;
    }

    @Override
    public void update() {
        // SRG merchInv.func_70296_d();
        merchInv.e();
    }

    @Override
    public String getName() {
        return container.getName();
    }

    @Override
    public void setName(String value) {
        // Why the fuck is that field there without means to change it?
        Util.<String>setField(net.minecraft.inventory.InventoryMerchant.class, merchInv, "name", value);
    }
}
