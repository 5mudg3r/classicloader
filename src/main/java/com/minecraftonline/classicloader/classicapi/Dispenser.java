package com.minecraftonline.classicloader.classicapi;

import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.tileentity.TileEntityDispenser;

/**
 *
 * @author Meaglin
 */
@RecodeProxy(net.canarymod.api.world.blocks.Dispenser.class)
public class Dispenser extends BaseContainerBlock<TileEntityDispenser> {

    public Dispenser(TileEntityDispenser disp) {
        this(null, disp);
    }

    public Dispenser(net.minecraft.inventory.Container oContainer, TileEntityDispenser disp) {
        super(oContainer, disp, "Trap");
    }

    public Dispenser(net.canarymod.api.world.blocks.Dispenser dispenser) {
        super(dispenser, "Trap");
    }

    public void fire() {
        getRecodeHandle().activate();
    }

    @Override
    public net.canarymod.api.world.blocks.Dispenser getRecodeHandle() {
        return tileEntity.getCanaryDispenser();
    }
}
