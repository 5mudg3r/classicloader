package com.minecraftonline.classicloader.classicapi;

import ca.grimoire.critical.Todo;
import net.canarymod.api.entity.living.humanoid.CanaryHuman;
import net.canarymod.api.entity.living.humanoid.CanaryHumanCapabilities;
import net.canarymod.api.entity.living.humanoid.Human;
import net.canarymod.api.world.position.Position;
import com.minecraftonline.classicloader.Util;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.util.ChunkCoordinates;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.FoodStats;
import net.minecraft.entity.player.PlayerCapabilities;

@RecodeProxy(Human.class)
public class HumanEntity extends LivingEntityBase {

    protected PlayerInventory inventory;

    /**
     * Constructor
     */
    public HumanEntity() {
    }

    /**
     * Constructor
     *
     * @param human
     */
    @SuppressWarnings("OverridableMethodCallInConstructor")
    public HumanEntity(EntityPlayer human) {
        super(human);
        inventory = Wrapper.wrap(this.getRecodeHandle().getInventory());
    }

    public HumanEntity(Human human) {
        super(human);
        inventory = Wrapper.wrap(human.getInventory());
    }

    /**
     * Returns the entity we're wrapping.
     *
     * @return
     */
    @Override
    public EntityPlayer getEntity() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        return (EntityPlayer) super.getEntity();
    }

    /**
     * Returns the name
     *
     * @return
     */
    @Override
    public String getName() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        return getRecodeHandle().getName();
    }

    /**
     * Returns the name displayed above this player's head.
     *
     * @return String
     */
    public String getDisplayName() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        return getRecodeHandle().getDisplayName();
    }

    /**
     * Sets the name displayed above this player's head.
     *
     * @param name The name displayed. Any non-color modification will affect
     * skin.
     */
    public void setDisplayName(String name) {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        getRecodeHandle().setDisplayName(name);
    }

    /**
     * Returns whether this player can receive damage.
     *
     * @return the disableDamage state
     */
    public boolean isDamageDisabled() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        return getRecodeHandle().getCapabilities().isInvulnerable();
    }

    /**
     * Sets whether this player can receive damage.
     *
     * @param disabled the new value.
     * @see #updateCapabilities()
     */
    public void setDamageDisabled(boolean disabled) {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        getRecodeHandle().getCapabilities().setInvulnerable(disabled);
    }

    /**
     * Returns whether the player is flying.
     *
     * @return the flying state
     */
    public boolean isFlying() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        return getRecodeHandle().getCapabilities().isFlying();
    }

    /**
     * Sets whether the player is flying.
     *
     * @param flying the flying state.
     * @see #updateCapabilities()
     */
    public void setFlying(boolean flying) {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        getRecodeHandle().getCapabilities().setFlying(flying);
    }

    /**
     * Returns whether this player is allowed to fly.
     *
     * @return the canFly state
     */
    public boolean canFly() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        return getRecodeHandle().getCapabilities().mayFly();
    }

    /**
     * Sets whether this player is allowed to fly.
     *
     * @param allow <tt>true</tt> to allow flying, <tt>false</tt> to deny it.
     * @see #updateCapabilities()
     */
    public void setCanFly(boolean allow) {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        getRecodeHandle().getCapabilities().setMayFly(allow);
    }

    /**
     * Returns whether the player has a creative perks. When set, enables stuff
     * like items not depleting on use, buckets not emptying, anvils and
     * enchantments always working, etc.
     *
     * @return whether player has a creative inventory.
     */
    public boolean hasCreativePerks() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        return getRecodeHandle().getCapabilities().instantBuild();
    }

    /**
     * Sets whether the player has a creative inventory. When set, enables stuff
     * like items not depleting on use, buckets not emptying, anvils and
     * enchantments always working, etc.
     *
     * @param creativePerks the new state
     * @see #updateCapabilities()
     */
    public void setCreativePerks(boolean creativePerks) {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        getRecodeHandle().getCapabilities().setInstantBuild(creativePerks);
    }

    /**
     * Returns whether the player has build restrictions like in Adventure.
     *
     * @return whether the player has build restrictions.
     */
    @Todo("Pull request this")
    public boolean hasAdventureRestrictions() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }

        PlayerCapabilities caps = Util.getField(CanaryHumanCapabilities.class, getRecodeHandle().getCapabilities(), "capabilities", PlayerCapabilities.class);
        // SRG return !caps.field_75099_e;
        return !caps.e;
    }

    /**
     * Sets whether the player has build restrictions like in Adventure.
     *
     * @param restrict The new value for the flag.
     * @see #updateCapabilities()
     */
    public void setAdventureRestrictions(boolean restrict) {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }

        PlayerCapabilities caps = Util.getField(CanaryHumanCapabilities.class, getRecodeHandle().getCapabilities(), "capabilities", PlayerCapabilities.class);
        // SRG caps.field_75099_e = !restrict;
        caps.e = !restrict;
    }

    /**
     * Returns the current flying speed of the player. Default seems to be
     * <tt>0.05</tt>.
     *
     * @return The current flying speed
     */
    public float getFlyingSpeed() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        return getRecodeHandle().getCapabilities().getFlySpeed();
    }

    /**
     * Returns the current walking speed of the player. Default seems to be
     * <tt>0.1</tt>.
     *
     * @return The current walking speed
     */
    public float getWalkingSpeed() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        return getRecodeHandle().getCapabilities().getWalkSpeed();
    }

    /**
     * Updates the human's capabilities to the client. The client won't be
     * affected unless you call this.
     */
    @Todo("Pull request pulling up from Player")
    public void updateCapabilities() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }

        if (getRecodeHandle().isPlayer()) {
            ((net.canarymod.api.entity.living.humanoid.Player) getRecodeHandle()).updateCapabilities();
        }
    }

    /**
     * Whether or not the player is in a bed.
     *
     * @return Sleeping or not.
     */
    @Todo("Pull request pulling up from Player")
    public boolean isSleeping() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        if (getRecodeHandle().isPlayer()) {
            return ((net.canarymod.api.entity.living.humanoid.Player) getRecodeHandle()).isSleeping();
        } else {
            // SRG return Util.getField(EntityPlayer.class, getEntity(), "field_71083_bS", boolean.class);
            return Util.getField(EntityPlayer.class, getEntity(), "bA", boolean.class);
        }
    }

    /**
     * Add experience points to total for this Player. The amount will be capped
     * at <tt>Integer.MAX_VALUE</tt>.
     *
     * @param amount the amount of experience points to add.
     */
    @Todo("Pull request pulling up from Player")
    public void addXP(int amount) {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        if (getRecodeHandle().isPlayer()) {
            ((net.canarymod.api.entity.living.humanoid.Player) getRecodeHandle()).addExperience(amount);
        } else {
            // SRG Util.callMethod(EntityPlayer.class, getEntity(), "func_71023_q", new Class<?>[]{int.class}, new Object[] {amount}, void.class);
            Util.callMethod(EntityPlayer.class, getEntity(), "v", new Class<?>[]{int.class}, new Object[] {amount}, void.class);
            this.updateXP();
        }
    }

    /**
     * Drop the player inventory
     */
    public void dropInventory() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }

        getRecodeHandle().dropInventory();
    }

    /**
     * Drop the player inventory at the specified Location
     *
     * @param location the location to drop the inventory at
     */
    public void dropInventory(Location location) {
        this.dropInventory(location.x, location.y, location.z);
    }

    /**
     * Drop the player inventory at the specified coordinate
     *
     * @param x
     * @param y
     * @param z
     */
    public void dropInventory(double x, double y, double z) {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }

        // Use ugly hack, but it probably just works (TM).
        Position pos = getRecodeHandle().getPosition();
        getRecodeHandle().teleportTo(new Position(x, y, z));
        getRecodeHandle().dropInventory();
        getRecodeHandle().teleportTo(pos);
    }

    /**
     * Drop item form specified slot
     *
     * @param slot
     */
    public void dropItemFromSlot(int slot) {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        Item i = inventory.getItemFromSlot(slot);
        if (i != null) {
            getWorld().dropItem(getLocation(), i);
            inventory.removeItem(slot);
        }
    }

    /**
     * Drop item form specified slot at the specified Location
     *
     * @param slot
     * @param location
     */
    public void dropItemFromSlot(int slot, Location location) {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        Item i = inventory.getItemFromSlot(slot);
        if (i != null) {
            getWorld().dropItem(location, i);
            inventory.removeItem(slot);
        }
    }

    /**
     * Drop item form specified slot at the specified coordinate
     *
     * @param slot
     * @param x
     * @param y
     * @param z
     */
    public void dropItemFromSlot(int slot, double x, double y, double z) {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        Item i = inventory.getItemFromSlot(slot);
        if (i != null) {
            getWorld().dropItem(x, y, z, i);
            inventory.removeItem(slot);
        }
    }

    /**
     * Get Players food ExhaustionLevel
     *
     * @return
     */
    @Todo("Pull request pulling up from Player")
    public float getFoodExhaustionLevel() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        if (getRecodeHandle().isPlayer()) {
            return ((net.canarymod.api.entity.living.humanoid.Player) getRecodeHandle()).getExhaustionLevel();
        } else {
            // SRG return getEntity().func_71024_bL().getExhaustionLevel();
            return getEntity().bQ().getExhaustionLevel();
        }
    }

    /**
     * Get player Food Level
     *
     * @return player food level
     */
    @Todo("Pull request pulling up from Player")
    public int getFoodLevel() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        if (getRecodeHandle().isPlayer()) {
            return ((net.canarymod.api.entity.living.humanoid.Player) getRecodeHandle()).getHunger();
        } else {
            // SRG return getEntity().func_71024_bL().func_75116_a();
            return getEntity().bQ().a();
        }
    }

    /**
     * Get Players food saturationLevel
     *
     * @return
     */
    @Todo("Pull request this")
    public float getFoodSaturationLevel() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        // SRG return getEntity().func_71024_bL().func_75115_e();
        return getEntity().bQ().e();
    }

    /**
     * Returns this player's inventory.
     *
     * @return inventory
     */
    public Inventory getInventory() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        return inventory;
    }

    /**
     * Gets the item the cursor currently has.
     *
     * @return The {@link Item} the cursor currently has.
     */
    public Item getInventoryCursorItem() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        return inventory.getCursorItem();
    }

    /**
     * Returns item id in player's hand
     *
     * @return item id
     */
    public int getItemInHand() {
        Item inHand = this.getItemStackInHand();
        return inHand == null ? 0 : inHand.getItemId();
    }

    /**
     * Returns the item stack in the player's hand.
     *
     * @return Item
     */
    public Item getItemStackInHand() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        int slot = inventory.getSelectedSlot();
        return slot >= 0 && slot < 9 ? inventory.getItemFromSlot(slot) : null;
    }

    /**
     * Returns a player's respawn location
     *
     * @return spawn location
     */
    public Location getRespawnLocation() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        if (getRecodeHandle().isPlayer()) {
            return new Location(((net.canarymod.api.entity.living.humanoid.Player) getRecodeHandle()).getSpawnPosition());
        } else {
            Location spawn = etc.getServer().getDefaultWorld().getSpawnLocation();
            // SRG ChunkCoordinates loc = getEntity().func_70997_bJ();
            ChunkCoordinates loc = getEntity().bN();
            if (loc != null) {
                // SRG spawn = new Location(etc.getServer().getDefaultWorld(), loc.field_71574_a, loc.field_71572_b, loc.field_71573_c);
                spawn = new Location(etc.getServer().getDefaultWorld(), loc.a, loc.b, loc.c);
            }
            return spawn;
        }
    }

    /**
     * Returns the score for this Player.
     *
     * @return the score for this Player.
     */
    @Todo("Pull request this")
    public int getScore() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        // SRG return getEntity().func_71037_bA();
        return getEntity().bD();
    }

    /**
     * Get experience level for this Player.
     *
     * @return
     */
    public int getLevel() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }

        if (getRecodeHandle().isPlayer()) {
            return ((net.canarymod.api.entity.living.humanoid.Player) getRecodeHandle()).getLevel();
        } else {
            // SRG return getEntity().field_71068_ca;
            return getEntity().bF;
        }
    }

    /**
     * Get total experience amount for this Player.
     *
     * @return
     */
    public int getXP() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }

        if (getRecodeHandle().isPlayer()) {
            return ((net.canarymod.api.entity.living.humanoid.Player) getRecodeHandle()).getExperience();
        } else {
            // SRG return getEntity().field_71067_cb;
            return getEntity().bG;
        }
    }

    /**
     * Returns the "progress" of the XP bar.
     *
     * @return the "progress" of the XP bar, as a float between 0 and 1.
     */
    @Todo("Pull request this")
    public float getXPBarProgress() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }

        // SRG return getEntity().field_71106_cc;
        return getEntity().bH;
    }

    /**
     * Gets the amount of XP needed to fill the bar.
     *
     * @return the amount of XP needed to fill the bar
     */
    @Todo("Pull request this")
    public int getXPBarMax() {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }

        // SRG return getEntity().func_71050_bK();
        return getEntity().bP();
    }

    /**
     * Gives an item to the player.
     *
     * @param item
     * @see PlayerInventory#insertItem(Item)
     */
    public void giveItem(Item item) {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        inventory.insertItem(item);
        inventory.update();
    }

    /**
     * Gives an item to the player
     *
     * @param itemId
     * @param amount
     */
    public void giveItem(int itemId, int amount) {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        inventory.giveItem(itemId, amount);
        inventory.update();
    }

    /**
     * Gives the player this item by dropping it in front of them.<br>
     * <strong>NOTE:</strong> using this method calls the
     * PluginLoader.Hook.ITEM_DROP hook.
     *
     * @param toDrop
     * @see PluginLoader.Hook.ITEM_DROP
     * @see PluginListener.onItemDrop(Player player, ItemEntity item)
     */
    public void giveItemDrop(Item toDrop) {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        getRecodeHandle().dropItem(toDrop.getRecodeHandle());
    }

    /**
     * Gives the player this item by dropping it in front of them.<br>
     * <strong>NOTE:</strong> using this method calls the
     * PluginLoader.Hook.ITEM_DROP hook.
     *
     * @param itemId
     * @param amount
     * @see PluginLoader.Hook.ITEM_DROP
     * @see PluginListener.onItemDrop(Player player, ItemEntity item)
     */
    public void giveItemDrop(int itemId, int amount) {
        giveItemDrop(itemId, amount, 0);
    }

    /**
     * Gives the player this item by dropping it in front of them.<br>
     * <strong>NOTE:</strong> using this method calls the
     * PluginLoader.Hook.ITEM_DROP hook.
     *
     * @param itemId
     * @param amount
     * @param damage
     * @see PluginLoader.Hook.ITEM_DROP
     * @see PluginListener.onItemDrop(Player player, ItemEntity item)
     */
    public void giveItemDrop(int itemId, int amount, int damage) {
        this.giveItemDrop(new Item(itemId, amount, -1, damage));
    }

    public boolean hasItem(Item item) {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        Item i = inventory.getItemFromId(item.getItemId());
        if (i == null) {
            return false;
        }
        return true;
    }

    /**
     * Remove experience points from total for this Player. The amount will be
     * capped at 0.
     *
     * @param amount the amount of experience points to remove.
     */
    public void removeXP(int amount) {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        this.getEntity().removeXP(amount);
        this.updateXP();
    }

    /**
     * Set player food exhaustion level
     *
     * @param foodExhaustionLevel
     */
    public void setFoodExhaustionLevel(float foodExhaustionLevel) {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }

        if (getRecodeHandle().isPlayer()) {
            ((net.canarymod.api.entity.living.humanoid.Player) getRecodeHandle()).setExhaustion(foodExhaustionLevel);
        } else {
            // SRG getEntity().func_71024_bL().setExhaustionLevel(foodExhaustionLevel);
            getEntity().bQ().setExhaustionLevel(foodExhaustionLevel);
            updateLevels();
        }
    }

    /**
     * Set Player food level
     *
     * @param foodLevel new food level, between 1 and 20
     */
    public void setFoodLevel(int foodLevel) {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }

        if (getRecodeHandle().isPlayer()) {
            ((net.canarymod.api.entity.living.humanoid.Player) getRecodeHandle()).setHunger(foodLevel);
        } else {
            // SRG getEntity().func_71024_bL().setFoodLevel(foodLevel);
            getEntity().bQ().setFoodLevel(foodLevel);
            updateLevels();
        }
    }

    /**
     * Set player food saturation level
     *
     * @param foodSaturationLevel
     */
    public void setFoodSaturationLevel(float foodSaturationLevel) {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        // SRG Util.<Float>setField(FoodStats.class, getEntity().func_71024_bL(), "field_75125_b", Math.min(foodSaturationLevel, getFoodLevel()));
        Util.<Float>setField(FoodStats.class, getEntity().bQ(), "b", Math.min(foodSaturationLevel, getFoodLevel()));
        updateLevels();
    }

    /**
     * Sets the item the cursor should have.
     */
    public void setInventoryCursorItem(Item item) {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }
        inventory.setCursorItem(item);
    }

    /**
     * Sets a player's respawn location. Only x, y and z apply, world will be
     * the default world.
     *
     * @param location The {@link Location} containing the x, y and z
     * coordinates for the respawn location
     */
    public void setRespawnLocation(Location location) {
        this.setRespawnLocation(etc.floor(location.x), etc.floor(location.y), etc.floor(location.z));
    }

    /**
     * Sets a player's respawn location
     *
     * @param x
     * @param y
     * @param z
     */
    public void setRespawnLocation(int x, int y, int z) {
        if (getRecodeHandle() == null) {
            throw new IllegalStateException("Human isn't fully initialized");
        }

        if (getRecodeHandle().isPlayer()) {
            ((net.canarymod.api.entity.living.humanoid.Player) getRecodeHandle()).setSpawnPosition(new net.canarymod.api.world.position.Location(x, y, z));
        } else {
            ChunkCoordinates loc = new ChunkCoordinates(x, y, z);
            // SRG getEntity().func_71063_a(loc, true);
            getEntity().a(loc, true);
        }
    }

    /**
     * Set total experience points for this Player. Calls {@link #removeXP(int)}
     * or {@link #addXP(int)} based on
     * <tt>amount</tt> and the current XP.
     *
     * @param amount the new amount of experience points.
     */
    public void setXP(int amount) {
        if (amount < this.getXP()) {
            this.removeXP(this.getXP() - amount);
        } else {
            this.addXP(amount - this.getXP());
        }
    }

    /**
     * Updates the inventory on the client.
     */
    public void updateInventory() {
        // Do nothing by default
    }

    /**
     * Send update food and health to client.
     */
    public void updateLevels() {
        // Do nothing by default
    }

    /**
     * Send player the updated experience packet.
     *
     */
    public void updateXP() {
        // Do nothing by default.
    }

    public void setInventory() {
        if (inventory == null) {
            inventory = Wrapper.wrap(getRecodeHandle().getInventory());
        }
    }

    /**
     * Returns this player's {@link EnderChestInventory} for modification.
     *
     * @return this player's {@link EnderChestInventory} for modification
     */
    public EnderChestInventory getEnderChest() {
        return new EnderChestInventory(null, this); // Cheating is fun!
    }

    @Override
    public Human getRecodeHandle() {
        return (Human) super.getRecodeHandle();
    }
}
