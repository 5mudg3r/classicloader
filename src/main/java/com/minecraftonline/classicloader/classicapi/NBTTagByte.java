package com.minecraftonline.classicloader.classicapi;

import net.canarymod.Canary;
import net.canarymod.api.nbt.ByteTag;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/**
 * Interface for the net.minecraft.server.NBTTagByte class
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(ByteTag.class)
public class NBTTagByte extends NBTBase {

    /**
     * Creates a wrapper around an net.minecraft.server.NBTTagByte
     *
     * @param baseTag
     */
    public NBTTagByte(net.minecraft.nbt.NBTTagByte baseTag) {
        super(baseTag);
    }

    /**
     * Creates a new NBTTagByte
     *
     * @param name the name of the new tag
     */
    public NBTTagByte(String name) {
        this(Canary.factory().getNBTFactory().newByteTag((byte) 0));
    }

    /**
     * Creates a new NBTTagByte
     *
     * @param name the name of the new tag
     * @param value the value of the new tag
     */
    public NBTTagByte(String name, byte value) {
        this(Canary.factory().getNBTFactory().newByteTag(value));
    }

    public NBTTagByte(ByteTag byteTag) {
        super(byteTag);
    }

    /**
     * Returns the byte value of this tag
     *
     * @return
     */
    public byte getValue() {
        return getRecodeHandle().getValue();
    }

    /**
     * Sets the byte value of this tag
     *
     * @param value
     */
    public void setValue(byte value) {
        getRecodeHandle().setValue(value);
    }

    @Override
    public net.minecraft.nbt.NBTTagByte getBaseTag() {
        return (net.minecraft.nbt.NBTTagByte) super.getBaseTag();
    }

    @Override
    public String toString() {
        return String.format("NBTTag[type=%s, name=%s, value=%d]", getTagName(getType()), getName(), getValue());
    }

    @Override
    public ByteTag getRecodeHandle() {
        return (ByteTag) super.getRecodeHandle();
    }

    public NBTTagByte[] fromRecodeArray(ByteTag[] recode) {
        NBTTagByte[] classic = new NBTTagByte[recode.length];
        for (int i = 0; i < recode.length; i++) {
            classic[i] = new NBTTagByte(recode[i]);
        }
        return classic;
    }

    public ByteTag[] toRecodeArray(NBTTagByte[] classic) {
        ByteTag[] recode = new ByteTag[classic.length];
        for (int i = 0; i < classic.length; i++) {
            recode[i] = classic[i].getRecodeHandle();
        }
        return recode;
    }
}
