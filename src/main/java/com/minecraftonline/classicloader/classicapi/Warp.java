package com.minecraftonline.classicloader.classicapi;

/**
 * Warp.java - Contains the stuff for a warp (Name, location, etc.). Also used
 * for homes.
 *
 * @author James
 */
public class Warp {

    /**
     * Warp ID - Used in database transactions.
     */
    public int ID;
    /**
     * Warp name.
     */
    public String Name;
    /**
     * Warp group.
     */
    public String Group;
    /**
     * Warp's location.
     */
    public Location Location;

    public static Warp fromRecode(net.canarymod.warp.Warp recode) {
        Warp w = new Warp();
        w.Name = recode.getName();
        w.Location = new Location(recode.getLocation());

        net.canarymod.user.Group[] groups = recode.getGroups();
        w.Group = groups == null || groups.length == 0 || groups[0] == null ? null : groups[0].getName();

        return w;
    }
}
