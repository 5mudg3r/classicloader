package com.minecraftonline.classicloader.classicapi;

import com.minecraftonline.classicloader.Util;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import java.util.*;
import net.canarymod.Canary;
import net.canarymod.ToolBox;
import net.canarymod.api.GameMode;
import net.canarymod.api.OfflinePlayer;
import net.canarymod.api.entity.living.humanoid.CanaryPlayer;
import net.canarymod.backbone.PlayerDataAccess;
import net.canarymod.database.exceptions.DatabaseReadException;
import net.canarymod.permissionsystem.PermissionNode;
import net.canarymod.permissionsystem.PermissionProvider;
import net.minecraft.entity.player.EntityPlayer.EnumChatVisibility;
import net.minecraft.entity.player.EntityPlayerMP;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.MessageFormatMessageFactory;

/**
 * Player.java - Interface so mods don't have to update often.
 *
 * @author James
 */
@RecodeProxy(net.canarymod.api.entity.living.humanoid.Player.class)
public class Player extends HumanEntity implements MessageReceiver {

    private static final Logger log = LogManager.getLogger(new MessageFormatMessageFactory());
    public static final String PERM_PREFIX = "com.minecraftonline.classicloader.command.";
    private final OfflinePlayer offlinePlayer;

    /**
     * Creates an empty player. Add the player by calling
     * {@link #setUser(EntityPlayerMP)}
     */
    public Player() {
        offlinePlayer = null;
    }

    public Player(EntityPlayerMP player) {
        this(player.getPlayer());
    }

    public Player(net.canarymod.api.entity.living.humanoid.Player player) {
        super(player);
        offlinePlayer = null;
    }

    public Player(OfflinePlayer offlinePlayer) {
        this.offlinePlayer = offlinePlayer;
    }

    /**
     * Returns the entity we're wrapping.
     *
     * @return
     */
    @Override
    public EntityPlayerMP getEntity() {
        return (EntityPlayerMP) super.getEntity();
    }

    @Override
    public net.canarymod.api.entity.living.humanoid.Player getRecodeHandle() {
        return (net.canarymod.api.entity.living.humanoid.Player) super.getRecodeHandle();
    }

    /**
     * Returns if the player is still connected
     *
     * @return
     */
    public boolean isConnected() {
        return Canary.getServer().getPlayer(this.getName()) != null;
    }

    /**
     * Kicks player with the specified reason
     *
     * @param reason
     */
    public void kick(String reason) {
        getRecodeHandle().kick(reason);
    }

    /**
     * Sends player a notification
     *
     * @param message
     */
    @Override
    public void notify(String message) {
        getRecodeHandle().notice(message);
    }

    /**
     * Sends a message to the player
     *
     * @param message
     */
    public void sendMessage(String message) {
        getRecodeHandle().message(message);
    }

    /**
     * Makes player send message.
     *
     * @param message
     */
    public void chat(String message) {
        getRecodeHandle().chat(message);
    }

    /**
     * Makes player use command.
     *
     * @param command
     *
     */
    public void command(String command) {
        getRecodeHandle().executeCommand(command.split(" "));
    }

    /**
     * Returns true if this player can use the specified command
     *
     * @param command
     * @return
     */
    public boolean canUseCommand(String command) {
        return getRecodeHandle().hasPermission(PERM_PREFIX + command);
    }

    /**
     * Returns true if this player can use the specified command This method
     * ignores permission management plugins and shouldn't be used by other
     * plugins.
     *
     * @param command
     * @return
     */
    public boolean canUseCommandByDefault(String command) {
        return getRecodeHandle().safeHasPermission(PERM_PREFIX + command);
    }

    /**
     * Checks to see if this player is in the specified group
     *
     * @param group
     * @return
     */
    public boolean isInGroup(String group) {
        return getRecodeHandle().isInGroup(group, true);
    }

    /**
     * Returns true if this player has control over the other player
     *
     * @param player
     * @return true if player has control
     */
    public boolean hasControlOver(Player player) {
        boolean isInGroup = false;

        if (player.hasNoGroups()) {
            return true;
        }
        for (String str : player.getGroups()) {
            if (isInGroup(str)) {
                isInGroup = true;
            } else {
                continue;
            }
            break;
        }

        return isInGroup;
    }

    /**
     * Returns the IP of this player
     *
     * @return
     */
    public String getIP() {
        return getRecodeHandle().getIP();
    }

    /**
     * Returns true if this player is an admin.
     *
     * @return
     */
    public boolean isAdmin() {
        return getRecodeHandle().isAdmin();
    }

    /**
     * Returns true if this player is an admin.
     *
     * @return
     * @deprecated Use {@link #isAdmin()} instead.
     */
    @Deprecated
    public boolean getAdmin() {
        return isAdmin();
    }

    /**
     * Sets whether or not this player is an administrator
     *
     * @param admin
     */
    public void setAdmin(boolean admin) {
        getRecodeHandle().getPermissionProvider().addPermission("canary.super.administrator", admin);
    }

    /**
     * Returns false if this player can not modify terrain, edit chests, etc.
     *
     * @return
     */
    public boolean canBuild() {
        return getRecodeHandle().canBuild();
    }

    /**
     * Don't use this, use canBuild()
     *
     * @return
     */
    @Deprecated
    public boolean canModifyWorld() {
        return canBuild();
    }

    /**
     * Sets whether or not this player can modify the dimension terrain
     *
     * @param canModifyWorld
     */
    public void setCanModifyWorld(boolean canModifyWorld) {
        getRecodeHandle().setCanBuild(canModifyWorld);
    }

    private PermissionNode getCommandNode() {
        PermissionProvider pp = getRecodeHandle().getPermissionProvider();
        List<PermissionNode> rootNodes = getRecodeHandle().getPermissionProvider().getPermissionMap();

        while (pp.getParent() != null) {
            pp = pp.getParent();
            rootNodes.addAll(pp.getPermissionMap());
        }

        PermissionNode com = null;
        for (PermissionNode rootNode : rootNodes) {
            if (rootNode.getName().equals("com")) {
                com = rootNode;
                break;
            }
        }

        return com != null
                && com.hasChildNode("minecraftonline")
                && com.getChildNode("minecraftonline").hasChildNode("classicloader")
                && com.getChildNode("minecraftonline").getChildNode("classicloader").hasChildNode("command")
                 ? com.getChildNode("minecraftonline").getChildNode("classicloader").getChildNode("command")
                 : null;
    }

    /**
     * Set allowed commands
     *
     * @return
     */
    public String[] getCommands() {
        HashSet<String> commands = new HashSet<String>();
        PermissionNode command = getCommandNode();
        if (command != null) {
            for (PermissionNode perm : command.getChilds().values()) {
                if (perm.getValue()) {
                    commands.add(perm.getName());
                }
            }
        }

        return commands.toArray(new String[commands.size()]);
    }

    /**
     * Sets this player's allowed commands
     *
     * @param commands
     */
    public void setCommands(String[] commands) {
        net.canarymod.api.entity.living.humanoid.Player p = getRecodeHandle();

        HashSet<String> toAdd = new HashSet<String>(commands.length);
        for (String command : commands) {
            toAdd.add(PERM_PREFIX + command);
        }

        PermissionNode command = getCommandNode();
        if (command != null) {
            for (PermissionNode perm : command.getChilds().values()) {
                if (perm.getValue() && toAdd.contains(perm.getFullPath())) {
                    toAdd.remove(perm.getName());
                } else {
		    // Don't use removePlayerPermission(String, PlayerReference) here,
		    // we don't want to reload the database in this loop
                    Canary.permissionManager().removePlayerPermission(
                            perm.getFullPath(),
                            p.getUUIDString(),
                            p.getWorld().getFqName()
                    );
                }
            }
        }

	// Reload the permissions here instead
	p.getPermissionProvider().reload()

        for (String cmd : toAdd) {
            p.getPermissionProvider().addPermission(cmd, true);
        }
    }

    /**
     * Returns this player's groups
     *
     * @return
     */
    public String[] getGroups() {
        net.canarymod.user.Group[] recodeGroups = getRecodeHandle().getPlayerGroups();
        LinkedHashSet<String> groups = new LinkedHashSet<String>(recodeGroups.length);
        for (net.canarymod.user.Group group : recodeGroups) {
            groups.add(group.getName());
        }

        return groups.toArray(new String[0]);
    }

    /**
     * Sets this player's groups
     *
     * @param groups
     */
    public void setGroups(String[] groups) {
        throw new UnsupportedOperationException("Cannot execute setGroups(String[]), "
                + "group system is fundamentally different from classic");
    }

    /**
     * Adds the player to the specified group
     *
     * @param group group to add player to
     */
    public void addGroup(String group) {
        getRecodeHandle().addGroup(Canary.usersAndGroups().getGroup(group));
    }

    /**
     * Removes specified group from list of groups
     *
     * @param group group to remove
     */
    public void removeGroup(String group) {
        getRecodeHandle().removeGroup(group);
    }

    /**
     * Returns the sql ID.
     *
     * @return
     */
    public int getSqlId() {
        PlayerDataAccess pda = new PlayerDataAccess();
        try {
            Canary.db().load(pda, Collections.<String, Object>singletonMap("name", getName()));
            return pda.id;
        } catch (DatabaseReadException dre) {
            log.warn("DatabaseReadException while getting ID", dre);
            return -1;
        }
    }

    /**
     * Sets the sql ID. Don't touch this.
     *
     * @param id
     */
    public void setSqlId(int id) {}

    /**
     * If the user can ignore restrictions this will return true. Things like
     * item amounts and such are unlimited, etc.
     *
     * @return
     */
    public boolean canIgnoreRestrictions() {
        return getRecodeHandle().canIgnoreRestrictions();
    }

    /**
     * Don't use. Use canIgnoreRestrictions()
     *
     * @return
     */
    @Deprecated
    public boolean ignoreRestrictions() {
        return canIgnoreRestrictions();
    }

    /**
     * Sets ignore restrictions
     *
     * @param ignoreRestrictions
     */
    public void setIgnoreRestrictions(boolean ignoreRestrictions) {
        getRecodeHandle().setCanIgnoreRestrictions(ignoreRestrictions);
    }

    /**
     * Returns allowed IPs
     *
     * @return
     */
    public String[] getIps() {
        return getRecodeHandle().getAllowedIPs();
    }

    /**
     * Sets allowed IPs
     *
     * @param ips
     */
    public void setIps(String[] ips) {
        // XXX Not implemented in Recode
    }

    /**
     * Returns the correct color/prefix for this player
     *
     * @return
     */
    public String getColor() {
        return getRecodeHandle().getPrefix();
    }

    /**
     * Returns the prefix. NOTE: Don't use this, use getColor() instead.
     *
     * @return
     */
    public String getPrefix() {
        return getColor();
    }

    /**
     * Sets the prefix.
     *
     * @param prefix
     */
    public void setPrefix(String prefix) {
        getRecodeHandle().setPrefix(prefix);
    }

    /**
     * Get players name.
     *
     * @return The offline name
     */
    public String getOfflineName() {
        if (getEntity() != null) {
            return getName();
        } else if (offlinePlayer != null) {
            return offlinePlayer.getName();
        } else {
            return "";
        }
    }

    /**
     * Sets offline player name.
     *
     * @param name The offline name
     */
    public void setOfflineName(String name) {}

    /**
     * Gets the full name prefix+name.
     *
     * @return The name including prefix
     */
    public String getFullName() {
        return this.getColor() + this.getName();
    }

    /**
     * Gets the name that shows on PlayerList (tab).
     *
     * @param show Whether to show the player
     * @return PlayerListEntry The entry to display.
     */
    public PlayerlistEntry getPlayerlistEntry(boolean show) {
        return Wrapper.wrap(getRecodeHandle().getPlayerListEntry(show));
    }

    /**
     * Gets player ping (as shown in playerlist).
     *
     * @return The ping
     */
    public int getPing() {
        return getRecodeHandle().getPing();
    }

    /**
     * Gets the actual user class.
     *
     * @return
     */
    public EntityPlayerMP getUser() {
        return this.getEntity();
    }

    @Override
    public void teleportTo(double x, double y, double z, float rotation, float pitch) {
        getRecodeHandle().teleportTo(x, y, z, pitch, rotation);
    }

    @Override
    public void teleportTo(BaseEntity entity) {
        getRecodeHandle().teleportTo(entity.getLocation().getRecodeLocation());
    }

    @Override
    public void teleportTo(Location loc) {
        getRecodeHandle().teleportTo(loc.getRecodeLocation());
    }

    /**
     * Returns true if the player is muted.
     *
     * @return Whether the player is muted.
     */
    public boolean isMuted() {
        return getRecodeHandle().isMuted();
    }

    /**
     * Toggles mute.
     *
     * @return Whether the player is now muted.
     */
    public boolean toggleMute() {
        getRecodeHandle().setMuted(!isMuted());
        return isMuted();
    }

    /**
     * Checks to see if this player is in any groups.
     *
     * @return true if this player isn't in any group.
     */
    public boolean hasNoGroups() {
        return getRecodeHandle().getPlayerGroups().length == 0;
    }

    /**
     * Returns whether or not this entity is blocking with/using the item in
     * their hand. (e.g. sword, bow, food)
     *
     * @return true if blocking
     */
    // TODO pull up
    public boolean isBlocking() {
        return getRecodeHandle().isBlocking();
    }

    /**
     * Returns the one-use only kits
     *
     * @return List of kit names
     */
    public List<String> getOnlyOneUseKits() {
        return Collections.emptyList();
    }

    /**
     * Add a cooldown kit to the cooldown map.
     *
     * @param kit The <tt>Kit</tt> to add to the map
     * @param delay The delay after which this kit can be used again
     */
    public void addCooldownKit(Kit kit, int delay) {
        net.canarymod.kit.Kit theKit = Canary.kits().getKit(kit.Name);
        @SuppressWarnings("unchecked")
        Map<String, Long> uses = Util.getField(net.canarymod.kit.Kit.class, theKit, "lastUsages", Map.class);
        uses.put(getName(), ToolBox.getUnixTimestamp() + delay / 20 - theKit.getDelay());
    }

    /**
     * Remove a cooldown kit from the cooldown map. This makes it available to
     * the player for use again immediately.
     *
     * @param kit The <tt>Kit</tt> to remove.
     */
    public void removeCooldownKit(Kit kit) {
        net.canarymod.kit.Kit theKit = Canary.kits().getKit(kit.Name);
        @SuppressWarnings("unchecked")
        Map<String, Long> uses = Util.getField(net.canarymod.kit.Kit.class, theKit, "lastUsages", Map.class);
        uses.remove(getName());
    }

    /**
     * Check whether a player can use the cooldown kit. If the cooldown has
     * expired, this method also removes the kit it from the cooldown map.
     *
     * @param kit The <tt>Kit</tt> to check for.
     * @return <tt>false</tt> if the kit's cooldown period is in effect,
     * <tt>true</tt> otherwise.
     */
    public boolean canUseCooldownKit(Kit kit) {
        net.canarymod.kit.Kit theKit = Canary.kits().getKit(kit.Name);
        @SuppressWarnings("unchecked")
        Map<String, Long> uses = Util.getField(net.canarymod.kit.Kit.class, theKit, "lastUsages", Map.class);
        return uses.get(getName()) + theKit.getDelay() < ToolBox.getUnixTimestamp();
    }

    /**
     * Switch to the specified dimension at the according position.
     *
     * @param world The world to switch to
     */
    public void switchWorlds(World world) {
        Location location = getLocation();
        location.setWorld(world);
        getRecodeHandle().teleportTo(location.getRecodeLocation());
    }

    @Override
    public String toString() {
        return String.format("Player[name=%s]", getName());
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Player)) {
            return false;
        }
        final Player other = (Player) obj;

        return getName() == null ? other.getName() == null : getName().equals(other.getName());
    }

    @Override
    public int hashCode() {
        int hash = 7;

        hash = 89 * hash + (this.getName() != null ? this.getName().hashCode() : 0);
        return hash;
    }

    /**
     * Set creative mode for this Player
     *
     * @param i
     */
    public void setCreativeMode(int i) {
        getRecodeHandle().setModeId(i);
    }

    /**
     * Get creative mode for this Player
     *
     * @return i
     */
    public int getCreativeMode() {
        return getRecodeHandle().getModeId();
    }

    /**
     * check if the player is in adventure mode
     *
     * @return true = it is in this mode<br>false = it is not in this mode
     */
    public boolean isAdventureMode() {
        return getRecodeHandle().getMode() == GameMode.ADVENTURE;
    }

    /**
     * Check if the player is in survival mode
     *
     * @return true = it is in this mode<br>false = it is not in this mode
     */
    public boolean isSurvivalMode() {
        return getRecodeHandle().getMode() == GameMode.SURVIVAL;
    }

    /**
     * Check if the player is in creative mode
     *
     * @return true = it is in this mode<br>false = it is not in this mode
     */
    public boolean isCreativeMode() {
        return getRecodeHandle().getMode() == GameMode.CREATIVE;
    }

    /**
     * Check to see if this Player is in creative mode
     *
     * @return <tt>true</tt> if the given Player is in creative mode,
     * <tt>null</tt> otherwise.
     * @deprecated Depend on {@link #getCreativeMode()} instead
     */
    @Deprecated
    public boolean getMode() {
        if (this.getCreativeMode() == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Refresh this Player's mode.
     *
     * @deprecated This method has a confusing name. Use a combination of
     * {@link #setCreativeMode(int)} and {@link World#getGameMode()} instead.
     */
    @Deprecated
    public void refreshCreativeMode() {
        this.setCreativeMode(this.getWorld().getGameMode());
    }

    /**
     * Returns whether falling is disabled.
     *
     * @return the disableFalling state
     * @deprecated Misleading name. See {@link #canFly()}
     */
    @Deprecated
    public boolean isFallingDisabled() {
        return canFly();
    }

    /**
     * Sets whether falling is disabled.
     *
     * @param disabled the new value
     * @deprecated Misleading name. See {@link #setCanFly(boolean)}
     */
    @Deprecated
    public void setFallingDisabled(boolean disabled) {
        setCanFly(disabled);
    }

    /**
     * Returns whether buckets are always full. When set, every bucket that the
     * player holds stays full after emptying.
     *
     * @return whether buckets are always full.
     * @deprecated Misleading name. See {@link #hasCreativePerks()}
     */
    public boolean isBucketAlwaysFull() {
        return hasCreativePerks();
    }

    /**
     * Sets whether buckets are always full. When set, every bucket that the
     * player holds stays full after emptying.
     *
     * @param alwaysFull the new state
     * @deprecated Misleading name. See {@link #setCreativePerks(boolean)}
     */
    public void setBucketAlwaysFull(boolean alwaysFull) {
        setCreativePerks(alwaysFull);
    }

    /**
     * Returns whether this player is an op.
     *
     * @return {@code true} if the player is op.
     */
    public boolean isOp() {
        return isOp(getName());
    }

    /**
     * Static method to determine whether a player is op.
     *
     * @param playerName The name of the player to check.
     * @return {@code true} if the player is op.
     */
    public static boolean isOp(String playerName) {
        return Canary.ops().isOpped(playerName);
    }

    /**
     * Returns a <tt>String</tt> containing possible completions for
     * <tt>currentText</tt>.
     *
     * @param currentText The text to be autocompleted.
     * @return A null-char-separated String of options.
     */
    public String autoComplete(String currentText) {
        String[] split = currentText.split(" ");
        String[] args = new String[split.length - 1];

        System.arraycopy(split, 1, args, 0, args.length);
        List<String> options = Canary.commands().tabComplete(getRecodeHandle(), split[0], args);

        if (options == null) {
            return null;
        }

        StringBuilder autoCompleteBuilder = new StringBuilder();
        for (String option : options) {
            autoCompleteBuilder.append(option).append('\000');
        }

        return autoCompleteBuilder.substring(0, autoCompleteBuilder.length() - 1);
    }

    /**
     * This method plays a sound for just this player, and no one else can hear
     * it.
     *
     * @param location the location to play the sound at
     * @param sound the sound to play
     * @param volume the volume to play the sound at
     * @param pitch the pitch to play the sound at
     * @see Sound
     * @see Location
     */
    public void playSound(Location location, Sound sound, float volume, float pitch) {
        playSound(location.x, location.y, location.z, sound, volume, pitch);
    }

    /**
     * This method plays a sound for just this player, and no one else can hear
     * it.
     *
     * @param x x coordinate to play the sound at
     * @param y y coordinate to play the sound at
     * @param z z coordinate to play the sound at
     * @param sound the sound to play
     * @param volume the volume to play the sound at
     * @param pitch the pitch to play the sound at
     * @see Sound
     * @see Location
     */
    public void playSound(double x, double y, double z, Sound sound, float volume, float pitch) {
        getRecodeHandle().sendPacket(Canary.factory().getPacketFactory().soundEffect(sound.getSoundString(), x, y, z, volume, pitch));
    }

    /**
     * Returns this player's set view distance.
     *
     * @return the view distance
     */
    public int getViewDistance() {
        return this.getEntity().getViewDistance();
    }

    /**
     * Returns the chat preference for this player. This number denotes whether
     * the player wants command output only (1), or has disabled messages
     * overall (2)
     *
     * @return chat preferences
     * @see #wantsCommandsOnly()
     * @see #hasChatDisabled()
     */
    public int getChatPreference() {
        // SRG return this.getEntity().func_147096_v().func_151428_a();
        return this.getEntity().u().a();
    }

    /**
     * Returns whether this player wants output from commands only.
     *
     * @return Whether the player set "Chat: Commands Only"
     */
    public boolean wantsCommandsOnly() {
        // SRG return this.getEntity().func_147096_v() == EnumChatVisibility.SYSTEM;
        return this.getEntity().u() == EnumChatVisibility.SYSTEM;
    }

    /**
     * Returns whether this player has chat disabled.
     *
     * @return Whether the player set "Chat: Hide"
     */
    public boolean hasChatDisabled() {
        // SRG return this.getEntity().func_147096_v() == EnumChatVisibility.HIDDEN;
        return this.getEntity().u() == EnumChatVisibility.HIDDEN;
    }

    /**
     * Returns the state of the client's "Colors" setting.
     *
     * @return whether this player wants colors disabled.
     */
    public boolean wantsColorDisabled() {
        return !this.getEntity().getColorEnabled();
    }

    @Override
    public void setInventoryCursorItem(Item item) {
        super.setInventoryCursorItem(item);
        // Update client
        getRecodeHandle().sendPacket(Canary.factory().getPacketFactory().setSlot(-1, -1, item.getRecodeHandle()));
    }

    @Override
    public void updateInventory() {
        // SRG getEntity().field_71069_bz.updateChangedSlots();
        getEntity().bn.updateChangedSlots();
    }

    @Override
    public void updateLevels() {
        getRecodeHandle().sendPacket(Canary.factory().getPacketFactory().updateHealth(getHealthFloat(), getFoodLevel(), getFoodSaturationLevel()));
    }

    @Override
    public void updateXP() {
        getRecodeHandle().sendPacket(Canary.factory().getPacketFactory().setExperience(getXPBarProgress(), getLevel(), getXP()));
    }
}
