package com.minecraftonline.classicloader.classicapi;

import java.util.Random;
import net.canarymod.api.entity.Fireball;
import com.minecraftonline.classicloader.Util;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.Entity;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.entity.projectile.EntityFireball;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.entity.IProjectile;

/**
 * Interface for a projectile
 *
 * @author gregthegeek
 *
 */
public class Projectile extends BaseEntity {

    @RecodeProxy(net.canarymod.api.entity.Projectile.class)
    protected static class GravityAffectedProjectile extends Projectile {

        public GravityAffectedProjectile(Entity base) {
            super(base);
        }

        public GravityAffectedProjectile(net.canarymod.api.entity.Projectile projectile) {
            super(projectile);
        }

        @Override
        public net.canarymod.api.entity.Projectile getRecodeHandle() {
            return (net.canarymod.api.entity.Projectile) super.getRecodeHandle();
        }
    }

    @RecodeProxy(Fireball.class)
    protected static class FireballProjectile extends Projectile {

        public FireballProjectile(Entity base) {
            super(base);
        }

        public FireballProjectile(Fireball fireball) {
            super(fireball);
        }

        @Override
        public Fireball getRecodeHandle() {
            return (Fireball) super.getRecodeHandle();
        }
    }

    /**
     * Wraps a projectile
     *
     * @param base the projectile to wrap
     */
    public Projectile(Entity base) {
        super(base);
    }

    public Projectile(net.canarymod.api.entity.Entity entity) {
        super(entity);
    }

    /**
     * Launch/shoot this projectile
     */
    public void launch() {
        getWorld().launchProjectile(this);
    }

    /**
     * Returns the shooter of this projectile. null if none. Note: not all
     * projectiles keep track of shooter.
     *
     * @return
     */
    public BaseEntity getShooter() {
        net.canarymod.api.entity.Entity me = getRecodeHandle();
        if (me instanceof net.canarymod.api.entity.throwable.EntityThrowable) {
            return Wrapper.wrap(((net.canarymod.api.entity.throwable.EntityThrowable) me).getThrower());
        } else if (me instanceof net.canarymod.api.entity.Arrow) {
            return Wrapper.wrap(((net.canarymod.api.entity.Arrow) me).getOwner());
        } else if (me instanceof Fireball) {
            return Wrapper.wrap(((Fireball) me).getOwner());
        }
        return null;
    }

    /**
     * Sets the shooter of this projectile. Note: not all projectiles keep track
     * of shooter.
     *
     * @return whether or not the operation was successful
     */
    public boolean setShooter(LivingEntity shooter) {
        Entity me = getEntity();
        if (me instanceof EntityThrowable) {
            // SRG Util.<EntityLivingBase>setField(EntityThrowable.class, getEntity(), "field_70192_c", shooter.getEntity());
            Util.<EntityLivingBase>setField(EntityThrowable.class, getEntity(), "g", shooter.getEntity());
            return true;
        } else if (me instanceof EntityArrow) {
            // SRG ((EntityArrow) me).field_70250_c = shooter.getEntity();
            ((EntityArrow) me).c = shooter.getEntity();
            return true;
        } else if (me instanceof EntityFireball) {
            // SRG ((EntityFireball) me).field_70235_a = shooter.getEntity();
            ((EntityFireball) me).a = shooter.getEntity();
            return true;
        }
        return false;
    }

    /**
     * Aims this projectile at a location. May not work so well on anything
     * that's not an instance of OIProjectile.
     *
     * @param x The x coordinate at which to aim it.
     * @param y The y coordinate at which to aim it.
     * @param z The z coordinate at which to aim it.
     * @param power The power that it will be fired at.
     * @param inaccuracy The inaccuracy with which it will be fired.
     */
    public void aimAt(double x, double y, double z, float power, float inaccuracy) { //pretty much copied directly from EntityArrow.c()
        if (getEntity() instanceof IProjectile) { // We have a method to do exactly this!
            ((IProjectile) getEntity()).c(x - getX(), y - getY(), z - getZ(), power, inaccuracy);
        } else {
            Random random = new Random();
            Fireball fireball = (Fireball) getRecodeHandle();

            // Turn into vector
            x -= getX();
            y -= getY();
            z -= getZ();

            // Convert to unit vector
            double vecLen = Math.sqrt(x * x + y * y + z * z);
            x /= vecLen;
            y /= vecLen;
            z /= vecLen;

            // Add inaccuracy
            x += random.nextGaussian() * (random.nextBoolean() ? -1 : 1) * 0.0075 * inaccuracy;
            y += random.nextGaussian() * (random.nextBoolean() ? -1 : 1) * 0.0075 * inaccuracy;
            z += random.nextGaussian() * (random.nextBoolean() ? -1 : 1) * 0.0075 * inaccuracy;
            // Add power
            x *= power;
            y *= power;
            z *= power;

            // Set acceleration!
            fireball.setAccelerationX(x);
            fireball.setAccelerationY(y);
            fireball.setAccelerationZ(z);
        }
    }

    /**
     * Aims this projectile at a location. May not work so well on anything
     * that's not an instance of OIProjectile.
     *
     * @param location The location at which to aim it.
     * @param power The power that it will be fired at.
     * @param inaccuracy The inaccuracy with which it will be fired.
     */
    public void aimAt(Location location, float power, float inaccuracy) {
        aimAt(location.x, location.y, location.z, power, inaccuracy);
    }
}
