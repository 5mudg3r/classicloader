package com.minecraftonline.classicloader.classicapi;

import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.tileentity.TileEntityBrewingStand;

/**
 * Interface to brewing stands.
 *
 * @author Willem Mulder (14mRh4X0r)
 */
@RecodeProxy(net.canarymod.api.world.blocks.BrewingStand.class)
public class BrewingStand extends BaseContainerBlock<TileEntityBrewingStand> {

    public BrewingStand(TileEntityBrewingStand brewingStand) {
        this(null, brewingStand);
    }

    public BrewingStand(net.minecraft.inventory.Container oContainer, TileEntityBrewingStand brewingStand) {
        super(oContainer, brewingStand, "Brewing Stand");
    }

    public BrewingStand(net.canarymod.api.world.blocks.BrewingStand brewingStand) {
        super(brewingStand, "Brewing Stand");
    }

    /**
     * Returns the time left to brew for. Range is from 0 to 400.
     *
     * @return this stand's brew time
     */
    public int getBrewTime() {
        // SRG return tileEntity.func_145935_i();
        return tileEntity.i();
    }

    @Override
    public net.canarymod.api.world.blocks.BrewingStand getRecodeHandle() {
        return tileEntity.getCanaryBrewingStand();
    }
}
