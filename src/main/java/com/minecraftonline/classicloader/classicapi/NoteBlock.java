package com.minecraftonline.classicloader.classicapi;

import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.tileentity.TileEntityNote;

/**
 * Class used for interfacing with tileEntity blocks.
 *
 * @author 14mRh4X0r
 */
@RecodeProxy(net.canarymod.api.world.blocks.NoteBlock.class)
public class NoteBlock extends ComplexBlockBase<TileEntityNote> {

    public NoteBlock(TileEntityNote note) {
        super(note);
    }

    public NoteBlock(net.canarymod.api.world.blocks.NoteBlock noteBlock) {
        super(noteBlock);
    }

    /**
     * Returns the current pitch of the tileEntity block.
     *
     * @return current pitch
     */
    public byte getNote() {
        return getRecodeHandle().getNote();
    }

    /**
     * Sets the pitch of the tileEntity block to a given value.
     *
     * @param pitch The new pitch
     */
    public void setNote(byte pitch) {
        getRecodeHandle().setNote(pitch);
    }

    @Override
    public net.canarymod.api.world.blocks.NoteBlock getRecodeHandle() {
        return (net.canarymod.api.world.blocks.NoteBlock) super.getRecodeHandle();
    }
}
