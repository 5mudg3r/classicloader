package com.minecraftonline.classicloader.classicapi;

import net.canarymod.Canary;
import net.canarymod.api.PlayerListEntry;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/**
 * PlayerlistEntry.java - Object for passing to the onPlayerlistEntryGet Hook
 *
 * @author Talmor
 */
@RecodeProxy(PlayerListEntry.class)
public class PlayerlistEntry {

    private PlayerListEntry entry;

    public PlayerlistEntry(String name, int ping, boolean show) {
        super();
        this.entry = new PlayerListEntry(name, ping, show);
    }

    public PlayerlistEntry(PlayerListEntry entry) {
        this.entry = entry;
    }

    public boolean isShow() {
        return entry.isShown();
    }

    public void setShow(boolean show) {
        entry.setShown(show);
    }

    public String getName() {
        return entry.getName();
    }

    public void setName(String name) {
        entry.setName(name);
    }

    public int getPing() {
        return entry.getPing();
    }

    public void setPing(int ping) {
        entry.setPing(ping);
    }

    public PlayerListEntry getRecodeHandle() {
        return entry;
    }
}
