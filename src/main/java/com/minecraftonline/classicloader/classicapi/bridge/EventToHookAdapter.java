/*
 * Copyright (C) 2013 Willem Mulder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.classicloader.classicapi.bridge;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map.Entry;
import lombok.Lombok;
import lombok.NonNull;

import net.canarymod.api.inventory.CustomStorageInventory;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.Container;

import net.canarymod.Canary;
import net.canarymod.api.entity.living.EntityLiving;
import net.canarymod.api.entity.living.LivingBase;
import net.canarymod.api.entity.living.animal.Cow;
import net.canarymod.api.entity.living.humanoid.CanaryPlayer;
import net.canarymod.api.entity.living.humanoid.Player;
import net.canarymod.api.entity.vehicle.Boat;
import net.canarymod.api.entity.vehicle.Minecart;
import net.canarymod.api.entity.vehicle.Vehicle;
import net.canarymod.api.inventory.AnimalInventory;
import net.canarymod.api.inventory.Enchantment;
import net.canarymod.api.world.World;
import net.canarymod.api.world.WorldManager;
import net.canarymod.api.world.blocks.Anvil;
import net.canarymod.api.world.blocks.BlockType;
import net.canarymod.api.world.position.Vector3D;
import net.canarymod.hook.HookHandler;
import net.canarymod.hook.command.*;
import net.canarymod.hook.entity.*;
import net.canarymod.hook.player.*;
import net.canarymod.hook.world.*;
import net.canarymod.plugin.PluginListener;
import net.canarymod.plugin.Priority;
import net.canarymod.tasks.ServerTask;

import com.minecraftonline.classicloader.ClassicLoader;
import com.minecraftonline.classicloader.Util;
import com.minecraftonline.classicloader.classicapi.BaseEntity;
import com.minecraftonline.classicloader.classicapi.Block;
import com.minecraftonline.classicloader.classicapi.HookParametersAnvilUse;
import com.minecraftonline.classicloader.classicapi.HookParametersChat;
import com.minecraftonline.classicloader.classicapi.HookParametersCloseInventory;
import com.minecraftonline.classicloader.classicapi.HookParametersConnect;
import com.minecraftonline.classicloader.classicapi.HookParametersDamage;
import com.minecraftonline.classicloader.classicapi.HookParametersDisconnect;
import com.minecraftonline.classicloader.classicapi.HookParametersEnchant;
import com.minecraftonline.classicloader.classicapi.HookParametersLogincheck;
import com.minecraftonline.classicloader.classicapi.HookParametersOpenInventory;
import com.minecraftonline.classicloader.classicapi.HookParametersSlotClick;
import com.minecraftonline.classicloader.classicapi.HookParametersSlotClick.AllowClick;
import com.minecraftonline.classicloader.classicapi.Inventory;
import com.minecraftonline.classicloader.classicapi.Item;
import com.minecraftonline.classicloader.classicapi.Location;
import com.minecraftonline.classicloader.classicapi.Mob;
import com.minecraftonline.classicloader.classicapi.PlayerlistEntry;
import com.minecraftonline.classicloader.classicapi.PluginLoader;
import com.minecraftonline.classicloader.classicapi.PluginLoader.Hook;
import com.minecraftonline.classicloader.classicapi.PluginLoader.HookResult;
import com.minecraftonline.classicloader.classicapi.PotionEffect;
import com.minecraftonline.classicloader.classicapi.Wrapper;


/**
 * Adapter to translate recode-type events to classic-type hooks
 * @author Willem Mulder
 */
public class EventToHookAdapter implements PluginListener {
    private final PluginLoader loader;
    
    public EventToHookAdapter(@NonNull PluginLoader loader) {
        this.loader = loader;

        Canary.getServer().addSynchronousTask(new TickHooks());
    }

    @HookHandler(ignoreCanceled = true)
    public void handlePreConnecting(PreConnectionHook event) {
        HookParametersLogincheck params = new HookParametersLogincheck(event.getWorld(), event.getName(), event.getIp());

        params.setKickReason(event.getKickReason());
        params.setWorldName(event.getWorld());

        params = (HookParametersLogincheck) loader.callHook(Hook.LOGINCHECK, params);

        event.setKickReason(params.getKickReason());
        event.setWorld(params.getWorldName());
    }

    @HookHandler(ignoreCanceled = true)
    public void handleConnecting(ConnectionHook event) {
        HookParametersConnect params = new HookParametersConnect(true, event.getPlayer().getName());
        params.setHidden(event.isHidden());
        params = (HookParametersConnect) loader.callHook(Hook.PLAYER_CONNECT, event.getPlayer(), params);
        if (params.getCustomMessage() != null) {
            event.setMessage(params.getCustomMessage());
        }
        event.setHidden(params.isHidden());

        final Player player = event.getPlayer();
        final HookParametersConnect finalParams = params;

        // Call LOGIN on next tick, at this point potion effects aren't initialized yet.
        Canary.getServer().addSynchronousTask(new ServerTask(ClassicLoader.getInstance(), 0) {

            @Override
            public void run() {
                if (!finalParams.applyPotionsEffects()) {
                    player.removeAllPotionEffects();
                }

                loader.callHook(Hook.LOGIN, Wrapper.wrap(player));
            }
        });
    }

    @HookHandler(ignoreCanceled = true)
    public void handleChat(ChatHook event) {
        String format = event.getFormat();
        format = format.substring(0, format.indexOf("%message"));
        for (Entry<String, String> entry : event.getPlaceholderMapping().entrySet()) {
            format = format.replace(entry.getKey(), entry.getValue());
        }
        @SuppressWarnings("ReplaceStringBufferByString")
        StringBuilder prefix = new StringBuilder(format);

        try {
            HookParametersChat params = new HookParametersChat(Wrapper.wrap(event.getPlayer()), prefix,
                    new StringBuilder(event.getMessage()),
                    Wrapper.<Player,com.minecraftonline.classicloader.classicapi.Player>wrapList(
                            event.getReceiverList(), net.canarymod.api.entity.living.humanoid.Player.class));

            params = (HookParametersChat) loader.callHook(Hook.CHAT, params);

            event.setReceiverList(new ArrayList<Player>(Wrapper.<Player,com.minecraftonline.classicloader.classicapi.Player>
                    unwrapList(params.getReceivers(), com.minecraftonline.classicloader.classicapi.Player.class)));
        } catch (NoSuchMethodException e) {
            throw Lombok.sneakyThrow(e);
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleCommand(PlayerCommandHook event) {
        if ((Boolean) loader.callHook(Hook.COMMAND, Wrapper.wrap(event.getPlayer()), event.getCommand())) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleConsoleCommand(ConsoleCommandHook event) {
        if ((Boolean) loader.callHook(Hook.SERVERCOMMAND, (Object) event.getCommand())) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleBan(BanHook event) {
        Object[] params = new Object[]{Wrapper.wrap(event.getModerator()), Wrapper.wrap(event.getBannedPlayer()),
                                       event.getReason()};
        if (event.isIpBan()) {
            loader.callHook(Hook.IPBAN, params);
        } else {
            loader.callHook(Hook.BAN, params);
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleKick(KickHook event) {
        com.minecraftonline.classicloader.classicapi.Player mod = event.getModerator() instanceof Player ?
                                                            Wrapper.wrap((Player) event.getModerator()) : null;
        loader.callHook(Hook.KICK, mod, Wrapper.wrap(event.getKickedPlayer()), event.getReason());
    }

    @HookHandler(ignoreCanceled = true)
    public void handleBlockRightClick(BlockRightClickHook event) {
        net.canarymod.api.world.blocks.Block clicked = event.getBlockClicked();
        Player player = event.getPlayer();

        Object[] params = new Object[]{Wrapper.wrap(player), new Block(clicked),
                                       player.getItemHeld() == null ? new Item(Item.Type.Air)
                                                                    : Wrapper.wrap(player.getItemHeld())};

        if ((Boolean) loader.callHook(Hook.BLOCK_RIGHTCLICKED, params)) {
            event.setCanceled();
        }

        Block placed = new Block(clicked.getFacingBlock(clicked.getFaceClicked()));
        loader.callHook(Hook.BLOCK_CREATED, params[0], placed, params[1], ((Item) params[2]).getItemId());
    }

    @HookHandler(ignoreCanceled = true)
    public void handleBlockLeftClick(BlockLeftClickHook event) {
        if ((Boolean) loader.callHook(Hook.BLOCK_DESTROYED, Wrapper.wrap(event.getPlayer()),
                new com.minecraftonline.classicloader.classicapi.Block(event.getBlock()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleDisconnect(DisconnectionHook event) {
        com.minecraftonline.classicloader.classicapi.Player player = Wrapper.wrap(event.getPlayer());
        loader.callHook(Hook.DISCONNECT, player);

        HookParametersDisconnect params = new HookParametersDisconnect(null, event.getPlayer().getName(), event.getReason());
        params.setHidden(event.isHidden());
        params = (HookParametersDisconnect) loader.callHook(Hook.PLAYER_DISCONNECT, player, params);
        if (params.getCustomMessage() != null) {
            event.setLeaveMessage(params.getCustomMessage());
        }
        event.setHidden(params.isHidden());
        event.setReason(params.getReason());
    }

    @HookHandler(ignoreCanceled = true)
    public void handlePlayerMove(PlayerMoveHook event) {
        loader.callHook(Hook.PLAYER_MOVE, Wrapper.wrap(event.getPlayer()), new Location(event.getFrom()),
                                          new Location(event.getTo()));
    }

    @HookHandler(ignoreCanceled = true)
    public void handleArmSwing(PlayerArmSwingHook event) {
        loader.callHook(Hook.ARM_SWING, Wrapper.wrap(event.getPlayer()));
    }

    @HookHandler(ignoreCanceled = true)
    public void handleItemDrop(ItemDropHook event) {
        if ((Boolean) loader.callHook(Hook.ITEM_DROP, Wrapper.wrap(event.getPlayer()), Wrapper.wrap(event.getItem()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleItemPickup(ItemPickupHook event) {
        if ((Boolean) loader.callHook(Hook.ITEM_PICK_UP, Wrapper.wrap(event.getPlayer()), Wrapper.wrap(event.getItem()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleItemTouchGround(ItemTouchGroundHook event) {
        if ((Boolean) loader.callHook(Hook.ITEM_TOUCH_GROUND, Wrapper.wrap(event.getEntityItem()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleTeleport(TeleportHook event) {
        if ((Boolean) loader.callHook(Hook.TELEPORT, Wrapper.wrap(event.getPlayer()),
                new Location(event.getPlayer().getLocation()), new Location(event.getDestination()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleBlockBroken(BlockDestroyHook event) {
        if ((Boolean) loader.callHook(Hook.BLOCK_BROKEN, Wrapper.wrap(event.getPlayer()), new Block(event.getBlock()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleIgnite(IgnitionHook event) {
        Block block = new Block(event.getBlock());
        block.setStatus(event.getCause().ordinal()); // Set status as CanaryMod no longer uses them
        if ((Boolean) loader.callHook(Hook.IGNITE, block, Wrapper.wrap(event.getPlayer()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleFlow(FlowHook event) {
        if ((Boolean) loader.callHook(Hook.FLOW, new Block(event.getBlockFrom()), new Block(event.getBlockTo()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    @SuppressWarnings("deprecation")
    public void handleExplode(ExplosionHook event) {
        Block source = new Block(event.getBlock());
        BaseEntity entity = Wrapper.wrap(event.getEntity());
        HashSet<Block> affected = new HashSet<Block>(event.getAffectedBlocks().size());

        for (net.canarymod.api.world.blocks.Block b : event.getAffectedBlocks()) {
            affected.add(new Block(b));
        }
        
        if ((Boolean) loader.callHook(Hook.EXPLODE, source, entity == null ? null : entity.getEntity(), affected)) {
            event.setCanceled();
        }
        
        ArrayList<Block> affectedList = new ArrayList<Block>(affected.size());
        for (Block b : affected) {
            affectedList.add(b);
        }
        
        if ((Boolean) loader.callHook(Hook.EXPLOSION, source, entity, affectedList)) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleEntitySpawn(EntitySpawnHook event) {
        if (event.getEntity() instanceof EntityLiving && (Boolean) loader.callHook(Hook.MOB_SPAWN,
                new Mob((EntityLiving) event.getEntity()))) {
            event.setCanceled();
        } else if (event.getEntity() instanceof Vehicle && !event.getEntity().isAnimal()
                && (Boolean) loader.callHook(Hook.VEHICLE_CREATE, Wrapper.wrap((Vehicle) event.getEntity()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleDamage(DamageHook event) {
        // Fire ATTACK before DAMAGE, bail if ATTACK returns true
        if (event.getAttacker() != null && event.getAttacker().isLiving() && event.getDefender().isLiving()
                && (Boolean) loader.callHook(Hook.ATTACK, Wrapper.wrap((LivingBase) event.getAttacker()),
                                                          Wrapper.wrap((LivingBase) event.getDefender()),
                                                          event.getDamageDealt())) {
            event.setCanceled();
            return;
        }

        HookParametersDamage hook = new HookParametersDamage(Wrapper.wrap(event.getAttacker()),
                Wrapper.wrap(event.getDefender()), Wrapper.wrap(event.getDamageSource()), event.getDamageDealt());

        hook = (HookParametersDamage) loader.callHook(Hook.DAMAGE, hook);

        event.setDamageDealt(hook.getDamageAmountFloat());
        event.setDamageSource(hook.getDamageSource().getRecodeHandle());

        if (hook.isCanceled()) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleHealthChange(HealthChangeHook event) {
        if ((Boolean) loader.callHook(Hook.HEALTH_CHANGE, Wrapper.wrap(event.getPlayer()), event.getOldValue(),
                                                          event.getNewValue())) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleRedstoneChange(RedstoneChangeHook event) {
        BlockType type = event.getSourceBlock().getType();
        if (type.equals(BlockType.Lever) || type.equals(BlockType.StonePlate) || type.equals(BlockType.WoodPlate)
                || type.equals(BlockType.LightWeightedPressurePlate) || type.equals(BlockType.HeavyWeightedPressurePlate)
                || type.equals(BlockType.RedstoneWire) || type.equals(BlockType.StoneButton)
                || type.equals(BlockType.WoodenButton) || type.equals(BlockType.RedstoneTorchOn)) {

            int newLevel = (Integer) loader.callHook(Hook.REDSTONE_CHANGE, new Block(event.getSourceBlock()),
                                                                           event.getOldLevel(), event.getNewLevel());

            if (newLevel == event.getOldLevel()) {
                event.setCanceled();
            } else if (newLevel != event.getNewLevel()
                    && (type.equals(BlockType.HeavyWeightedPressurePlate)
                        || type.equals(BlockType.LightWeightedPressurePlate)
                        || type.equals(BlockType.RedstoneWire))) {
                // Set status flag so we know we need to handle it.
                event.getSourceBlock().setStatus((byte) (event.getSourceBlock().getStatus() | 0x80));
                Util.setField(RedstoneChangeHook.class, event, "newLevel", newLevel); // Set new level.
            }
        }
    }

    @HookHandler(ignoreCanceled = true, priority = Priority.PASSIVE) // Sorry for the abuse
    public void handleCustomRedstoneChange(final RedstoneChangeHook event) {
        final net.canarymod.api.world.blocks.Block block = event.getSourceBlock();
        if ((event.getSourceBlock().getStatus() & 0x80) != 0) {
            Canary.getServer().addSynchronousTask(new ServerTask(ClassicLoader.getInstance(), 0) {
                @Override
                public void run() {
                    if (!block.getWorld().getBlockAt(block.getX(), block.getY(), block.getZ()).getType().equals(block.getType())) {
                        return; // Block is gone
                    }
                    block.setData((short) event.getNewLevel());
                }
            });
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handlePistonExtend(PistonExtendHook event) {
        if ((Boolean) loader.callHook(Hook.PISTON_EXTEND, new Block(event.getPiston()),
                                                          event.getPiston().getType().equals(BlockType.StickyPiston))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handlePistonRetract(PistonRetractHook event) {
        if ((Boolean) loader.callHook(Hook.PISTON_RETRACT, new Block(event.getPiston()),
                                                           event.getPiston().getType().equals(BlockType.StickyPiston))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleBlockPhysics(BlockPhysicsHook event) {
        if ((Boolean) loader.callHook(Hook.BLOCK_PHYSICS, new Block(event.getBlock()), event.wasPlaced())) {
            event.setCanceled();
        }
    }
    // VEHICLE_CREATE is up there with MOB_SPAWN
    // TODO VEHICLE_UPDATE
    @HookHandler(ignoreCanceled = true)
    public void handleVehicleDamage(VehicleDamageHook event) {
        if (event.getVehicle().isAnimal()) {
            return;
        }

        if ((Boolean) loader.callHook(Hook.VEHICLE_DAMAGE, Wrapper.wrap(event.getVehicle()),
                                                           Wrapper.wrap(event.getAttacker()), event.getDamageDealt())) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleVehicleCollision(VehicleCollisionHook event) {
        if (event.getVehicle().isAnimal()) {
            return;
        }

        if ((Boolean) loader.callHook(Hook.VEHICLE_COLLISION, Wrapper.wrap(event.getVehicle()),
                                                              Wrapper.wrap(event.getEntity()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleVehicleDestroy(VehicleDestroyHook event) {
        if (event.getVehicle().isAnimal()) {
            return;
        }

        loader.callHook(Hook.VEHICLE_DESTROYED, Wrapper.wrap(event.getVehicle()));
    }

    @HookHandler(ignoreCanceled = true)
    public void handleVehicleEnter(VehicleEnterHook event) {
        if (event.getEntity().isPlayer() && !event.getVehicle().isAnimal()) {
            loader.callHook(Hook.VEHICLE_ENTERED, Wrapper.wrap(event.getVehicle()), Wrapper.wrap((Player) event.getEntity()));
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleVehicleMove(VehicleMoveHook event) {
        if (event.getVehicle().isAnimal()) {
            return;
        }

        Vector3D newpos = event.getTo();
        loader.callHook(Hook.VEHICLE_POSITIONCHANGE, Wrapper.wrap(event.getVehicle()), newpos.getBlockX(),
                                                     newpos.getBlockY(), newpos.getBlockZ());
    }

    @HookHandler(ignoreCanceled = true)
    public void handleItemUse(ItemUseHook event) {
        net.canarymod.api.world.blocks.Block clicked = event.getBlockClicked();

        if ((Boolean) loader.callHook(Hook.ITEM_USE, Wrapper.wrap(event.getPlayer()),
                                                     clicked.getFaceClicked() == null ? null
                                                             : new Block(clicked.getFacingBlock(clicked.getFaceClicked())),
                                                     new Block(clicked), Wrapper.wrap(event.getItem()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleBlockPlaced(BlockPlaceHook event) {
        if ((Boolean) loader.callHook(Hook.BLOCK_PLACE, Wrapper.wrap(event.getPlayer()),
                                                        new Block(event.getBlockPlaced()),
                                                        new Block(event.getBlockClicked()),
                                                        Wrapper.wrap(event.getPlayer().getItemHeld()))) {
            event.setCanceled();
        }
    }
    // BLOCK_RIGHTCLICKED is up with BLOCK_CREATED
    @HookHandler(ignoreCanceled = true)
    public void handleLiquidDestroy(LiquidDestroyHook event) {
        // We already ignore canceled!
        HookResult currentState = event.isForceDestroy() ? HookResult.ALLOW_ACTION : HookResult.DEFAULT_ACTION;
        HookResult result = (HookResult) loader.callHook(Hook.LIQUID_DESTROY, (int) event.getLiquidType().getId(),
                                                                              new Block(event.getBlock()));
        if (currentState == HookResult.DEFAULT_ACTION) {
            event.setForceDestroy(result == HookResult.ALLOW_ACTION);
            if (result == HookResult.PREVENT_ACTION) {
                event.setCanceled();
            }
        }
    }
    // ATTACK is up with DAMAGE
    @HookHandler(ignoreCanceled = true)
    public void handleInventory(InventoryHook event) {
        if (event.getInventory() instanceof AnimalInventory || event.getInventory() instanceof CustomStorageInventory) {
            return;
        }
        Inventory inventory = (Inventory) Wrapper.wrap(event.getInventory());
        com.minecraftonline.classicloader.classicapi.Player player = Wrapper.wrap(event.getPlayer());
        if (event.isClosing()) {
            loader.callHook(Hook.CLOSE_INVENTORY, new HookParametersCloseInventory(player, inventory, false));
        } else {
            HookParametersOpenInventory params = new HookParametersOpenInventory(player, inventory, false);
            if ((Boolean) loader.callHook(Hook.OPEN_INVENTORY, params)) {
                event.setCanceled();
            }
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleSignShow(SignShowHook event) {
        loader.callHook(Hook.SIGN_SHOW, Wrapper.wrap(event.getPlayer()), Wrapper.wrap(event.getSign()));
    }

    @HookHandler(ignoreCanceled = true)
    public void handleSignChange(SignChangeHook event) {
        if ((Boolean) loader.callHook(Hook.SIGN_CHANGE, Wrapper.wrap(event.getPlayer()), Wrapper.wrap(event.getSign()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleLeafDecay(LeafDecayHook event) {
        if ((Boolean) loader.callHook(Hook.LEAF_DECAY, new Block(event.getBlock()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleEntityTame(EntityTameHook event) {
        HookResult result = (HookResult) loader.callHook(Hook.TAME, Wrapper.wrap(event.getPlayer()),
                                                                    Wrapper.wrap(event.getAnimal()), event.isTamed());
        if (result == HookResult.ALLOW_ACTION) {
            event.setTamed(true);
        } else if (result == HookResult.PREVENT_ACTION) {
            event.setTamed(false);
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleEntityLightningStruck(EntityLightningStruckHook event) {
        if ((Boolean) loader.callHook(Hook.LIGHTNING_STRIKE, Wrapper.wrap(event.getStruckEntity()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleWeatherChange(WeatherChangeHook event) {
        if (event.isThunderChange()) {
            if ((Boolean) loader.callHook(Hook.THUNDER_CHANGE, Wrapper.wrap(event.getWorld()), event.turningOn())) {
                event.setCanceled();
            }
        } else if ((Boolean) loader.callHook(Hook.WEATHER_CHANGE, Wrapper.wrap(event.getWorld()), event.turningOn())) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handlePortalUse(PortalUseHook event) {
        if ((Boolean) loader.callHook(Hook.PORTAL_USE, Wrapper.wrap(event.getPlayer()), new Location(event.getTo()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleChunkCreation(ChunkCreationHook event) {
        byte[] blocksByte = (byte[]) loader.callHook(Hook.CHUNK_CREATE, event.getX(), event.getZ(),
                                                                        Wrapper.wrap(event.getWorld()));
        if (blocksByte == null) {
            return;
        }

        int[] blocks = new int[blocksByte.length];
        for (int i = 0; i < blocksByte.length; i++) {
            blocks[i] = blocksByte[i];
        }
        event.setBlockData(blocks);
    }

    // TODO spawnpoint hook

    @HookHandler(ignoreCanceled = true)
    public void handleChunkCreated(ChunkCreatedHook event) {
        loader.callHook(Hook.CHUNK_CREATED, Wrapper.wrap(event.getChunk()));
    }

    @HookHandler(ignoreCanceled = true)
    public void handleChunkLoaded(ChunkLoadedHook event) {
        loader.callHook(Hook.CHUNK_LOADED, Wrapper.wrap(event.getChunk()));
    }

    @HookHandler(ignoreCanceled = true)
    public void handleChunkUnload(ChunkUnloadHook event) {
        loader.callHook(Hook.CHUNK_UNLOAD, Wrapper.wrap(event.getChunk()));
    }

    @HookHandler(ignoreCanceled = true)
    public void handleTimeChange(TimeChangeHook event) {
        if ((Boolean) loader.callHook(Hook.TIME_CHANGE, Wrapper.wrap(event.getWorld()), event.getTime())) {
            event.setCanceled();
        }
    }
    // Skip COMMAND_CHECK here, only check classic stuff.
    private Block[][] toClassicBlockSet(net.canarymod.api.world.blocks.Block[][] blockSet) {
        Block[][] classicBlockSet = new Block[blockSet.length][];
        for (int i = 0; i < blockSet.length; i++) {
            classicBlockSet[i] = new Block[blockSet[i].length];
            for (int j = 0; j < blockSet[i].length; j++) {
                classicBlockSet[i][j] = new Block(blockSet[i][j]);
            }
        }

        return classicBlockSet;
    }

    @HookHandler(ignoreCanceled = true)
    public void handlePortalCreate(PortalCreateHook event) {
        Block[][] blockSet = toClassicBlockSet(event.getBlockSet());
        if ((Boolean) loader.callHook(Hook.PORTAL_CREATE, (Object) blockSet)) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handlePortalDestroy(PortalDestroyHook event) {
        Block[][] blockSet = toClassicBlockSet(event.getBlockSet());
        if ((Boolean) loader.callHook(Hook.PORTAL_DESTROY, (Object) blockSet)) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handlePlayerRespawned(PlayerRespawnedHook event) {
        loader.callHook(Hook.PLAYER_RESPAWN, Wrapper.wrap(event.getPlayer()), new Location(event.getLocation()));
    }

    @HookHandler(ignoreCanceled = true)
    public void handleEntityDespawn(EntityDespawnHook event) {
        if ((Boolean) loader.callHook(Hook.ENTITY_DESPAWN, Wrapper.wrap(event.getEntity()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleEndermanPickup(EndermanPickupBlockHook event) {
        if ((Boolean) loader.callHook(Hook.ENDERMAN_PICKUP, Wrapper.wrap(event.getEnderman()), new Block(event.getBlock()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleEndermanDrop(EndermanDropBlockHook event) {
        if ((Boolean) loader.callHook(Hook.ENDERMAN_DROP, Wrapper.wrap(event.getEnderman()), new Block(event.getBlock()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleEntityInteract(EntityRightClickHook event) {
        // TODO fix this in CanaryMod (not called for some mobs)
        if (event.getEntity() instanceof Cow
                && (Boolean) loader.callHook(Hook.COW_MILK, Wrapper.wrap(event.getPlayer()), Wrapper.wrap((Cow) event.getEntity()))) {
            event.setCanceled();
            return;
        }

        HookResult res = (HookResult) loader.callHook(Hook.ENTITY_RIGHTCLICKED, Wrapper.wrap(event.getPlayer()),
                Wrapper.wrap(event.getEntity()), Wrapper.wrap(event.getPlayer().getItemHeld()));

        if (res != HookResult.DEFAULT_ACTION) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleEat(EatHook event) {
        if ((Boolean) loader.callHook(Hook.EAT, Wrapper.wrap(event.getPlayer()), Wrapper.wrap(event.getItem()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleFoodLevel(FoodLevelHook event) {
        event.setNewValue((Integer) loader.callHook(Hook.FOODLEVEL_CHANGE, Wrapper.wrap(event.getPlayer()),
                                                                           event.getOldValue(), event.getNewValue()));
    }

    @HookHandler(ignoreCanceled = true)
    public void handleFoodExhaustion(FoodExhaustionHook event) {
        event.setNewValue((Float) loader.callHook(Hook.FOODEXHAUSTION_CHANGE, Wrapper.wrap(event.getPlayer()),
                                                                              event.getOldValue(), event.getNewValue()));
    }

    @HookHandler(ignoreCanceled = true)
    public void handleFoodSaturation(FoodSaturationHook event) {
        event.setNewValue((Float) loader.callHook(Hook.FOODSATURATION_CHANGE, Wrapper.wrap(event.getPlayer()),
                                                                              event.getOldValue(), event.getNewValue()));
    }

    @HookHandler(ignoreCanceled = true)
    public void handlePotionEffectApplied(PotionEffectAppliedHook event) {
        PotionEffect effect = Wrapper.wrap(event.getPotionEffect());
        effect = (PotionEffect) loader.callHook(Hook.POTION_EFFECT, Wrapper.wrap(event.getEntity()), effect);
        event.setPotionEffect(effect.getRecodeHandle());
    }

    @HookHandler(ignoreCanceled = true)
    public void handlePotionEffectFinish(PotionEffectFinishHook event) {
        loader.callHook(Hook.POTION_EFFECTFINISHED, Wrapper.wrap(event.getEntity()), Wrapper.wrap(event.getPotionEffect()));
    }

    @HookHandler(ignoreCanceled = true)
    public void handleExperience(ExperienceHook event) {
        if ((Boolean) loader.callHook(Hook.EXPERIENCE_CHANGE, Wrapper.wrap(event.getPlayer()), event.getOldValue(),
                                                              event.getNewValue())) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleLevelUp(LevelUpHook event) {
        loader.callHook(Hook.LEVEL_UP, Wrapper.wrap(event.getPlayer()));
    }

    @HookHandler(ignoreCanceled = true)
    public void handlePlayerListEntry(PlayerListEntryHook event) {
        PlayerlistEntry entry = Wrapper.wrap(event.getEntry());
        loader.callHook(Hook.GET_PLAYERLISTENTRY, Wrapper.wrap(event.getEntry().getPlayer()), entry);
    }
    // PLAYER_CONNECT is up with LOGIN
    // PLAYER_DISCONNECT is up with DISCONNECT
    // ENTITY_RIGHTCLICKED is up with COW_MILK
    @HookHandler(ignoreCanceled = true)
    public void handleMobTarget(MobTargetHook event) {
        if ((Boolean) loader.callHook(Hook.MOB_TARGET, Wrapper.wrap(event.getTarget()), Wrapper.wrap(event.getEntity()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleBlockUpdate(BlockUpdateHook event) {
        if (event.getNewBlockId() != 0
                && (Boolean) loader.callHook(Hook.BLOCK_UPDATE, new Block(event.getBlock()), event.getNewBlockId())) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleEnchant(EnchantHook event) {
        try {
            HookParametersEnchant params = new HookParametersEnchant(Wrapper.wrap(event.getPlayer()), event.getItem().getId(),
                                                                     Wrapper.wrapList(event.getEnchantmentList(), Enchantment.class));
            params = (HookParametersEnchant) loader.callHook(Hook.ENCHANT, params);

            if (params.isCanceled()) {
                event.setCanceled();
            }
        } catch (NoSuchMethodException e) {
            throw Lombok.sneakyThrow(e);
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleDispense(DispenseHook event) {
        if ((Boolean) loader.callHook(Hook.DISPENSE, Wrapper.wrap(event.getDispenser()), Wrapper.wrap(event.getEntity()))) {
            event.setCanceled();
        }
    }

    // TODO light change hook

    @HookHandler(ignoreCanceled = true)
    public void handleEntityDeath(EntityDeathHook event) {
        loader.callHook(Hook.DEATH, Wrapper.wrap(event.getEntity()));
    }

    @HookHandler(ignoreCanceled = true)
    public void handleProjectileHit(ProjectileHitHook event) {
        if ((Boolean) loader.callHook(Hook.PROJECTILE_HIT, Wrapper.wrap(event.getProjectile()), Wrapper.wrap(event.getEntityHit()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleVillagerTrade(VillagerTradeHook event) {
        if ((Boolean) loader.callHook(Hook.VILLAGER_TRADE, Wrapper.wrap(event.getPlayer()), Wrapper.wrap(event.getVillager()),
                                                           Wrapper.wrap(event.getTrade()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleVillagerTradeUnlock(VillagerTradeUnlockHook event) {
        if ((Boolean) loader.callHook(Hook.VILLAGER_TRADE_UNLOCK, Wrapper.wrap(event.getVillager()), Wrapper.wrap(event.getTrade()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleAnvilUse(AnvilUseHook event) {
        // Prevent stack overflow
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        for (int i = 1; i < stackTrace.length; i++) {
            StackTraceElement ste = stackTrace[i];
            if (ste.getClassName().equals(EventToHookAdapter.class.getName()) && ste.getMethodName().equals("handleAnvilUse")) {
                return;
            }
        }

        Anvil anvil = event.getAnvil();
        //org.apache.logging.log4j.LogManager.getLogger().info(anvil.getResult());
        HookParametersAnvilUse params = new HookParametersAnvilUse(Wrapper.wrap(anvil), new Block(anvil.getBlock()));
        params = (HookParametersAnvilUse) loader.callHook(Hook.ANVIL_USE, params);

        anvil.setSlot(0, params.slotOne == null ? null : params.slotOne.getRecodeHandle());
        anvil.setSlot(1, params.slotTwo == null ? null : params.slotTwo.getRecodeHandle());
        anvil.setResult(params.result == null ? null : params.result.getRecodeHandle());
        anvil.setToolName(params.toolName);
        anvil.setXPCost(params.xpLevel);
    }

    @HookHandler(ignoreCanceled = true)
    public void handleFireworkExplode(FireworkExplodeHook event) {
        if ((Boolean) loader.callHook(Hook.FIREWORK_EXPLODE, Wrapper.wrap(event.getFireworkRocket()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleSlotClick(SlotClickHook event) {
        if (event.getInventory() instanceof AnimalInventory) {
            return;
        }

        // Hello, my API has a one-way mapping!
        int grabOption = -1;
        switch (event.getGrabMode()) {
            case DEFAULT:
                grabOption = 0;
                break;
            case SHIFT_CLICK:
                grabOption = 1;
                break;
            case HOVER_SWAP:
                grabOption = 2;
                break;
            case PICK_BLOCK:
                grabOption = 3;
                break;
            case DROP:
                grabOption = 4;
                break;
            case PAINT:
                grabOption = 5;
                break;
            case DOUBLE_CLICK:
                grabOption = 6;
        }

        int mouseClick = -1;
        switch (event.getButtonPress()) {
            case KEY_1:
            case LEFT_PAINT_START:
            case KEY_DROP:
            case LEFT:
                mouseClick = 0;
                break;
            case KEY_2:
            case LEFT_PAINT_PROGRESS:
            case CTRL_DROP:
            case RIGHT:
                mouseClick = 1;
                break;
            case KEY_3:
            case LEFT_PAINT_END:
            case MIDDLE:
                mouseClick = 2;
                break;
            case KEY_4:
                mouseClick = 3;
                break;
            case KEY_5:
            case RIGHT_PAINT_START:
                mouseClick = 4;
                break;
            case KEY_6:
            case RIGHT_PAINT_PROGRESS:
                mouseClick = 5;
                break;
            case KEY_7:
            case RIGHT_PAINT_END:
                mouseClick = 6;
                break;
            case KEY_8:
                mouseClick = 7;
                break;
            case KEY_9:
                mouseClick = 8;
                break;
        }
        
        EntityPlayerMP player = ((CanaryPlayer) event.getPlayer()).getHandle();

        net.canarymod.api.inventory.Inventory inv = event.getInventory();
        Container container = player.bo; // SRG 71070

        HookParametersSlotClick params = new HookParametersSlotClick(container, event.getSlotId(), mouseClick, grabOption, player);
        params.allowClick = event.doUpdate() ? AllowClick.ALLOW : AllowClick.CANCEL_WITHOUT_CLIENT_UPDATE;

        params = (HookParametersSlotClick) loader.callHook(Hook.SLOT_CLICK, params);

        if (params.allowClick != AllowClick.ALLOW) {
            event.setCanceled();
            event.setDoUpdate(params.allowClick == AllowClick.CANCEL);
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleCommandBlockCommand(CommandBlockCommandHook event) {
        if ((Boolean) loader.callHook(Hook.COMMAND_BLOCK_COMMAND, Wrapper.wrap(event.getCommandBlock()), event.getArguments())) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleStatGained(StatGainedHook event) {
        if ((Boolean) loader.callHook(Hook.STAT_GAINED, Wrapper.wrap(event.getPlayer()), Wrapper.wrap(event.getStat()))) {
            event.setCanceled();
        }
    }

    // TODO EntityDestroyHook 404

    @HookHandler(ignoreCanceled = true)
    public void handleHangingEntityDestroy(HangingEntityDestroyHook event) {
        if ((Boolean) loader.callHook(Hook.HANGING_ENTITY_DESTROYED, Wrapper.wrap(event.getPainting()),
                                                                     Wrapper.wrap(event.getDamageSource()))) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleHopperTransfer(HopperTransferHook event) {
        if ((Boolean) loader.callHook(Hook.HOPPER_TRANSFER, Wrapper.wrap(event.getHopper()),
                                                            Wrapper.wrap(event.getItemTransferred()), event.isInputting())) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handleMinecartActivate(MinecartActivateHook event) {
        if ((Boolean) loader.callHook(Hook.MINECART_ACTIVATE, Wrapper.wrap(event.getMinecart()), event.isPowered())) {
            event.setCanceled();
        }
    }

    private class TickHooks extends ServerTask {

        public TickHooks() {
            super(ClassicLoader.getInstance(), 0, true);
        }

        @Override
        public void run() {
            WorldManager wm = Canary.getServer().getWorldManager();
            for (World world : wm.getAllWorlds()) {
                for (Minecart minecart : world.getMinecartList()) {
                    loader.callHook(Hook.VEHICLE_UPDATE, Wrapper.wrap(minecart));
                }
                for (Boat boat : world.getBoatList()) {
                    loader.callHook(Hook.VEHICLE_UPDATE, Wrapper.wrap(boat));
                }
            }
        }

    }
}
