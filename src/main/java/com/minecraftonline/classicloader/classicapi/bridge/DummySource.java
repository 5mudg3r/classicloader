/*
 * Copyright (C) 2014 willem
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.minecraftonline.classicloader.classicapi.bridge;

import com.minecraftonline.classicloader.classicapi.Ban;
import com.minecraftonline.classicloader.classicapi.DataSource;
import com.minecraftonline.classicloader.classicapi.Group;
import com.minecraftonline.classicloader.classicapi.Kit;
import com.minecraftonline.classicloader.classicapi.Player;
import com.minecraftonline.classicloader.classicapi.Warp;
import java.util.Arrays;
import java.util.List;
import net.canarymod.Canary;
import net.canarymod.api.inventory.ItemType;

/**
 *
 * @author willem
 */
public class DummySource extends DataSource {

    @Override
    public void initialize() {
        loadItems();
        loadGroups();
    }

    @Override
    public void loadGroups() {
        while (Canary.usersAndGroups() == null) {
            Thread.yield();
        }

        synchronized (groupLock) {
            for (net.canarymod.user.Group g : Canary.usersAndGroups().getGroups()) {
                if (!g.isGlobal()) {
                    continue;
                }

                Group group = new Group();
                group.Administrator = g.isAdministratorGroup();
                group.CanModifyWorld = g.canBuild();
                group.Commands = new String[0];
                group.DefaultGroup = g.isDefaultGroup();
                group.IgnoreRestrictions = g.canIgnorerestrictions();

                List<net.canarymod.user.Group> parents = g.parentsToList();
                String[] inherited = new String[parents.size()];
                int i = 0;
                for (net.canarymod.user.Group parent : parents) {
                    inherited[i++] = parent.getName();
                }
                group.InheritedGroups = inherited;

                group.Name = g.getName();
                group.Prefix = g.getPrefix();
            }
        }
    }

    @Override
    public void loadKits() {
        // hurr.
    }

    @Override
    public void loadHomes() {
        // erm.
    }

    @Override
    public void loadWarps() {
        // hmm.
    }

    @Override
    public void loadItems() {
        synchronized (itemLock) {
            for (ItemType type : ItemType.values()) {
                items.put(type.getMachineName().replaceFirst("^minecraft:", ""), type.getId());
            }
        }
    }

    @Override
    public void loadBanList() {
        // Am I...
    }

    @Override
    public void loadMutedPlayers() {
        // supposed to...
    }

    @Override
    public void loadEnderBlocks() {
        // load the stuff
    }

    @Override
    public void loadAntiXRayBlocks() {
        // into DataSource?
    }

    @Override
    public void addPlayer(Player player) {
        // I sure hope not.
    }

    @Override
    public void modifyPlayer(Player player) {
        // That would be tiring.
    }

    @Override
    public boolean doesPlayerExist(String player) {
        return Arrays.asList(Canary.usersAndGroups().getPlayers()).contains(player);
    }

    @Override
    public Player getPlayer(String name) {
        return new Player(Canary.getServer().getOfflinePlayer(name));
    }

    @Override
    public void addGroup(Group group) {
        // hmm?
    }

    @Override
    public void modifyGroup(Group group) {
        // meh.
    }

    @Override
    public void addKit(Kit kit) {
        // nope.avi
    }

    @Override
    public void modifyKit(Kit kit) {
        // err, nope.
    }

    @Override
    public void addHome(Warp home) {
        // nope.
    }

    @Override
    public void changeHome(Warp home) {
        // nope nope nope.
    }

    @Override
    public void addWarp(Warp warp) {
        // not either.
    }

    @Override
    public void changeWarp(Warp warp) {
        // I told you, no.
    }

    @Override
    public void removeWarp(Warp warp) {
        // The hell I am.
    }

    @Override
    public void setPlayerToMuteList(String name) {
        // *sticks out tongue*
    }

    @Override
    public void removePlayerFromMuteList(String name) {
        // So much room for comments.
    }

    @Override
    public void addToWhitelist(String name) {
        // Are they witty?
    }

    @Override
    public void removeFromWhitelist(String name) {
        // Are they supposed to be?
    }

    @Override
    public boolean isUserOnWhitelist(String user) {
        return Canary.whitelist().isWhitelisted(user);
    }

    @Override
    public void addBan(Ban ban) {
        // No one knows.
    }

    @Override
    public void addToReserveList(String name) {
        // *guitar, guitar*
    }

    @Override
    public void removeFromReserveList(String name) {
        // Yup, that's Queens of the Stone Age
    }

    @Override
    public boolean isUserOnReserveList(String user) {
        return Canary.reservelist().isSlotReserved(user);
    }

    @Override
    public void expireBan(Ban ban) {
        // THE END!
    }
}
