package com.minecraftonline.classicloader.classicapi;

import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.tileentity.TileEntityFurnace;

/**
 * Furnace.java - Interface for furnaces
 *
 * @author 14mRh4X0r
 */
@RecodeProxy(net.canarymod.api.world.blocks.Furnace.class)
public class Furnace extends BaseContainerBlock<TileEntityFurnace> implements ComplexBlock {

    public Furnace(TileEntityFurnace furnace) {
        this(null, furnace);
    }

    public Furnace(net.minecraft.inventory.Container oContainer, TileEntityFurnace furnace) {
        super(oContainer, furnace, "Furnace");
    }

    public Furnace(net.canarymod.api.world.blocks.Furnace furnace) {
        super(furnace, "Furnace");
    }

    /**
     * Returns the number of ticks the current fuel item has to go.
     *
     * @return burn time ticks
     */
    public short getBurnTime() {
        return getRecodeHandle().getBurnTime();
    }

    /**
     * Sets the number of ticks the current fuel item has to go.
     *
     * @param time ticks of burning left
     */
    public void setBurnTime(short time) {
        getRecodeHandle().setBurnTime(time);
    }

    /**
     * Returns the number of ticks the item to smelt has smolten. An item is
     * ready on 200 ticks.
     *
     * @return cook time ticks
     */
    public short getCookTime() {
        return getRecodeHandle().getCookTime();
    }

    /**
     * Sets the number of ticks the item to smelt has smolten. An item is ready
     * on 200 ticks.
     *
     * @param time ticks of cooking
     */
    public void setCookTime(short time) {
        getRecodeHandle().setCookTime(time);
    }

    @Override
    public net.canarymod.api.world.blocks.Furnace getRecodeHandle() {
        return tileEntity.getCanaryFurnace();
    }
}
