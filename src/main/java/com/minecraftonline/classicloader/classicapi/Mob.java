package com.minecraftonline.classicloader.classicapi;

import ca.grimoire.critical.Todo;
import java.util.Map;
import net.canarymod.api.entity.living.CanaryLivingBase;
import net.canarymod.api.entity.living.EntityLiving;
import net.canarymod.api.entity.living.LivingBase;
import com.minecraftonline.classicloader.Util;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.IEntityLivingData;

/**
 * Mob.java - Interface for mobs
 *
 * @author James
 */
@RecodeProxy(EntityLiving.class)
public class Mob extends LivingEntity {

    /**
     * Creates a mob interface
     *
     * @param locallb name of mob
     */
    public Mob(net.minecraft.entity.EntityLiving locallb) {
        super(locallb);
    }

    /**
     * Creates a mob interface
     *
     * @param mob name of mob
     * @deprecated Use {@link #Mob(java.lang.String, World)} instead.
     */
    public Mob(String mob) {
        this(mob, etc.getServer().getDefaultWorld());
    }

    /**
     * Creates a mob interface
     *
     * @param mob name of the mob
     * @param world World for the mob
     */
    public Mob(String mob, World world) {
        // Gotta use EntityList since Lib uses their own names
        // SRG this((EntityLiving) EntityList.func_75620_a(mob, world.getWorld()).getCanaryEntity());
        this((EntityLiving) EntityList.a(mob, world.getWorld()).getCanaryEntity());
        // SRG this.getEntity().func_110161_a((IEntityLivingData) null);
        this.getEntity().a((IEntityLivingData) null);
    }

    /**
     * Creates a mob interface
     *
     * @param mobName name of mob
     * @param location location of mob
     */
    public Mob(String mobName, Location location) {
        this(mobName, location.getWorld());
        teleportTo(location);
    }

    public Mob(EntityLiving mob) {
        super(mob);
    }

    /**
     * Backwards compat.
     */
    public void spawn(Mob rider) {
        this.spawn((LivingEntity) rider);
    }

    /**
     * Returns the current target of the mob
     *
     * @return net.minecraft.server.Entity
     */
    public net.minecraft.entity.Entity getTarget() {
        CanaryLivingBase target = (CanaryLivingBase) getRecodeHandle().getAttackTarget();
        return target == null ? null : target.getHandle();
    }

    /**
     * Sets the mobs target
     *
     * @param target the entity to target
     */
    public void setTarget(net.minecraft.entity.Entity target) {
        if (!(target.getCanaryEntity() instanceof LivingBase)) {
            return;
        }
        getRecodeHandle().setAttackTarget((LivingBase) target.getCanaryEntity());
    }

    @Override
    public void setHealth(float health) {
        super.setHealth(health);
        if (health <= 0) {
            dropLoot();
        }
    }

    @Override
    @Deprecated
    public void setHealth(int health) {
        super.setHealth(health);
    }

    /**
     * Returns the actual mob
     *
     * @return
     */
    public net.minecraft.entity.EntityLiving getMob() {
        return getEntity();
    }

    /**
     * Checks to see if the mob is a valid mob
     *
     * @param mob the mob to check
     * @return true of mob is valid
     */
    public static boolean isValid(String mob) {
        // SRG return Util.getField(EntityList.class, null, "field_75625_b", Map.class).containsKey(mob);
        return Util.getField(EntityList.class, null, "c", Map.class).containsKey(mob);
    }

    @Todo("Pull request this")
    public boolean isInLove() {
        if (getEntity() instanceof net.minecraft.entity.passive.EntityAnimal) {
            // SRG return ((net.minecraft.entity.passive.EntityAnimal) getEntity()).func_70880_s();
            return ((net.minecraft.entity.passive.EntityAnimal) getEntity()).ce();
        }
        return false;
    }

    @Override
    public EntityLiving getRecodeHandle() {
        return super.getRecodeHandle(); //To change body of generated methods, choose Tools | Templates.
    }
}
