package com.minecraftonline.classicloader.classicapi;

import ca.grimoire.critical.FixMe;
import java.util.HashMap;
import java.util.Map;
import net.canarymod.Canary;
import net.canarymod.api.factory.PotionFactory;
import net.canarymod.api.potion.CanaryPotionEffect;
import com.minecraftonline.classicloader.Util;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/**
 * PotionEffect.java - Interface to potions Effects, (poison in 1.8.1).
 *
 * @author Talmor
 */
@RecodeProxy(net.canarymod.api.potion.PotionEffect.class)
public class PotionEffect {

    public final net.canarymod.api.potion.PotionEffect potionEffect;
    private static final PotionFactory factory = Canary.factory().getPotionFactory();

    public enum Type {

        MOVE_SPEED(1), //
        SLOW_DOWN(2), //
        DIG_SPEED(3), //
        DIG_SLOW(4), //
        DAMAGE_BOOST(5), //
        HEAL(6), //
        HARM(7), //
        JUMP(8), //
        CONFUSION(9), //
        REGENERATION(10), //
        RESISTANCE(11), //
        FIRE_RESISTANCE(12), //
        WATER_BREATHING(13), //
        INVISIBILITY(14), //
        BLINDNESS(15), //
        NIGHTVISION(16), //
        HUNGER(17), //
        WEAKNESS(18), //
        POISON(19),
        WITHER(20);
        private int id;
        private static Map<Integer, Type> map;

        @SuppressWarnings("LeakingThisInConstructor")
        Type(int id) {
            this.id = id;
            add(id, this);
        }

        private static void add(int id, Type name) {
            if (map == null) {
                map = new HashMap<Integer, Type>();
            }
            map.put(id, name);
        }

        public int getId() {
            return id;
        }

        public static Type fromId(final int id) {
            return map.get(id);
        }
    }

    /**
     * Instantiated this wrapper around OPotionEffect
     *
     * @param potionEffect the OPotionEffect to wrap
     */
    public PotionEffect(net.minecraft.potion.PotionEffect potionEffect) {
        this.potionEffect = new CanaryPotionEffect(potionEffect);
    }

    public PotionEffect(net.canarymod.api.potion.PotionEffect potionEffect) {
        this.potionEffect = potionEffect;
    }

    /**
     * Creates a new PotionEffect
     *
     * @param effect
     * @param amplifier
     * @param duration
     * @return
     */
    public static PotionEffect getNewPotionEffect(PotionEffect.Type effect, int amplifier, int duration) {
        return Wrapper.wrap(factory.newPotionEffect(effect.getId(), duration, amplifier));
    }

    /**
     * Creates a new PotionEffect
     *
     * @param tag the tag to get the potion effect's data from
     * @return
     */
    public static PotionEffect getNewPotionEffect(NBTTagCompound tag) {
        return Wrapper.wrap(factory.newPotionEffect(tag.getInt("Id"), tag.getInt("Duration"), tag.getInt("Amplifier"), tag.getBoolean("Ambient")));
    }

    public PotionEffect.Type getType() {
        return PotionEffect.Type.fromId(potionEffect.getPotionID());
    }

    public int getAmplifier() {
        return potionEffect.getAmplifier();
    }

    public int getDuration() {
        return potionEffect.getDuration();
    }

    @FixMe("Implement in API methods when available")
    public void setType(Type effect) {
        Util.<Integer>setField(net.minecraft.potion.PotionEffect.class,
                // SRG ((CanaryPotionEffect) potionEffect).getHandle(), "field_76462_a", effect.getId());
                ((CanaryPotionEffect) potionEffect).getHandle(), "a", effect.getId());
    }

    @FixMe("Implement in API methods when available")
    public void setDuration(int duration) {
        Util.<Integer>setField(net.minecraft.potion.PotionEffect.class,
                // SRG ((CanaryPotionEffect) potionEffect).getHandle(), "field_76460_b", duration);
                ((CanaryPotionEffect) potionEffect).getHandle(), "b", duration);
    }

    @FixMe("Implement in API methods when available")
    public void setAmplifier(int amplifier) {
        Util.<Integer>setField(net.minecraft.potion.PotionEffect.class,
                // SRG ((CanaryPotionEffect) potionEffect).getHandle(), "field_76461_c", amplifier);
                ((CanaryPotionEffect) potionEffect).getHandle(), "c", amplifier);
    }

    /**
     * Writes this potion effect's data to an NBTTagCompound
     *
     * @param tag the tag to write to
     * @return the same tag that was passed in, but rewrapped
     */
    @FixMe("Implement in API methods when available")
    public NBTTagCompound writeToTag(NBTTagCompound tag) {
        // SRG ((CanaryPotionEffect) potionEffect).getHandle().func_82719_a(tag.getBaseTag());
        ((CanaryPotionEffect) potionEffect).getHandle().a(tag.getBaseTag());
        return tag;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.potionEffect != null ? this.potionEffect.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PotionEffect other = (PotionEffect) obj;
        if (this.potionEffect != other.potionEffect && (this.potionEffect == null || !this.potionEffect.equals(other.potionEffect))) {
            return false;
        }
        return true;
    }

    /* RecodeProxy-mandated methods */
    static PotionEffect[] fromRecodeArray(net.canarymod.api.potion.PotionEffect[] recode) {
        if (recode == null) {
            return null;
        }
        PotionEffect[] classic = new PotionEffect[recode.length];
        for (int i = 0; i < recode.length; i++) {
            classic[i] = Wrapper.wrap(recode[i]);
        }
        return classic;
    }

    static net.canarymod.api.potion.PotionEffect[] toRecodeArray(PotionEffect[] classic) {
        if (classic == null) {
            return null;
        }
        net.canarymod.api.potion.PotionEffect[] recode = new net.canarymod.api.potion.PotionEffect[classic.length];
        for (int i = 0; i < classic.length; i++) {
            recode[i] = classic[i].getRecodeHandle();
        }
        return recode;
    }

    public net.canarymod.api.potion.PotionEffect getRecodeHandle() {
        return potionEffect;
    }
}
