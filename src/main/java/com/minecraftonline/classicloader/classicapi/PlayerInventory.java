package com.minecraftonline.classicloader.classicapi;

import ca.grimoire.critical.Todo;
import net.canarymod.api.inventory.CanaryPlayerInventory;
import com.minecraftonline.classicloader.classicapi.bridge.ContainerProxy;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.item.ItemStack;

@RecodeProxy(net.canarymod.api.inventory.PlayerInventory.class)
public class PlayerInventory extends ItemArray<ContainerProxy> {

    private final net.canarymod.api.inventory.PlayerInventory inv;

    // These first two constructors are to maintain backward compatibility
    public PlayerInventory(Player player) {
        this((HumanEntity) player);
    }

    public PlayerInventory(net.minecraft.inventory.Container oContainer, Player player) {
        this(oContainer, (HumanEntity) player);
    }

    public PlayerInventory(HumanEntity human) {
        this(human.getRecodeHandle().getInventory());
    }

    public PlayerInventory(net.minecraft.inventory.Container oContainer, HumanEntity human) {
        super(oContainer, ContainerProxy.of(human.getRecodeHandle().getInventory()));
        this.inv = human.getRecodeHandle().getInventory();
    }

    public PlayerInventory(net.canarymod.api.inventory.PlayerInventory inv) {
        super(ContainerProxy.of(inv));
        this.inv = inv;
    }

    /**
     * Give an item to this inventory. The amount that does not fit into the
     * inventory is dropped. This method takes enchantments into account.
     *
     * @param itemId The id of the item to give.
     * @param amount The amount of the item to give.
     * @see #insertItem(Item)
     */
    public void giveItem(int itemId, int amount) {
        Item toGive = new Item(itemId, amount);
        if (!inv.insertItem(toGive.getRecodeHandle())) {
            this.getHuman().giveItemDrop(itemId, amount);
        }
    }

    @Override
    public void update() {
        inv.update();
    }

    /**
     * Returns a String value representing this PlayerInventory
     *
     * @return String representation of this PlayerInventory
     */
    @Override
    public String toString() {
        return String.format("PlayerInventory[user=%s]", this.getHuman());
    }

    public HumanEntity getHuman() {
        return Wrapper.wrap(((CanaryPlayerInventory) inv).getHandle().d.getCanaryHuman());
    }

    /**
     * Returns the owner of this PlayerInventory
     *
     * @return Player
     */
    public Player getPlayer() {
        return getHuman() instanceof Player ? (Player) getHuman() : null;
    }

    @Override
    public String getName() {
        return inv.getInventoryName();
    }

    @Override
    public void setName(String value) {
        inv.setInventoryName(value);
    }

    /**
     * Returns Item held in player's mouse cursor. Ex: When moving items within
     * an inventory.
     *
     * @return Item
     */
    @Todo("Pull request this")
    public Item getCursorItem() {
        // SRG ItemStack itemstack = ((CanaryPlayerInventory) inv).getHandle().func_70445_o();
        ItemStack itemstack = ((CanaryPlayerInventory) inv).getHandle().o();
        return itemstack == null ? null : Wrapper.wrap(itemstack.getCanaryItem());
    }

    public void setCursorItem(Item item) {
        // SRG ((CanaryPlayerInventory) inv).getHandle().func_70437_b(item.getBaseItem());
        ((CanaryPlayerInventory) inv).getHandle().b(item.getBaseItem());
    }

    /**
     * Returns the highlighted slot on the quickbar (0-8).
     *
     * @return
     */
    public int getSelectedSlot() {
        return inv.getSelectedHotbarSlotId();
    }

    public net.canarymod.api.inventory.PlayerInventory getRecodeHandle() {
        return inv;
    }
}
