package com.minecraftonline.classicloader.classicapi;

import com.minecraftonline.classicloader.apt.RecodeProxy;
import com.minecraftonline.classicloader.classicapi.bridge.ContainerProxy;
import net.minecraft.inventory.ContainerEnchantment;

/**
 * An interface class to Enchantment Tables.
 *
 * @author Willem Mulder (14mRh4X0r)
 */
@RecodeProxy(net.canarymod.api.world.blocks.EnchantmentTable.class)
public class EnchantmentTable extends ItemArray<ContainerProxy> {

    private final net.canarymod.api.world.blocks.EnchantmentTable enchantTable;

    public EnchantmentTable(ContainerEnchantment block) {
        super(block, ContainerProxy.of(block.getInventory()));
        enchantTable = (net.canarymod.api.world.blocks.EnchantmentTable) block.getInventory();
    }

    public EnchantmentTable(net.canarymod.api.world.blocks.EnchantmentTable table) {
        super(ContainerProxy.of(table));
        enchantTable = table;
    }

    @Override
    public void update() {
        enchantTable.update();
    }

    public void update(Player player) {
        this.update(); // Apparently we have an update method now
    }

    @Override
    public String getName() {
        return container.getName();
    }

    @Override
    public void setName(String value) {
        container.setName(value);
    }

    /**
     * Enchants the item and subtracts the XP from the player.
     *
     * @param player The {@link Player} to subract XP from
     * @param slot The slot the player would've clicked. Range: 0-2
     * @return false when <tt>player</tt> doesn't have enough XP.
     * @see #getEnchantLevels()
     * @see Player#getXP()
     */
    public boolean enchantItem(Player player, int slot) {
        return enchantTable.enchantItem(player.getRecodeHandle(), slot);
    }

    /**
     * Gets the levels as displayed in the GUI. The upper slot has index 0.
     *
     * @return an int[3] containing the levels of the slots
     */
    public int[] getEnchantLevels() {
        return enchantTable.getEnchantLevels();
    }

    public net.canarymod.api.world.blocks.EnchantmentTable getRecodeHandle() {
        return enchantTable;
    }
}
