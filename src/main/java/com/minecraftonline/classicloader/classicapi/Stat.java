package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.statistics.CanaryStat;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.stats.StatBase;
import net.minecraft.stats.StatList;

/**
 * Wrapper around a stat.
 *
 * @author Brian McCarthy
 *
 */
@RecodeProxy(net.canarymod.api.statistics.Stat.class)
public class Stat {

    /**
     * StatBase we are wrapping.
     */
    private net.canarymod.api.statistics.Stat stat;

    /**
     * Constructor.
     *
     * @param statBase StatBase to wrap.
     */
    public Stat(StatBase statBase) {
        this.stat = new CanaryStat(statBase);
    }

    public Stat(net.canarymod.api.statistics.Stat stat) {
        this.stat = stat;
    }

    /**
     * Get the id of this stat. May not be very useful apart from comparing
     * equality of stats with different names. E.G. if Minecraft changes the
     * name
     *
     * @return
     */
    public int getID() {
        // SRG return StatList.field_75940_b.indexOf(((CanaryStat) stat).getHandle());
        return StatList.b.indexOf(((CanaryStat) stat).getHandle());
    }

    /**
     * Gets the name of the stat. NOTE: This has gone through Minecraft's string
     * translate to get a readable form/
     *
     * @return
     */
    public String getName() {
        return stat.getName();
    }

    /**
     * Return is this stat is independent. Unsure what this does at the moment.
     *
     * @return
     */
    public boolean isIndependent() {
        return stat.isIndependent();
    }

    /**
     * Get the base wrapped stat.
     *
     * @return
     */
    public StatBase getBase() {
        return ((CanaryStat) stat).getHandle();
    }

    public net.canarymod.api.statistics.Stat getRecodeHandle() {
        return stat;
    }
}
