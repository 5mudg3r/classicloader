package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.world.blocks.CanaryTileEntity;
import com.minecraftonline.classicloader.classicapi.bridge.ContainerProxy;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;

/**
 * Generic superclass for Chests and Furnaces, as they are really similar.
 *
 * @author lightweight
 *
 * @param <T> The type of container we wish to wrap.
 */
public abstract class BaseContainerBlock<T extends TileEntity & IInventory> extends ItemArray<Container<ItemStack>> implements ComplexBlock {

    private final String name;
    protected final T tileEntity;

    /**
     * Create a BaseContainerBlock to act as a wrapper for a given container.
     *
     * @param block The in-world block to 'envelop'.
     * @param reference Shows in toString().
     */
    public BaseContainerBlock(T block, String reference) {
        this(null, block, reference);
    }

    public BaseContainerBlock(net.minecraft.inventory.Container oContainer, T block, String reference) {
        super(oContainer, ContainerProxy.of(oContainer.getInventory()));
        this.name = reference;
        this.tileEntity = block;
    }

    BaseContainerBlock(net.canarymod.api.world.blocks.TileEntity tileEntity, String identity) {
        super(ContainerProxy.of((net.canarymod.api.inventory.Inventory) tileEntity));
        // Explicitly specify T to work around compiler bug, see http://bugs.sun.com/view_bug.do?bug_id=6302954
        this.tileEntity = BaseContainerBlock.<T>getTFromTileEntity(tileEntity);
        this.name = identity;
    }


    @SuppressWarnings("unchecked")
    private static <T extends TileEntity & IInventory> T getTFromTileEntity(net.canarymod.api.world.blocks.TileEntity tileEntity) {
        TileEntity tile = ((CanaryTileEntity) tileEntity).getTileEntity();
        if (!(tile instanceof IInventory) || !(tileEntity instanceof net.canarymod.api.inventory.Inventory)) {
            throw new IllegalArgumentException("tileEntity is not a container");
        }

        return (T) tile;
    }

    @Override
    public int getX() {
        return this.tileEntity.complexBlock.getX();
    }

    @Override
    public int getY() {
        return this.tileEntity.complexBlock.getY();
    }

    @Override
    public int getZ() {
        return this.tileEntity.complexBlock.getZ();
    }

    @Override
    public World getWorld() {
        return Wrapper.wrap(this.tileEntity.complexBlock.getWorld());
    }

    @Override
    public Block getBlock() {
        return this.getWorld().getBlockAt(this.getX(), this.getY(), this.getZ());
    }

    @Override
    public void update() {
        this.tileEntity.complexBlock.update();
    }

    @Override
    public String getName() {
        return this.container.getName();
    }

    @Override
    public void setName(String value) {
        this.container.setName(value);
    }

    /**
     * Tests the given object to see if it equals this object
     *
     * @param obj the object to test
     * @return true if the two objects match
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (((BaseContainerBlock) obj).tileEntity.getClass() != tileEntity.getClass()) {
            return false;
        }

        // Supress warning since we've already returned if class is wrong.
        @SuppressWarnings("unchecked")
        final BaseContainerBlock<T> other = (BaseContainerBlock<T>) obj;

        if (this.getX() != other.getX()) {
            return false;
        }
        if (this.getY() != other.getY()) {
            return false;
        }
        if (this.getZ() != other.getZ()) {
            return false;
        }
        if (!this.getWorld().equals(other.getWorld())) {
            return false;
        }
        return true;
    }

    /**
     * Returns a semi-unique hashcode for this object
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        int hash = 7;

        hash = 97 * hash + this.getX();
        hash = 97 * hash + this.getY();
        hash = 97 * hash + this.getZ();
        hash = 97 * hash + this.getWorld().hashCode();
        return hash;
    }

    @Override
    public String toString() {
        return String.format(this.name + " [x=%d, y=%d, z=%d, world=%s, dimension=%d]", this.getX(), this.getY(), this.getZ(), this.getWorld().getName(), this.getWorld().getType().getId());
    }

    /**
     * Sets this block's data to that found in an NBTTagCompound
     *
     * @param tag the tag to read from
     */
    @Override
    public void readFromTag(NBTTagCompound tag) {
        tileEntity.complexBlock.readFromTag(tag.getRecodeHandle());
    }

    /**
     * Writes this block's data to an NBTagCompound
     *
     * @param tag the tag to write to
     */
    @Override
    public void writeToTag(NBTTagCompound tag) {
        tileEntity.complexBlock.writeToTag(tag.getRecodeHandle());
    }

    @Override
    public NBTTagCompound getMetaTag() {
        return new NBTTagCompound(tileEntity.complexBlock.getMetaTag());
    }

    public abstract net.canarymod.api.world.blocks.TileEntity getRecodeHandle();
}
