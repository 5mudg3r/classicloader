package com.minecraftonline.classicloader.classicapi;

import net.canarymod.Canary;
import net.canarymod.api.nbt.StringTag;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/**
 * Interface for the net.minecraft.server.NBTTagString class
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(StringTag.class)
public class NBTTagString extends NBTBase {

    /**
     * Wraps an net.minecraft.server.NBTTagString
     *
     * @param baseTag the tag to wrap
     */
    public NBTTagString(net.minecraft.nbt.NBTTagString baseTag) {
        super(baseTag);
    }

    /**
     * Creates a new NBTTagString
     *
     * @param name the name of the tag
     */
    public NBTTagString(String name) {
        this("","");
        throw new UnsupportedOperationException("NBT tags don't have names anymore since 1.7");
    }

    /**
     * Creates a new NBTTagString
     *
     * @param name the name of the tag
     * @param value the value of the tag
     */
    public NBTTagString(String name, String value) {
        this(Canary.factory().getNBTFactory().newStringTag(value));
    }

    public NBTTagString(StringTag stringTag) {
        super(stringTag);
    }

    @Override
    public net.minecraft.nbt.NBTTagString getBaseTag() {
        return (net.minecraft.nbt.NBTTagString) super.getBaseTag();
    }

    /**
     * Returns the value of this tag
     *
     * @return
     */
    public String getValue() {
        return getRecodeHandle().getValue();
    }

    /**
     * Sets the value of this tag
     *
     * @param value the new value
     */
    public void setValue(String value) {
        getRecodeHandle().setValue(value);
    }

    /**
     * Gets the plain string of this tag.
     *
     * @return the plain string
     */
    @Override
    public String toPlainString() {
        return getValue();
    }

    @Override
    public String toString() {
        return String.format("NBTTag[type=%s, name=%s, value=%s]", getTagName(getType()), getName(), getValue());
    }

    @Override
    public StringTag getRecodeHandle() {
        return (StringTag) super.getRecodeHandle();
    }

    public static NBTTagString[] fromRecodeArray(StringTag[] recode) {
        NBTTagString[] classic = new NBTTagString[recode.length];
        for (int i = 0; i < recode.length; i++) {
            classic[i] = new NBTTagString(recode[i]);
        }
        return classic;
    }

    public static StringTag[] toRecodeArray(NBTTagString[] classic) {
        StringTag[] recode = new StringTag[classic.length];
        for (int i = 0; i < classic.length; i++) {
            recode[i] = classic[i].getRecodeHandle();
        }
        return recode;
    }
}
