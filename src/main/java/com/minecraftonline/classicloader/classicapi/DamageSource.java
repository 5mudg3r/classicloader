package com.minecraftonline.classicloader.classicapi;

import ca.grimoire.critical.Todo;
import net.canarymod.api.CanaryDamageSource;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.EntityDamageSourceIndirect;

/**
 * A basic DamageSource
 *
 * @author chris
 *
 */
@RecodeProxy(net.canarymod.api.DamageSource.class)
public class DamageSource {

    protected net.canarymod.api.DamageSource source;

    /**
     * Create a new CanaryMod DamageSource from an ODamageSource
     *
     * @param source the native ODamageSource
     */
    public DamageSource(net.minecraft.util.DamageSource source) {
        this(source.getCanaryDamageSource());
    }

    public DamageSource(net.canarymod.api.DamageSource source) {
        this.source = source;
    }

    /**
     * Get the death message for the player.
     * This is the message that will be displayed in chat if the player dies
     * from this {@link DamageSource}.
     *
     * @param player
     * @return
     */
    public String getDeathMessage(Player player) {
        return source.getDeathMessage(player.getRecodeHandle());
    }

    /**
     * Get the name of the damage type as defined when the underlying
     * ODamageSource was created.
     *
     * @return
     */
    public String getName() {
        return source.getNativeName();
    }

    /**
     * Get the <tt>BaseEntity</tt> that is the root of the damage. For example,
     * if a player shoots an arrow, this returns the player. This may return
     * null if the the damage was not caused by an entity.
     *
     * @return the source entity
     */
    public BaseEntity getSourceEntity() {
        return Wrapper.wrap(source.getDamageDealer());
    }

    /**
     * Get the <tt>BaseEntity</tt> that is causing the damage. For example, if a
     * player shoots an arrow, this returns the arrow. This may return null if
     * the the damage was not caused by an entity.
     *
     * @return the damaging entity
     */
    @Todo("Implement in lib methods when possible")
    public BaseEntity getDamagingEntity() {
        // SRG return getDamageSource().func_76364_f() != null ? Wrapper.wrap(getDamageSource().func_76364_f().getCanaryEntity()) : null;
        return getDamageSource().i() != null ? Wrapper.wrap(getDamageSource().i().getCanaryEntity()) : null;
    }

    /**
     * Checks if this DamageSource is a projectile.
     *
     * @return true if projectile, false otherwise
     */
    public boolean isProjectileDamage() {
        return source.isProjectile();
    }

    /**
     * Mark this DamageSource as projectile and return an instance.
     *
     * @return this
     */
    @Todo("Implement in lib methods when possible")
    public DamageSource setProjectileDamage() {
        // SRG getDamageSource().func_76349_b();
        getDamageSource().b();
        return this;
    }

    /**
     * Checks if this DamageSOurce is an explosion.
     *
     * @return true if is explosion, false otherwise
     */
    @Todo("Create an issue about method inclusion")
    public boolean isExplosionDamage() {
        // SRG return getDamageSource().func_94541_c();
        return getDamageSource().c();
    }

    /**
     * Mark this DamageSource as explosion and return an instance.
     *
     * @return this
     */
    @Todo("Implement in lib methods when possible")
    public DamageSource setExplosionDamage() {
        // SRG getDamageSource().func_94540_d();
        getDamageSource().d();
        return this;
    }

    /**
     * Check if this is fire damage
     *
     * @return true if fire damage, false otherwise
     */
    public boolean isFireDamage() {
        return source.isFireDamage();
    }

    /**
     * Check if this is magic damage.
     *
     * @return true if magic damage, false otherwise
     */
    @Todo("Create an issue about method inclusion")
    public boolean isMagicDamage() {
        // SRG return getDamageSource().func_82725_o();
        return getDamageSource().s();
    }

    /**
     * Mark this DamageSource as magic damage and return an instance
     *
     * @return this
     */
    @Todo("Implement in lib methods when possible")
    public DamageSource setMagicDamage() {
        // SRG getDamageSource().func_82726_p();
        getDamageSource().t();
        return this;
    }

    /**
     * Returns true when this damage source cannot be blocked.
     *
     * @return
     */
    public boolean isUnblockable() {
        return source.isUnblockable();
    }

    /**
     * Returns true when this damage also applies in creative-mode
     *
     * @return
     */
    public boolean canDamageInCreativeMode() {
        return source.validInCreativeMode();
    }

    /**
     * Check if this DamageSource is caused by an entity
     *
     * @return
     */
    @Todo("Create an issue about method inclusion")
    public boolean isEntityDamageSource() {
        return getDamageSource() instanceof EntityDamageSource;
    }

    /**
     * Check if this DamageSource has an indirect cause (magic, explosions etc)
     *
     * @return
     */
    @Todo("Create an issue about method inclusion")
    public boolean isIndirectDamageSource() {
        return getDamageSource() instanceof EntityDamageSourceIndirect;
    }

    /**
     * Returns the amount of hunger that will be removed upon damage infliction.
     *
     * @return
     */
    public float getHungerImpact() {
        return source.getHungerDamage();
    }

    /**
     * Check if the damage done will be scaled down/up by difficulty level
     *
     * @return
     */
    @Todo("Create an issue about method inclusion")
    public boolean isDifficultyScaled() {
        // SRG return getDamageSource().func_76350_n();
        return getDamageSource().r();
    }

    /**
     * Make this DamageSource dependent on difficulty level
     *
     * @return
     */
    @Todo("Implement in lib methods when possible")
    public DamageSource setDifficultyScaled() {
        // SRG getDamageSource().func_76351_m();
        getDamageSource().q();
        return this;
    }

    /**
     * Returns the native ODamageSource reference
     *
     * @return
     */
    public net.minecraft.util.DamageSource getDamageSource() {
        return ((CanaryDamageSource) source).getHandle();
    }

    public DamageType getDamageType() {
        return DamageType.fromDamageSource(this);
    }

    // Static util methods
    /**
     * Create a DamageSource for the given LivingEntity.
     *
     * @param entity LivingEntity that causes the damage
     * @return {@link EntityDamageSource}
     */
    public static DamageSource createMobDamage(LivingEntity entity) {
        return Wrapper.wrap(net.minecraft.util.DamageSource.a(entity.getEntity()).getCanaryDamageSource());
    }

    /**
     * Create a DamageSource for the given Player.
     *
     * @param player Player that causes the damage
     * @return {@link EntityDamageSource}
     */
    public static DamageSource createPlayerDamage(Player player) {
        return Wrapper.wrap(net.minecraft.util.DamageSource.a(player.getEntity()).getCanaryDamageSource());
    }

    /**
     * Creates a thorn-enchantment DamageSource that is caused by the given
     * entity
     *
     * @param entity
     * @return
     */
    public static DamageSource createThornsDamage(BaseEntity entity) {
        return Wrapper.wrap(net.minecraft.util.DamageSource.a(entity.getEntity()).getCanaryDamageSource());
    }

    public net.canarymod.api.DamageSource getRecodeHandle() {
        return this.source;
    }

    public static DamageSource[] fromRecodeArray(net.canarymod.api.DamageSource[] recode) {
        DamageSource[] classic = new DamageSource[recode.length];
        for (int i = 0; i < recode.length; i++) {
            classic[i] = Wrapper.wrap(recode[i]);
        }
        return classic;
    }

    public static net.canarymod.api.DamageSource[] toRecodeArray(DamageSource[] classic) {
        net.canarymod.api.DamageSource[] recode = new net.canarymod.api.DamageSource[classic.length];
        for (int i = 0; i < classic.length; i++) {
            recode[i] = classic[i].getRecodeHandle();
        }
        return recode;
    }
}
