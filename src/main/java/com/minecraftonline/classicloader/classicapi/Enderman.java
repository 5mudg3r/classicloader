package com.minecraftonline.classicloader.classicapi;

import com.google.common.primitives.Ints;
import java.util.ArrayList;
import java.util.List;
import net.canarymod.api.entity.living.monster.CanaryEnderman;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.canarymod.config.Configuration;
import net.canarymod.config.WorldConfiguration;

@RecodeProxy(net.canarymod.api.entity.living.monster.Enderman.class)
public class Enderman extends Mob {

    /**
     * Creates an enderman wrapper
     *
     * @param entity The entity to wrap
     */
    public Enderman(net.minecraft.entity.monster.EntityEnderman entity) {
        super(entity);
    }

    /**
     * Creates a new enderman
     *
     * @param world The world to create it in
     */
    public Enderman(World world) {
        super("Enderman", world);
    }

    /**
     * Creates a new enderman
     *
     * @param location The location at which to create it
     */
    public Enderman(Location location) {
        super("Enderman", location);
    }

    public Enderman(net.canarymod.api.entity.living.monster.Enderman enderman) {
        super(enderman);
    }

    /**
     * Returns block in endermans's hand
     *
     * @return Block
     */
    @SuppressWarnings("deprecation")
    public Block getBlockInHand() {
        return new Block(Block.Type.fromId(getRecodeHandle().getCarriedBlockID()),
                getRecodeHandle().getCarriedBlockMetaData());
    }

    /**
     * Sets the block the enderman is holding.
     *
     * @param blockID - the block ID that the enderman should hold.
     * @param blockData - the data of the block the enderman should hold.
     * @return True if the enderman can hold the block or not.
     */
    public boolean setBlockInHand(int blockID, int blockData) {
        if (getHoldable(blockID)) {
            getRecodeHandle().setCarriedBlockID((short) blockID);
            getRecodeHandle().setCarriedBlockMetaData((short) blockData);
            return true;
        }
        return false;
    }

    /**
     * Sets the block the enderman is holding.
     *
     * @param blockID - the block ID that the enderman should hold.
     * @return True if the enderman can hold the block or not.
     */
    public boolean setBlockInHand(int blockID) {
        return this.setBlockInHand(blockID, 0);
    }

    /**
     * Sets the block the enderman is holding.
     *
     * @param block - the block the enderman should hold
     * @return True if the enderman can hold the block or not.
     */
    public boolean setBlockInHand(Block block) {
        return this.setBlockInHand(block.getType(), block.getData());
    }

    /**
     * @param blockID - the block ID to check if the enderman can hold
     * @return True if the enderman can hold the block or not.
     */
    public boolean getHoldable(int blockID) {
        return Ints.asList(Configuration.getWorldConfig(getRecodeHandle().getWorld().getFqName()).getEnderBlocks()).contains(blockID);
    }

    /**
     * Allows or prevents the enderman from picking up a specific block
     *
     * @param blockID - the block to allow or prevent the enderman from picking
     * up.
     * @param holdable Whether the enderman should be able to hold the block or
     * not.
     */
    public void setHoldable(int blockID, boolean holdable) {
        // Hello ugly hack!
        WorldConfiguration worldConf = Configuration.getWorldConfig(getRecodeHandle().getWorld().getFqName());
        net.visualillusionsent.utils.PropertiesFile props = worldConf.getFile();

        List<Integer> holdableItems = new ArrayList<Integer>(Ints.asList(worldConf.getEnderBlocks()));
        if (holdableItems.contains(blockID) && !holdable) {
            holdableItems.remove(blockID);
        } else if (!holdableItems.contains(blockID) && holdable) {
            holdableItems.add(blockID);
        }

        StackTraceElement[] trace = Thread.currentThread().getStackTrace();
        props.setIntArray("ender-blocks", Ints.toArray(holdableItems),
                "Changed by ClassicLoader" + (trace.length > 1
                ? " on behalf of " + trace[1].getClassName() : ""));
    }

    @Override
    public net.minecraft.entity.monster.EntityEnderman getEntity() {
        return (net.minecraft.entity.monster.EntityEnderman) super.getEntity();
    }

    @Override
    public net.canarymod.api.entity.living.monster.Enderman getRecodeHandle() {
        return (net.canarymod.api.entity.living.monster.Enderman) super.getRecodeHandle();
    }
}
