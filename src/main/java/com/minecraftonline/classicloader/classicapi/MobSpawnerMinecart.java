package com.minecraftonline.classicloader.classicapi;

import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.item.EntityMinecartMobSpawner;

import static net.canarymod.api.entity.EntityType.MOBSPAWNERMINECART;

@RecodeProxy(net.canarymod.api.entity.vehicle.MobSpawnerMinecart.class)
public class MobSpawnerMinecart extends Minecart {

    private MobSpawnerLogic logic;

    MobSpawnerMinecart(EntityMinecartMobSpawner o) {
        super(o);
    }

    public MobSpawnerMinecart(net.canarymod.api.entity.vehicle.MobSpawnerMinecart cart) {
        super(cart);
    }

    /**
     * Create a new Spawner Minecart with the given position. Call
     * {@link #spawn()} to spawn it in the world, use this cart's
     * {@link MobSpawnerLogic} to set options for the spawner.
     *
     * @param world The world for the new minecart
     * @param x The x coordinate for the new minecart
     * @param y The y coordinate for the new minecart
     * @param z The z coordinate for the new minecart
     * @see #getLogic()
     */
    public MobSpawnerMinecart(World world, double x, double y, double z) {
        this((net.canarymod.api.entity.vehicle.MobSpawnerMinecart) factory.newVehicle(MOBSPAWNERMINECART,
                new Location(world, x, y, z).getRecodeLocation()));
    }

    /**
     * Get the {@link MobSpawnerLogic} for this <tt>MobSpawnerMinecart</tt>.
     *
     * @return This <tt>MobSpawnerMinecart</tt>'s {@link MobSpawnerLogic}
     */
    public MobSpawnerLogic getLogic() {
        if (logic == null) {
            logic = new MobSpawnerLogic(getEntity().a);
        }

        return logic;
    }

    @Override
    public EntityMinecartMobSpawner getEntity() {
        return (EntityMinecartMobSpawner) super.getEntity();
    }

    @Override
    public net.canarymod.api.entity.vehicle.MobSpawnerMinecart getRecodeHandle() {
        return (net.canarymod.api.entity.vehicle.MobSpawnerMinecart) super.getRecodeHandle();
    }
}
