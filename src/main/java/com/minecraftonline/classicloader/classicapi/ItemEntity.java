package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.entity.EntityItem;
import com.minecraftonline.classicloader.apt.RecodeProxy;

@RecodeProxy(EntityItem.class)
public class ItemEntity extends BaseEntity {

    /**
     * Constructor
     */
    public ItemEntity() {
    }

    /**
     * Constructor
     *
     * @param item
     */
    public ItemEntity(net.minecraft.entity.item.EntityItem item) {
        super(item);
    }

    public ItemEntity(EntityItem item) {
        super(item);
    }

    /**
     * Returns the entity we're wrapping.
     *
     * @return
     */
    @Override
    public net.minecraft.entity.item.EntityItem getEntity() {
        return (net.minecraft.entity.item.EntityItem) super.getEntity();
    }

    public void setAge(int age) {
        getRecodeHandle().setAge((short) age);
    }

    public int getAge() {
        return getRecodeHandle().getAge();
    }

    public void setDelayBeforeCanPickUp(int time) {
        getRecodeHandle().setPickUpDelay(time);
    }

    public int getDelayBeforeCanPickUp() {
        return getRecodeHandle().getPickUpDelay();
    }

    public Item getItem() {
        return Wrapper.wrap(getRecodeHandle().getItem());
    }

    @Override
    public World getWorld() {
        return super.getWorld(); // Backward compatibility
    }

    @Override
    public EntityItem getRecodeHandle() {
        return (EntityItem) super.getRecodeHandle();
    }

    public static ItemEntity[] fromRecodeArray(EntityItem[] recode) {
        ItemEntity[] classic = new ItemEntity[recode.length];
        for (int i = 0; i < recode.length; i++) {
            classic[i] = Wrapper.wrap(recode[i]);
        }
        return classic;
    }

    public static EntityItem[] toRecodeArray(ItemEntity[] classic) {
        EntityItem[] recode = new EntityItem[classic.length];
        for (int i = 0; i < classic.length; i++) {
            recode[i] = classic[i].getRecodeHandle();
        }
        return recode;
    }
}
