package com.minecraftonline.classicloader.classicapi;

import java.util.ArrayList;
import java.util.List;
import net.canarymod.Canary;
import net.canarymod.api.entity.living.LivingBase;
import net.canarymod.api.potion.PotionEffectType;
import com.minecraftonline.classicloader.Util;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.canarymod.api.entity.CanaryEntity;
import net.minecraft.entity.EntityLivingBase;

@RecodeProxy(LivingBase.class)
public class LivingEntityBase extends BaseEntity {

    public LivingEntityBase() {
    }

    protected LivingEntityBase(EntityLivingBase nms) {
        super(nms);
    }

    public LivingEntityBase(LivingBase base) {
        super(base);
    }

    /**
     * Returns the entity we're wrapping.
     *
     * @return
     */
    @Override
    public EntityLivingBase getEntity() {
        return (EntityLivingBase) super.getEntity();
    }

    /**
     * Returns the entity's health.
     *
     * @return health
     * @deprecated Health is measured in floats now, use
     * {@link #getHealthFloat()} instead.
     */
    @Deprecated
    public int getHealth() {
        return (int) getHealthFloat();
    }

    /**
     * Returns this entity's health.
     *
     * @return This entity's health.
     */
    public float getHealthFloat() {
        return getRecodeHandle().getHealth();
    }

    /**
     * Sets the entity's health. 20 = max health 1 = 1/2 heart 2 = 1 heart
     *
     * @param health The entity's new health.
     * @deprecated Health is measured in floats now, use
     * {@link #setHealth(float)} instead.
     */
    @Deprecated
    public void setHealth(int health) {
        setHealth((float) health);
    }

    /**
     * Sets the entity's health. 20 = max health 1 = 1/2 heart 2 = 1 heart
     *
     * @param health The entity's new health.
     */
    public void setHealth(float health) {
        getRecodeHandle().setHealth(health);
    }

    /**
     * Increase entity health.
     *
     * @param health amount of health to increase the players health with.
     * @deprecated Health is measured in floats now, use
     * {@link #increaseHealth(float)} instead.
     */
    @Deprecated
    public void increaseHealth(int health) {
        this.increaseHealth((float) health);
    }

    /**
     * Increase entity health.
     *
     * @param health amount of health to increase the players health with.
     */
    public void increaseHealth(float health) {
        getRecodeHandle().increaseHealth(health);
    }

    /**
     * Get an entities max health value
     *
     * @return max health
     * @deprecated Max health is measured in doubles now, use
     * {@link #getMaxHealthDouble()} instead.
     */
    @Deprecated
    public int getMaxHealth() {
        return (int) getMaxHealthDouble();
    }

    /**
     * Get an entities max health value
     *
     * @return max health
     */
    public double getMaxHealthDouble() {
        return getRecodeHandle().getMaxHealth();
    }

    /**
     * Set the entities max health
     *
     * @param toSet max health
     * @deprecated Max health is measured in doubles now, use
     * {@link #setMaxHealth(double)} instead.
     */
    @Deprecated
    public void setMaxHealth(int toSet) {
        setMaxHealth((double) toSet);
    }

    /**
     * Set the entities max health
     *
     * @param maxHealth max health
     */
    public void setMaxHealth(double maxHealth) {
        if (maxHealth > 0) {
            getRecodeHandle().setMaxHealth(maxHealth);
        }
    }

    /**
     * Set the amount of ticks this entity will not take damage. (until it
     * heals) 20 ticks per second.
     *
     * @param ticks
     */
    public void setBaseNoDamageTicks(int ticks) {
        getRecodeHandle().setInvulnerabilityTicks(ticks);
    }

    /**
     * Get the current maximum damage taken during this NoDamageTime
     *
     * @return
     * @deprecated Damage is measured in floats now, use
     * {@link #getLastDamageFloat()} instead.
     */
    public int getLastDamage() {
        return (int) getLastDamageFloat();
    }

    /**
     * Get the current maximum damage taken during this NoDamageTime
     *
     * @return
     */
    public float getLastDamageFloat() {
        return Util.getField(EntityLivingBase.class, getEntity(), "bc", float.class);
    }

    /**
     * Set the current maximum damage taken during this NoDamageTime (if any
     * damage is higher than this number the difference will be added)
     *
     * @param amount
     * @deprecated Damage is measured in floats now, use
     * {@link #setLastDamage(float)} instead.
     */
    public void setLastDamage(int amount) {
        setLastDamage((float) amount);
    }

    /**
     * Set the current maximum damage taken during this NoDamageTime (if any
     * damage is higher than this number the difference will be added)
     *
     * @param amount
     */
    public void setLastDamage(float amount) {
        Util.<Float>setField(EntityLivingBase.class, getEntity(), "bc", amount);
    }

    /**
     * Adds a potion effect to the entity.
     *
     * @param effect the effect to add.
     */
    public void addPotionEffect(PotionEffect effect) {
        getRecodeHandle().addPotionEffect(effect.getRecodeHandle());
    }

    /**
     * Removes a potion effect from entity.
     *
     * @param effect The potion effect to remove
     */
    public void removePotionEffect(PotionEffect effect) {
        getRecodeHandle().removePotionEffect(PotionEffectType.fromId(effect.getType().getId()));
    }

    /**
     * Returns whether this entity has a potion effect of the specified type.
     *
     * @param effectType The potion effect type to check for
     * @return whether this entity has a potion effect of the specified type
     */
    public boolean hasPotionEffect(PotionEffect.Type effectType) {
        return getRecodeHandle().getAllActivePotionEffects().contains(
                Canary.factory().getPotionFactory().newPotionEffect(effectType.getId(), 0, 0));
    }

    /**
     * Returns a Collection of potion effects active on the entity.
     *
     * @return List of potion effects
     */
    @SuppressWarnings("unchecked")
    public List<PotionEffect> getPotionEffects() {
        List<net.canarymod.api.potion.PotionEffect> potionEffects = getRecodeHandle().getAllActivePotionEffects();
        ArrayList<PotionEffect> list = new ArrayList<PotionEffect>(potionEffects.size());

        for (net.canarymod.api.potion.PotionEffect potionEffect : potionEffects) {
            list.add(Wrapper.wrap(potionEffect));
        }
        return list;
    }

    /**
     * Damages this entity, taking into account armor/enchantments/potions
     *
     * @param type The type of damage to deal (certain types byass armor or
     * affect potions differently)
     * @param amount The amount of damage to deal (2 = 1 heart)
     * @deprecated use applyDamage(DamageType, int)
     */
    @Deprecated
    public void applyDamage(PluginLoader.DamageType type, int amount) {
        applyDamage(Wrapper.wrap(type.getDamageSource().getCanaryDamageSource()), (float) amount);
    }

    /**
     * Damages this entity, taking into account armor/enchantments/potions
     *
     * @param type
     * @param amount
     * @deprecated Damage is measured in floats now. Use
     * {@link #applyDamage(DamageType, float)} instead.
     */
    @Deprecated
    public void applyDamage(DamageType type, int amount) {
        applyDamage(type.getDamageSource(), (float) amount);
    }

    /**
     * Damages this entity, taking into account armor/enchantments/potions
     *
     * @param type
     * @param amount
     */
    public void applyDamage(DamageType type, float amount) {
        applyDamage(type.getDamageSource(), amount);
    }

    /**
     * Damages this entity, taking into account armor/enchantments/potions
     *
     * @param source
     * @param amount
     * @deprecated Damage is measured in floats now, use
     * {@link #applyDamage(DamageSource, float)} instead. Also, this method is
     * majorly typo'd.
     */
    @Deprecated
    public void applayDamage(DamageSource source, int amount) {
        applyDamage(source, (float) amount);
    }

    /**
     * Damages this entity, taking into account armor/enchantments/potions
     *
     * @param source
     * @param amount
     */
    // TODO: pull up
    public void applyDamage(DamageSource source, float amount) {
        getRecodeHandle().dealDamage(source.getRecodeHandle().getDamagetype(), amount);
    }

    /**
     * Get the amount of ticks this entity will not take damage. (unless it
     * heals) 20 ticks per second.
     *
     * @return
     */
    public int getBaseNoDamageTicks() {
        return getRecodeHandle().getInvulnerabilityTicks();
    }

    /**
     * Get the amount of ticks this entity is dead. 20 ticks per second.
     *
     * @return
     */
    public int getDeathTicks() {
        return getRecodeHandle().getDeathTicks();
    }

    /**
     * Set the amount of ticks this entity is dead. 20 ticks per second.
     *
     * @param ticks
     */
    public void setDeathTicks(int ticks) {
        getRecodeHandle().setDeathTicks(ticks);
    }

    @Override
    public LivingBase getRecodeHandle() {
        return (LivingBase) super.getRecodeHandle();
    }
}
