package com.minecraftonline.classicloader.classicapi;

import java.sql.SQLException;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * That guy runs to clean connections that are not used anymore in the
 * connection pool.
 *
 * @author Chris
 *
 */
public class ConnectionGuard implements Runnable {

    private ConnectionService cp;
    private static final Logger log = LogManager.getLogger();

    public ConnectionGuard(ConnectionService cp) {
        log.info("CanaryMod: Starting connection cleanup thread.");
        this.cp = cp;
    }

    @Override
    public void run() {
        try {
            cp.clearConnections();
        } catch (SQLException e) {
            log.error("Error in ConnectionGuard", e);
        }
    }
}
