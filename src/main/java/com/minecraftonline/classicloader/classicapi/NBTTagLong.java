package com.minecraftonline.classicloader.classicapi;

import net.canarymod.Canary;
import net.canarymod.api.nbt.LongTag;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/**
 * Interface for the net.minecraft.server.NBTTagLong class
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(LongTag.class)
public class NBTTagLong extends NBTBase {

    /**
     * Wraps an net.minecraft.server.NBTTagLong
     *
     * @param tagBase the tag to wrap
     */
    public NBTTagLong(net.minecraft.nbt.NBTTagLong tagBase) {
        super(tagBase);
    }

    /**
     * Creates a new NBTTagLong
     *
     * @param name the name of the tag
     */
    public NBTTagLong(String name) {
        this(name, 0);
    }

    /**
     * Creates a new NBTTagLong
     *
     * @param name the name of the tag
     * @param value the value of the tag
     */
    public NBTTagLong(String name, long value) {
        this(Canary.factory().getNBTFactory().newLongTag(value));
    }

    public NBTTagLong(LongTag longTag) {
        super(longTag);
    }

    @Override
    public net.minecraft.nbt.NBTTagLong getBaseTag() {
        return (net.minecraft.nbt.NBTTagLong) super.getBaseTag();
    }

    /**
     * Returns the value of this tag
     *
     * @return
     */
    public long getValue() {
        return getRecodeHandle().getValue();
    }

    /**
     * Sets the value of this tag
     *
     * @param value the new value
     */
    public void setValue(long value) {
        getRecodeHandle().setValue(value);
    }

    @Override
    public String toString() {
        return String.format("NBTTag[type=%s, name=%s, value=%d]", getTagName(getType()), getName(), getValue());
    }

    @Override
    public LongTag getRecodeHandle() {
        return (LongTag) super.getRecodeHandle();
    }

    public static NBTTagLong[] fromRecodeArray(LongTag[] recode) {
        NBTTagLong[] classic = new NBTTagLong[recode.length];
        for (int i = 0; i < recode.length; i++) {
            classic[i] = new NBTTagLong(recode[i]);
        }
        return classic;
    }

    public static LongTag[] toRecodeArray(NBTTagLong[] classic) {
        LongTag[] recode = new LongTag[classic.length];
        for (int i = 0; i < classic.length; i++) {
            recode[i] = classic[i].getRecodeHandle();
        }
        return recode;
    }
}
