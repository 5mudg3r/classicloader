package com.minecraftonline.classicloader.classicapi;

import java.util.Arrays;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryCraftResult;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

/**
 * Wrapper for inventories that implement some kind of crafting.
 * @param <C> The container for the "crafting matrix".
 */
public class InventoryCrafting<C extends Container<ItemStack>> extends ItemArray<C> {

    public InventoryCraftResult result;
    private final int resultStartIndex;

    public InventoryCrafting(net.minecraft.inventory.Container oContainer, C container, IInventory result) {
        this(oContainer, container, result, 0);
    }

    public InventoryCrafting(net.minecraft.inventory.Container oContainer, C container, IInventory result, int resultStartIndex) {
        super(oContainer, container);
        this.result = (InventoryCraftResult) result;
        this.resultStartIndex = resultStartIndex;
    }


    /**
     * Returns the (native) items contained in the crafting matrix.
     * @return the crafting matrix' contents
     */
    public ItemStack[] getCraftMatrixContents() {
        return container.getContents();
    }

    /**
     * Returns the (native) item(s) that would be the result of this crafting operation.
     * @return the result of the current craft matrix setup
     */
    public ItemStack[] getResultContents() {
        return result.a;
    }

    /**
     * Returns the size of the crafting matrix.
     * @return the crafting matrix' size
     */
    public int getCraftMatrixSize() {
        return container.getContentsSize();
    }

    /**
     * Return the size of the result (number of possible items).
     * @return the result's size (number of possible items).
     */
    public int getResultSize() {
        return result.a.length;
    }

    @Override
    public Item getItemFromSlot(int slot) {
        Slot oslot = getOContainer().getSlot(slot);
        if (oslot != null) {
            // SRG ItemStack oitem = oslot.func_75211_c();
            ItemStack oitem = oslot.d();
            return oitem == null ? null : Wrapper.wrap(oitem.getCanaryItem());
        }

        return null;
    }

    @Override
    public int getEmptySlot() {
        int size = getContentsSize();

        for (int i = 0; size > i; i++) {
            Slot oslot = getOContainer().getSlot(i);
            // SRG if (oslot != null && oslot.func_75211_c() != null) {
            if (oslot != null && oslot.d() != null) {
                continue;
            }
            return i;
        }

        return -1;
    }

    @Override
    public void removeItem(int slot) {
        setSlot((ItemStack) null, slot);
    }

    @Override
    public void setSlot(Item item, int slot) {
        setSlot(item == null ? null : item.getBaseItem(), slot);
    }

    @Override
    public void setSlot(int itemId, int amount, int damage, int slot) {
        // SRG net.minecraft.item.Item itemtype = net.minecraft.item.Item.func_150899_d(itemId);
        net.minecraft.item.Item itemtype = net.minecraft.item.Item.d(itemId);
        ItemStack item = new ItemStack(itemtype, (amount > 64 ? (amount == 255 ? -1 : 64) : amount), damage);
        setSlot(item, slot);
    }

    private void setSlot(ItemStack item, int slot) {
        //since OSlot "setSlot" uses update methods, need to do it the hard way.
        int size = getResultSize();
        int index = slot;
        if (slot >= resultStartIndex && slot < resultStartIndex + size) {
            index = slot - resultStartIndex;
            result.a[index] = item;
        }

        if (resultStartIndex == 0) {
            index = slot - resultStartIndex - size;
        }

        size = getCraftMatrixSize();

        if (index >= 0 && index < size) {
            container.setContentsAt(index, item);
        }
    }

    @Override
    public void clearContents() {
        int size = getContentsSize();

        for (int i = 0; size > i; i++) {
            removeItem(i);
        }
    }

    @Override
    public int getContentsSize() {
        return getCraftMatrixSize() + getResultSize();
    }

    @Override
    public void update() {
        getOContainer().updateChangedSlots();
    }

    @Override
    public void setOContainer(net.minecraft.inventory.Container oContainer) {
        if (oContainer == null) {
            return;
        }
        super.setOContainer(oContainer);
    }

    public int getLocalSlotIndex(int slot) {
        if (slot == SlotType.OUTSIDE) {
            return slot;
        }

        int size = getResultSize();
        if (slot >= resultStartIndex && slot < resultStartIndex + size) {
            return slot - resultStartIndex;
        }

        if (resultStartIndex == 0) {
            return slot - resultStartIndex - size;
        }

        return slot;
    }

    @Override
    public String getName() {
        return container.getName();
    }

    @Override
    public void setName(String value) {
        container.setName(value);
    }

    @Override
    public String toString() {
        return String.format("CraftMatrix[size=%d, contents=%s] Result[size=%d, contents=%s]",
                container.getContentsSize(), Arrays.toString(getCraftMatrixContents()), getResultSize(), Arrays.toString(getResultContents()));
    }
}
