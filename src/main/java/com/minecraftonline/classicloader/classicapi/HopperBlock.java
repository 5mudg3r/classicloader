package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.world.blocks.TileEntity;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.tileentity.TileEntityHopper;

/**
 * Wrapper class for hoppers.
 *
 * @author Somners
 */
@RecodeProxy(net.canarymod.api.world.blocks.HopperBlock.class)
public class HopperBlock extends BaseContainerBlock<TileEntityHopper> implements Hopper {

    HopperBlock(TileEntityHopper hopper) {
        this(null, hopper);
    }

    public HopperBlock(net.minecraft.inventory.Container oContainer, TileEntityHopper hopper) {
        super(oContainer, hopper, "Hopper");
    }

    public HopperBlock(net.canarymod.api.world.blocks.HopperBlock hopper) {
        super(hopper, "Hopper");
    }

    /**
     * Gets the Inventory inputting to the hopper.
     *
     * @return the Inventory or null if none.
     */
    public Inventory getInputContainer() {
        net.canarymod.api.inventory.Inventory input = getRecodeHandle().getInputInventory();
        if (input instanceof net.canarymod.api.entity.vehicle.ContainerMinecart) {
            return Wrapper.wrap((net.canarymod.api.entity.vehicle.ContainerMinecart) input);
        }
        return getBaseContainerBlock(getRecodeHandle().getInputInventory());
    }

    /**
     * Gets the Inventory the hopper outputs to.
     *
     * @return the Inventory or null if none.
     */
    public Inventory getOutputContainer() {
        return this.getBaseContainerBlock(getRecodeHandle().getOutputInventory());
    }

    /**
     * Check if this hopper is connected to any Container either input or
     * output.
     *
     * @return true - it is connected<br>false - it is not connected
     */
    public boolean isConnected() {
        return (this.isInputConnected() && this.isOutputConnected());
    }

    /**
     * Check if the block this hopper inputs from is a Container.
     *
     * @return true - it is connected<br>false - it is not connected
     */
    public boolean isInputConnected() {
        return this.getInputContainer() != null;
    }

    /**
     * Check if the block this hopper outputs to is a Container.
     *
     * @return true - it is connected<br>false - it is not connected
     */
    public boolean isOutputConnected() {
        return this.getOutputContainer() != null;
    }

    /**
     * Gets the Inventory from the inventory instance.
     *
     * @param oiinventory OIInventory instance to get Inventory wrapper for.
     * @return The inventory or null if none.
     */
    private Inventory getBaseContainerBlock(net.canarymod.api.inventory.Inventory inventory) {
        if (inventory instanceof TileEntity) { // Fuck it, this is never gonna change
            if (inventory instanceof net.canarymod.api.world.blocks.Beacon) {
                return Wrapper.wrap((net.canarymod.api.world.blocks.Beacon) inventory);
            } else if (inventory instanceof net.canarymod.api.world.blocks.BrewingStand) {
                return Wrapper.wrap((net.canarymod.api.world.blocks.BrewingStand) inventory);
            } else if (inventory instanceof net.canarymod.api.world.blocks.Chest) {
                return Wrapper.wrap((net.canarymod.api.world.blocks.Chest) inventory);
            } else if (inventory instanceof net.canarymod.api.world.blocks.Dispenser) {
                return Wrapper.wrap((net.canarymod.api.world.blocks.Dispenser) inventory);
            } else if (inventory instanceof net.canarymod.api.world.blocks.Furnace) {
                return Wrapper.wrap((net.canarymod.api.world.blocks.Furnace) inventory);
            } else if (inventory instanceof net.canarymod.api.world.blocks.HopperBlock) {
                return Wrapper.wrap((net.canarymod.api.world.blocks.HopperBlock) inventory);
            }
        } else if (inventory instanceof net.canarymod.api.world.blocks.DoubleChest) {
            return Wrapper.wrap((net.canarymod.api.world.blocks.DoubleChest) inventory);
        }
        return null;
    }

    @Override
    public double getPosX() {
        return this.getX();
    }

    @Override
    public double getPosY() {
        return this.getY();
    }

    @Override
    public double getPosZ() {
        return this.getZ();
    }

    @Override
    public int getTranferCooldown() {
        return getRecodeHandle().getTranferCooldown();
    }

    @Override
    public void setTransferCooldown(int cooldown) {
        getRecodeHandle().setTransferCooldown(cooldown);
    }

    @Override
    public net.canarymod.api.world.blocks.HopperBlock getRecodeHandle() {
        return tileEntity.getCanaryHopper();
    }
}
