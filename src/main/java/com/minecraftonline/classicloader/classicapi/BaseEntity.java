package com.minecraftonline.classicloader.classicapi;

import ca.grimoire.critical.Todo;
import net.canarymod.Canary;
import net.canarymod.api.entity.CanaryEntity;
import net.canarymod.api.entity.Entity;
import net.canarymod.api.factory.EntityFactory;
import com.minecraftonline.classicloader.Util;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/**
 * BaseEntity.java - Class for accessing things that all entities share - W, X,
 * Y, health.
 *
 * @author James
 */
@RecodeProxy(Entity.class)
public class BaseEntity implements Metadatable {

    static final EntityFactory factory = Canary.factory().getEntityFactory();
    Entity entity;

    /**
     * Creates an interface for an entity
     *
     * @param entity
     */
    public BaseEntity(net.minecraft.entity.Entity entity) {
        this(entity.getCanaryEntity());
    }

    /**
     * Interface for entities.
     */
    public BaseEntity() {
    }

    public BaseEntity(Entity entity) {
        if (entity == null) {
            throw new IllegalStateException("entity cannot be null");
        }
        this.entity = entity;
    }

    /**
     * Returns the ID for this mob
     *
     * @return id
     */
    public int getId() {
        return entity.getID();
    }

    /**
     * Teleports to the provided location.
     *
     * @param x
     * @param rotation
     * @param y
     * @param z
     * @param pitch
     */
    public void teleportTo(double x, double y, double z, float rotation, float pitch) {
        entity.teleportTo(x, y, z, pitch, rotation);
    }

    /**
     * Teleports to the other entity.
     *
     * @param ent entity to teleport to
     */
    public void teleportTo(BaseEntity ent) {
        this.teleportTo(ent.getX(), ent.getY(), ent.getZ(), ent.getRotation(), ent.getPitch());
    }

    /**
     * Teleports to the provided location.
     *
     * @param location location to teleport to
     */
    public void teleportTo(Location location) {
        this.teleportTo(location.x, location.y, location.z, location.rotX, location.rotY);
    }

    /**
     * Returns the entity's W
     *
     * @return x
     */
    public double getX() {
        return entity.getX();
    }

    /**
     * Sets the entity's W
     *
     * @param x x to set
     */
    public void setX(double x) {
        this.teleportTo(x, this.getY(), this.getZ(), this.getRotation(), this.getPitch());
    }

    /**
     * Returns the entity's X
     *
     * @return y
     */
    public double getY() {
        return entity.getY();
    }

    /**
     * Sets the entity's X
     *
     * @param y y to set
     */
    public void setY(double y) {
        this.teleportTo(this.getX(), y, this.getZ(), this.getRotation(), this.getPitch());
    }

    /**
     * Returns the entity's Y
     *
     * @return z
     */
    public double getZ() {
        return entity.getZ();
    }

    /**
     * Sets the entity's Y
     *
     * @param z z to set
     */
    public void setZ(double z) {
        this.teleportTo(this.getX(), this.getY(), z, this.getRotation(), this.getPitch());
    }

    /**
     * Returns the entity's pitch
     *
     * @return pitch
     */
    public float getPitch() {
        return entity.getPitch();
    }

    /**
     * Sets the entity's pitch
     *
     * @param pitch pitch to set
     */
    public void setPitch(float pitch) {
        this.teleportTo(this.getX(), this.getY(), this.getZ(), this.getRotation(), pitch);
    }

    /**
     * Returns the entity's rotation
     *
     * @return rotation
     */
    public float getRotation() {
        return entity.getRotation();
    }

    /**
     * Sets the entity's rotation
     *
     * @param rotation rotation to set
     */
    public void setRotation(float rotation) {
        this.teleportTo(this.getX(), this.getY(), this.getZ(), rotation, this.getPitch());
    }

    /**
     * Returns the entity we're wrapping.
     *
     * @return
     */
    public net.minecraft.entity.Entity getEntity() {
        return ((CanaryEntity) entity).getHandle();
    }

    /**
     * Returns whether or not this entity is a mob
     *
     * @return true if mob
     */
    public boolean isMob() {
        return entity.isMob();
    }

    public static boolean isMob(net.minecraft.entity.Entity entity) {
        return entity.getCanaryEntity().isMob();
    }

    /**
     * Returns whether or not this entity is an animal
     *
     * @return true if animal
     */
    public boolean isAnimal() {
        return entity.isAnimal();
    }

    public static boolean isAnimal(net.minecraft.entity.Entity entity) {
        return entity.getCanaryEntity().isAnimal();
    }

    /**
     * Returns true if this entity is a player
     *
     * @return true if player
     */
    public boolean isPlayer() {
        return entity.isPlayer();
    }

    public static boolean isPlayer(net.minecraft.entity.Entity entity) {
        return entity.getCanaryEntity().isPlayer();
    }

    /**
     * Returns whether or not this entity is alive
     *
     * @return true if living entity
     */
    public boolean isLiving() {
        return entity.isLiving();
    }

    public static boolean isLiving(net.minecraft.entity.Entity entity) {
        return entity.getCanaryEntity().isLiving();
    }

    /**
     * Returns whether or not this entity is an item entity
     *
     * @return true if item entity
     */
    public boolean isItem() {
        return entity.isItem();
    }

    public static boolean isItem(net.minecraft.entity.Entity entity) {
        return entity.getCanaryEntity().isItem();
    }

    /**
     * Returns the player for this entity
     *
     * @return player
     */
    public Player getPlayer() {
        if (!this.isPlayer()) {
            return null;
        }

        return Wrapper.wrap((net.canarymod.api.entity.living.humanoid.Player) entity);
    }

    /**
     * Get the default amount of AirTicks for this entity 20 ticks per second.
     *
     * @return 300
     * @deprecated It doesn't exist anymore P:
     */
    @Deprecated
    public int getBaseAirTicks() {
        return 300;
    }

    /**
     * Set the default amount of AirTicks for this entity 20 ticks per second.
     *
     * @param ticks
     * @deprecated It doesn't exist anymore
     * @throws UnsupportedOperationException because it doesn't exist anymore
     */
    @Deprecated
    public void setBaseAirTicks(int ticks) {
        throw new UnsupportedOperationException("BaseAirTicks has been abolished by Notch.");
    }

    /**
     * Get the current NoDamageTicks for this entity.
     *
     * This gets lowered every game tick, until its smaller than half the
     * BaseNoDamageTicks it only registers any damage more than
     * {@link LivingEntity#getLastDamage()}. 20 ticks per second.
     *
     * @return
     */
    @Todo("Figure out why this isn't in the recode")
    public int getNoDamageTicks() {
        // SRG return this.getEntity().field_70172_ad;
        return this.getEntity().ad;
    }

    /**
     * Set the current NoDamageTicks for this entity.
     *
     * This gets lowered every game tick, until its smaller than half the
     * BaseNoDamageTicks it only registers any damage more than
     * {@link LivingEntity#getLastDamage()}. 20 ticks per second.
     *
     * @param ticks
     */
    @Todo("Figure out why this isn't in the recode")
    public void setNoDamageTicks(int ticks) {
        // SRG this.getEntity().field_70172_ad = ticks;
        this.getEntity().ad = ticks;
    }

    /**
     * Get the amount of AirTicks left.
     *
     * This gets lowered every game tick when you are under water. 20 ticks per
     * second.
     *
     * @return
     */
    @Todo("Figure out why this isn't in the recode")
    public int getAirTicks() {
        // SRG return this.getEntity().func_70086_ai();
        return this.getEntity().ar();
    }

    /**
     * Set the amount of AirTicks left.
     *
     * This gets lowered every game tick when you are under water. 20 ticks per
     * second.
     *
     * @param ticks the number of ticks you have air
     */
    @Todo("Figure out why this isn't in the recode")
    public void setAirTicks(int ticks) {
        // SRG this.getEntity().func_70050_g(ticks);
        this.getEntity().h(ticks);
    }

    /**
     * Get the amount of FireTicks left.
     *
     * This gets lowered every game tick when you are on fire. 20 ticks per
     * second.
     *
     * @return
     */
    public int getFireTicks() {
        return entity.getFireTicks();
    }

    /**
     * Set the amount of FireTicks left.
     *
     * This gets lowered every game tick when you are on fire. 20 ticks per
     * second.
     *
     * @param ticks the amount of fire ticks
     */
    public void setFireTicks(int ticks) {
        entity.setFireTicks(ticks);
    }

    /**
     * Gets the World object from this entity.
     *
     * @return the World this entity is in
     */
    public World getWorld() {
        return Wrapper.wrap(entity.getWorld());
    }

    /**
     * Returns the x-motion of this entity
     *
     * @return x-motion
     */
    public double getMotionX() {
        return entity.getMotionX();
    }

    /**
     * Returns the y-motion of this entity
     *
     * @return y-motion
     */
    public double getMotionY() {
        return entity.getMotionY();
    }

    /**
     * Returns the z-motion of this entity
     *
     * @return z-motion
     */
    public double getMotionZ() {
        return entity.getMotionZ();
    }

    /**
     * Set entity motion
     *
     * @param motionX
     * @param motionY
     * @param motionZ
     */
    public void setMotion(double motionX, double motionY, double motionZ) {
        this.setMotionX(motionX);
        this.setMotionY(motionY);
        this.setMotionZ(motionZ);
    }

    /**
     * Sets the x-motion of this entity
     *
     * @param motion motion to set
     */
    public void setMotionX(double motion) {
        entity.setMotionX(motion);
    }

    /**
     * Sets the y-motion of this entity
     *
     * @param motion motion to set
     */
    public void setMotionY(double motion) {
        entity.setMotionY(motion);
    }

    /**
     * Sets the z-motion of this entity
     *
     * @param motion motion to set
     */
    public void setMotionZ(double motion) {
        entity.setMotionZ(motion);
    }

    /**
     * Destroys this entity
     */
    public void destroy() {
        entity.destroy();
    }

    /**
     * Returns this mob's name
     *
     * @return name
     */
    public String getName() {
        return entity.getName();
    }

    /**
     * Returns whether this entity is sprinting.
     *
     * @return the sprinting state
     */
    public boolean getSprinting() {
        return entity.isSprinting();
    }

    /**
     * Set whether this entity is sprinting. Note: for players, this may not
     * make them go faster.
     *
     * @param sprinting
     */
    public void setSprinting(boolean sprinting) {
        entity.setSprinting(sprinting);
    }

    /**
     * Writes this entity's data to an NBTTagCompound
     *
     * @param tag the tag to write the data to
     * @param includeId whether or not to include the entity id in the write
     * @return whether or not the write was successful
     */
    public boolean writeToTag(NBTTagCompound tag, boolean includeId) {
        if (includeId) {
            // SRG return getEntity().func_98035_c(tag.getBaseTag());
            return getEntity().c(tag.getBaseTag());
        }
        // SRG getEntity().func_70109_d(tag.getBaseTag());
        getEntity().e(tag.getBaseTag());
        return true;
    }

    /**
     * Reads this entity's data from an NBTTagCompound
     *
     * @param tag the tag to read the data from
     */
    public void readFromTag(NBTTagCompound tag) {
        // SRG getEntity().func_70020_e(tag.getBaseTag());
        getEntity().f(tag.getBaseTag());
    }

    @Override
    public NBTTagCompound getMetaTag() {
        return new NBTTagCompound(entity.getMetaData());
    }

    /**
     * Returns the location of this entity.
     *
     * @return location
     */
    public Location getLocation() {
        Location loc = new Location();

        loc.x = getX();
        loc.y = getY();
        loc.z = getZ();
        loc.rotX = getRotation();
        loc.rotY = getPitch();
        loc.setWorld(getWorld());
        return loc;
    }

    /**
     * Spawns this entity
     */
    public void spawn() {
        spawn((LivingEntity) null);
    }

    /**
     * Spawns this entity with a rider
     *
     * @param rider
     */
    public void spawn(LivingEntity rider) {
        entity.spawn(rider == null ? null : rider.getRecodeHandle());
    }

    /**
     * Returns whether or not this entity is invulnerable.
     *
     * @return
     */
    @Todo("Figure out why this isn't in the recode")
    public boolean isInvulnerable() {
        // SRG return getEntity().func_85032_ar();
        return getEntity().aw();
    }

    /**
     * Sets whether or not this entity is invulnerable.
     *
     * @param isInvulnerable
     */
    @Todo("Figure out why this isn't in the recode")
    public void setInvulnerable(boolean isInvulnerable) {
        // SRG Util.<Boolean>setField(net.minecraft.entity.Entity.class, getEntity(), "field_83001_bt", isInvulnerable);
        Util.<Boolean>setField(net.minecraft.entity.Entity.class, getEntity(), "i", isInvulnerable);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BaseEntity) {
            return ((BaseEntity) obj).getId() == getId();
        }
        return false;
    }

    @Override
    public String toString() {
        return String.format("Entity[id=%d, name=%s, location=%s]", getId(), getName(), getLocation());
    }

    /**
     * Gets the "passenger" for this entity. Passenger is any entity that is on
     * top of this entity. Example player in a minecart, the player is the
     * passenger.
     *
     * @return
     */
    public BaseEntity getRiddenByEntity() {
        return entity.isRidden() ? Wrapper.wrap(entity.getRider()) : null;
    }

    /**
     * Sets the entity's rider.
     *
     * @param entity
     */
    public void setRiddenByEntity(BaseEntity entity) {
        if (entity == null) {
            if (this.getRiddenByEntity() != null) {
                this.getRiddenByEntity().dismount();
            }
        } else {
            this.entity.setRider(entity.getRecodeHandle());
        }
    }

    /**
     * Get the "vehicle" for this entity. Vehicle is any entity that is
     * underneath this entity. Example player in a minecart, the minecart is the
     * vehicle.
     *
     * @return
     */
    public BaseEntity getRidingEntity() {
        return entity.isRiding() ? Wrapper.wrap(entity.getRiding()) : null;
    }

    /**
     * Sets the entity's "vehicle". Can also be used to dismount vehicle.
     *
     * @param entity
     */
    public void setRidingEntity(BaseEntity entity) {
        if (entity == null) {
            this.entity.dismount();
        } else {
            this.entity.mount(entity.getRecodeHandle());
        }
    }

    /**
     * Dismounts entity from vehicle.
     */
    public void dismount() {
        setRidingEntity(null);
    }

    /**
     * If this entity is marked as "to be removed". This commonly happens next
     * tick, so something can be destroyed and the next tick can still be
     * detected as being in the world.
     *
     * @return
     */
    public boolean isDead() {
        return entity.isDead();
    }

    /**
     * If this entity is touching the ground.
     *
     * @return boolean True if on ground, false if in midair.
     */
    @Todo("Figure out why this isn't in the recode")
    public boolean isOnGround() {
        // SRG return this.getEntity().field_70122_E;
        return this.getEntity().D;
    }

    /**
     * Gets the height of this entity's eyes above its feet. Will return 0 if
     * this entity has no eyes.
     *
     * @return
     */
    @Todo("Figure out why this isn't in the recode")
    public float getEyeHeight() {
        // SRG return getEntity().func_70047_e();
        return getEntity().g();
    }

    /**
     * Returns whether or not this entity is currently sneaking (crouching).
     *
     * @return true if sneaking
     */
    public boolean getSneaking() {
        return entity.isSneaking();
    }

    /**
     * Force this entity to be sneaking or not
     *
     * @param sneaking true if sneaking
     */
    public void setSneaking(boolean sneaking) {
        entity.setSneaking(sneaking);
    }

    /**
     * Gets the entity's mob spawner.
     *
     * @return MobSpawner of the entity, or null if it wasn't spawned with a mob
     * spawner.
     */
    @Todo("Track this ourselves")
    public MobSpawner getSpawner() {
        return null;
    }

    public Entity getRecodeHandle() {
        return entity;
    }

    public static BaseEntity[] fromRecodeArray(Entity[] recode) {
        BaseEntity[] classic = new BaseEntity[recode.length];
        for (int i = 0; i < recode.length; i++) {
            classic[i] = Wrapper.wrap(recode[i]);
        }
        return classic;
    }

    public static Entity[] toRecodeArray(BaseEntity[] classic) {
        Entity[] recode = new Entity[classic.length];
        for (int i = 0; i < classic.length; i++) {
            recode[i] = classic[i].getRecodeHandle();
        }
        return recode;
    }
}
