package com.minecraftonline.classicloader.classicapi;

import net.canarymod.Canary;
import net.canarymod.api.MobSpawnerEntry;
import net.canarymod.api.factory.ObjectFactory;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.MobSpawnerBaseLogic;

public class MobSpawnerLogic {

    private final MobSpawnerBaseLogic baseLogic;
    private final net.canarymod.api.MobSpawnerLogic logic;
    private final ObjectFactory factory = Canary.factory().getObjectFactory();

    public MobSpawnerLogic(MobSpawnerBaseLogic logic) {
        this.baseLogic = logic;
        this.logic = logic.logic;
    }

    public int getX() {
        // SRG return baseLogic.func_98275_b();
        return baseLogic.b();
    }

    public int getY() {
        // SRG return baseLogic.func_98274_c();
        return baseLogic.c();
    }

    public int getZ() {
        // SRG return baseLogic.func_98266_d();
        return baseLogic.d();
    }

    public World getWorld() {
        // SRG return Wrapper.wrap(baseLogic.func_98271_a().getCanaryWorld());
        return Wrapper.wrap(baseLogic.a().getCanaryWorld());
    }

    public void update() {
        // SRG baseLogic.func_98278_g();
        baseLogic.g();
    }

    /**
     * Allows what to spawn to change on-the-fly.
     * <strong>NB:</strong> This only works if a complex entity has not been
     * set.
     *
     * @param spawn The name of the entity to spawn.
     * @see #setSpawnedEntity(BaseEntity)
     * @see #setSpawnedEntity(Item)
     */
    public void setSpawn(String spawn) {
        logic.setSpawnedEntities(Canary.factory().getObjectFactory().newMobSpawnerEntry(spawn));
    }

    /**
     * Returns the spawn used.
     *
     * @return
     */
    public String getSpawn() {
        return logic.getSpawns()[0];
   }

    /**
     * Returns the current delay of the spawner. When this reaches 0, the
     * spawner tries to spawn a mob.
     *
     * @return The current delay in ticks.
     */
    public int getDelay() {
        // SRG return baseLogic.field_98286_b;
        return baseLogic.b;
    }

    /**
     * Allows delay of what to spawn to change on-the-fly. Modification of this
     * is near-useless as delays get randomized after spawn. Setting this to 0
     * causes the spawner to try to spawn a mob on the next tick.
     *
     * @param delay The new delay
     */
    public void setDelay(int delay) {
        logic.setDelay(delay);
    }

    /**
     * Returns the minimum delay of the spawner. The delay between spawns is
     * picked randomly between this and the max delay.
     *
     * @return
     */
    public int getMinDelay() {
        return logic.getMinDelay();
    }

    /**
     * Sets the minimum delay of the spawner. The delay between spawns is picked
     * randomly between this and the max delay. Default is 200.
     *
     * @param delay
     */
    public void setMinDelay(int delay) {
        logic.setMinDelay(delay);
    }

    /**
     * Returns the maximum delay of the spawner. The delay between spawns is
     * picked randomly between this and the min delay.
     *
     * @return
     */
    public int getMaxDelay() {
        return logic.getMaxDelay();
    }

    /**
     * Sets the maximum delay of the spawner. The delay between spawns is picked
     * randomly between this and the min delay. Default is 800.
     *
     * @param delay
     */
    public void setMaxDelay(int delay) {
        logic.setMaxDelay(delay);
    }

    /**
     * Returns the amount of mobs this spawner attempts to spawn.
     *
     * @return
     */
    public int getSpawnCount() {
        return logic.getSpawnCount();
    }

    /**
     * Sets the amount of mobs this spawner attempts to spawn. Default is 4.
     *
     * @param count
     */
    public void setSpawnCount(int count) {
        logic.setSpawnCount(count);
    }

    /**
     * Returns the maximum number of entities this spawner allows nearby in
     * order to continue spawning. Any more entities and this spawner won't
     * spawn mobs.
     *
     * @return
     */
    public int getMaxNearbyEntities() {
        return logic.getMaxNearbyEntities();
    }

    /**
     * Sets the maximum number of entities this spawner allows nearby in order
     * to continue spawning. Any more entities and this spawner won't spawn
     * mobs. Default is 6.
     *
     * @param entities
     */
    public void setMaxNearbyEntities(int entities) {
        logic.setMaxNearbyEntities(entities);
    }

    /**
     * If there are no players within this distance of the spawner, it won't
     * spawn.
     *
     * @return
     */
    public int getRequiredPlayerRange() {
        return logic.getRequiredPlayerRange();
    }

    /**
     * If there are no players within this distance of the spawner, it won't
     * spawn. Default is 16.
     *
     * @param range
     */
    public void setRequiredPlayerRange(int range) {
        logic.setRequiredPlayerRange(range);
    }

    /**
     * Returns the maximum distance that this spawner will spawn mobs at.
     *
     * @return
     */
    public int getSpawnRange() {
        return logic.getSpawnRange();
    }

    /**
     * Sets the maximum distance that this spawner will spawn mobs at. Default
     * is 4.
     *
     * @param range
     */
    public void setSpawnRange(int range) {
        logic.setSpawnRange(range);
    }

    /**
     * Sets the entity spawned by this spawner.
     *
     * @param entity The entity this spawner should spawn
     */
    public void setSpawnedEntity(BaseEntity entity) {
        logic.setSpawnedEntities(factory.newMobSpawnerEntry(entity.getRecodeHandle()));
    }

    /**
     * Sets the entity spawned by this spawner to an item.
     *
     * @param item The item this spawner should spawn
     */
    public void setSpawnedEntity(Item item) {
        logic.setSpawnedEntities(factory.newMobSpawnerEntry(item.getRecodeHandle()));
    }

    /**
     * Sets the entity spawned by this spawner.
     *
     * @param entity The entity this spawner should spawn
     */
    public void setSpawnedEntity(Entity entity) {
        logic.setSpawnedEntities(factory.newMobSpawnerEntry(entity.getCanaryEntity()));
    }

    /**
     * Sets the entities spawned by this spawner.
     *
     * @param entities The entities this spawner should spawn
     */
    public void setSpawnedEntity(BaseEntity... entities) {
        net.canarymod.api.entity.Entity[] recode = BaseEntity.toRecodeArray(entities);

        MobSpawnerEntry[] mse = new MobSpawnerEntry[recode.length];
        for (int i = 0; i < recode.length; i++) {
            mse[i] = factory.newMobSpawnerEntry(recode[i]);
        }

        logic.setSpawnedEntities(mse);
    }

    /**
     * Sets the entities spawned by this spawner to items.
     *
     * @param items The items this spawner should spawn
     */
    public void setSpawnedEntity(Item... items) {
        net.canarymod.api.inventory.Item[] recode = Item.toRecodeArray(items);

        MobSpawnerEntry[] mse = new MobSpawnerEntry[recode.length];
        for (int i = 0; i < recode.length; i++) {
            mse[i] = factory.newMobSpawnerEntry(recode[i]);
        }

        logic.setSpawnedEntities(mse);
    }

    /**
     * Sets the entities spawned by this spawner.
     *
     * @param entities The list of entities this spawner should spawn
     */
    public void setSpawnedEntity(Object[] entities) {
        Entity[] nms = (Entity[]) entities;

        MobSpawnerEntry[] mse = new MobSpawnerEntry[nms.length];
        for (int i = 0; i < nms.length; i++) {
            mse[i] = factory.newMobSpawnerEntry(nms[i].getCanaryEntity());
        }

        logic.setSpawnedEntities(mse);
    }

    /**
     * Returns the wrapped (native) logic object.
     *
     * @return the wrapped <tt>OMobSpawnerBaseLogic</tt>
     */
    public MobSpawnerBaseLogic getLogic() {
        return baseLogic;
    }

    /**
     * Writes this logic's data to an NBTTagCompound.
     *
     * @param tag the tag to write the data to
     */
    public void writeToTag(NBTTagCompound tag) {
        // SRG baseLogic.func_98280_b(tag.getBaseTag());
        baseLogic.b(tag.getBaseTag());
    }

    /**
     * Reads this logic's data from an NBTTagCompound.
     *
     * @param tag the tag to read the data from
     */
    public void readFromTag(NBTTagCompound tag) {
        // SRG baseLogic.func_98270_a(tag.getBaseTag());
        baseLogic.a(tag.getBaseTag());
    }
}
