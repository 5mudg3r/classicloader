package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.entity.living.monster.CanaryZombie;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.monster.EntityZombie;

/**
 * Interface for zombies
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(net.canarymod.api.entity.living.monster.Zombie.class)
public class Zombie extends Mob {

    /**
     * Creates a zombie wrapper
     *
     * @param oentity The entity to wrap
     */
    public Zombie(EntityZombie oentity) {
        super(oentity);
    }

    /**
     * Creates a new zombie
     *
     * @param world The world in which to create it
     */
    public Zombie(World world) {
        super("Zombie", world);
    }

    /**
     * Creates a new zombie
     *
     * @param location The location at which to create it
     */
    public Zombie(Location location) {
        super("Zombie", location);
    }

    public Zombie(net.canarymod.api.entity.living.monster.Zombie zombie) {
        super(zombie);
    }

    /**
     * Returns whether or not this zombie is a baby
     *
     * @return
     */
    public boolean isBaby() {
        return getRecodeHandle().isChild();
    }

    /**
     * Sets whether or not this zombie is a baby
     *
     * @param isBaby
     */
    public void setBaby(boolean isBaby) {
        getRecodeHandle().setChild(isBaby);
    }

    /**
     * Returns whether or not this zombie is a zombie villager
     *
     * @return
     */
    public boolean isVillager() {
        return getRecodeHandle().isVillager();
    }

    /**
     * Sets whether or not this zombie is a zombie villager
     *
     * @param isVillager
     */
    public void setVillager(boolean isVillager) {
        getRecodeHandle().setVillager(isVillager);
    }

    @Override
    public EntityZombie getEntity() {
        return (EntityZombie) super.getEntity();
    }

    @Override
    public net.canarymod.api.entity.living.monster.Zombie getRecodeHandle() {
        return (net.canarymod.api.entity.living.monster.Zombie) super.getRecodeHandle();
    }
}
