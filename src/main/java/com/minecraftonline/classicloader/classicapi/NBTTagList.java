package com.minecraftonline.classicloader.classicapi;

import java.util.ArrayList;
import java.util.Arrays;
import net.canarymod.Canary;
import net.canarymod.api.nbt.BaseTag;
import net.canarymod.api.nbt.ListTag;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/**
 * Interface for the net.minecraft.server.NBTTagList class
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(ListTag.class)
public class NBTTagList extends NBTBase {

    /**
     * Wraps an net.minecraft.server.NBTTagList
     *
     * @param baseTag the tag to wrap
     */
    public NBTTagList(net.minecraft.nbt.NBTTagList baseTag) {
        super(baseTag);
    }

    /**
     * Creates a new NBTTagList
     */
    public NBTTagList() {
        this(Canary.factory().getNBTFactory().newListTag());
    }

    /**
     * Creates a new NBTTagList
     *
     * @param name the name of the tag
     */
    public NBTTagList(String name) {
        this(Canary.factory().getNBTFactory().newListTag());
    }

    @SuppressWarnings("rawtypes")
    public NBTTagList(ListTag listTag) {
        super(listTag);
    }

    @Override
    public net.minecraft.nbt.NBTTagList getBaseTag() {
        return (net.minecraft.nbt.NBTTagList) super.getBaseTag();
    }

    /**
     * Returns the size of this list
     *
     * @return
     */
    public int size() {
        return getRecodeHandle().size();
    }

    /**
     * Adds a tag to this list
     *
     * @param tag
     */
    public void add(NBTBase tag) {
        getRecodeHandle().add(tag.getRecodeHandle());
    }

    /**
     * Gets the tag at the specified index
     *
     * @param index
     * @return
     */
    public NBTBase get(int index) {
        // SRG return NBTBase.wrap(getBaseTag().field_74747_a.get(index));
        return NBTBase.wrap((net.minecraft.nbt.NBTBase) getBaseTag().b.get(index));
    }

    /**
     * Returns an array of the values in this NBTTagList.
     *
     * @return
     */
    public NBTBase[] getValues() {
        NBTBase[] vals = new NBTBase[size()];
        for (int i = 0; i < vals.length; i++) {
            vals[i] = get(i);
        }
        return vals;
    }

    @Override
    public String toString() {
        return String.format("NBTTag[type=%s, name=%s, value=%s]", getTagName(getType()), getName(), Arrays.toString(getValues()));
    }

    /**
     * Removes the NBTTag at the specified index.
     *
     * @param index The index of the tag to remove.
     */
    public void remove(int index) {
        // SRG getBaseTag().field_74747_a.remove(index);
        getBaseTag().b.remove(index);
    }

    @Override
    @SuppressWarnings({"unchecked","rawtypes"})
    public ListTag getRecodeHandle() {
        return (ListTag) super.getRecodeHandle();
    }

    public NBTTagList[] fromRecodeArray(ListTag<BaseTag>[] recode) {
        NBTTagList[] classic = new NBTTagList[recode.length];
        for (int i = 0; i < recode.length; i++) {
            classic[i] = new NBTTagList(recode[i]);
        }
        return classic;
    }

    @SuppressWarnings("unchecked")
    public ListTag<BaseTag>[] toRecodeArray(NBTTagList[] classic) {
        // Something to do with generics and type safety...
        ArrayList<ListTag<BaseTag>> recodeList = new ArrayList<ListTag<BaseTag>>(classic.length);
        for (int i = 0; i < classic.length; i++) {
            recodeList.add(i, classic[i].getRecodeHandle());
        }
        return (ListTag<BaseTag>[]) recodeList.toArray();
    }
}
