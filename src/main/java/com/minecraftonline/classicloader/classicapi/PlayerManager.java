package com.minecraftonline.classicloader.classicapi;

import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.player.EntityPlayerMP;

/**
 * CanaryMod Player manager wrapper
 *
 * @author Chris Ksoll
 *
 */
@RecodeProxy(net.canarymod.api.PlayerManager.class)
public class PlayerManager {

    private net.canarymod.api.PlayerManager pm;

    public PlayerManager(net.minecraft.server.management.PlayerManager pm) {
        this.pm = pm.getPlayerManager();
    }

    public PlayerManager(net.canarymod.api.PlayerManager pm) {
        this.pm = pm;
    }

    /**
     * Update the given player
     *
     * @param player
     */
    public void updateMountedMovingPlayer(EntityPlayerMP player) {
        pm.updateMountedMovingPlayer(player.getPlayer());
    }

    /**
     * Update given player
     *
     * @param player
     */
    public void updateMountedMovingPlayer(Player player) {
        this.updateMountedMovingPlayer(player.getEntity());
    }

    /**
     * Add player to this player manager
     *
     * @param player
     */
    public void addPlayer(EntityPlayerMP player) {
        pm.addPlayer(player.getPlayer());
    }

    /**
     * Remove the given player from this player manager
     *
     * @param player
     */
    public void removePlayer(EntityPlayerMP player) {
        pm.removePlayer(player.getPlayer());
    }

    public void markBlockNeedsUpdate(int x, int y, int z) {
        pm.markBlockNeedsUpdate(x, y, z);
    }

    public net.canarymod.api.PlayerManager getRecodeHandle() {
        return pm;
    }
}
