package com.minecraftonline.classicloader.classicapi;

import ca.grimoire.critical.Todo;
import net.canarymod.api.entity.living.monster.CanarySkeleton;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/**
 * Interface for skeletons
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(net.canarymod.api.entity.living.monster.Skeleton.class)
public class Skeleton extends Mob {

    /**
     * Creates a skeleton wrapper
     *
     * @param oentity The entity to wrap
     */
    public Skeleton(net.minecraft.entity.monster.EntitySkeleton oentity) {
        super(oentity);
    }

    /**
     * Creates a new skeleton
     *
     * @param world The world to create it in
     */
    public Skeleton(World world) {
        super("Skeleton", world);
    }

    /**
     * Creates a new skeleton
     *
     * @param location The location at which to create it
     */
    public Skeleton(Location location) {
        super("Skeleton", location);
    }

    public Skeleton(net.canarymod.api.entity.living.monster.Skeleton skeleton) {
        super(skeleton);
    }

    /**
     * Returns whether or not this is a wither skeleton
     *
     * @return
     */
    public boolean isWither() {
        return getRecodeHandle().isWitherSkeleton();
    }

    /**
     * Sets whether or not this is a wither skeleton
     *
     * @param isWither
     */
    public void setWither(boolean isWither) {
        getRecodeHandle().setIsWitherSkeleton(isWither);
    }

    @Override
    public net.minecraft.entity.monster.EntitySkeleton getEntity() {
        return (net.minecraft.entity.monster.EntitySkeleton) super.getEntity();
    }

    /**
     * Make this skeleton launch an arrow at the living entity. This method
     * mimics Minecraft's force calculation.
     *
     * @param ent the entity to shoot at
     */
    public void shootAt(LivingEntity ent) {
        double distanceToEntity = this.getLocation().distanceTo(ent.getLocation());
        float power = Math.min(Math.max(0.1F, (float) distanceToEntity / 15F), 1.0F);
        this.shootAt(ent, power);
    }

    /**
     * Make this skeleton launch an arrow at the entity with specified power.
     *
     * @param ent The entity to shoot at
     * @param power The power to shoot with (0.0-1.0)
     */
    @Todo("Pull request this shit")
    public void shootAt(LivingEntity ent, float power) {
        // SRG getEntity().func_82196_d(ent.getEntity(), power);
        getEntity().a(ent.getEntity(), power);
    }

    @Override
    public net.canarymod.api.entity.living.monster.Skeleton getRecodeHandle() {
        return (net.canarymod.api.entity.living.monster.Skeleton) super.getRecodeHandle();
    }
}
