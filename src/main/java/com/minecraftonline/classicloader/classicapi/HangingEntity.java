package com.minecraftonline.classicloader.classicapi;

import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.EntityHanging;

/**
 * Interface for hanging entities (paintings, item frames, etc)
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(net.canarymod.api.entity.hanging.HangingEntity.class)
public class HangingEntity extends BaseEntity {

    public enum Position {

        /**
         * Faces south and is located on the block face
         */
        SOUTH_FACE,
        /**
         * Faces west and is located on the block face
         */
        WEST_FACE,
        /**
         * Faces north and is located on the block face
         */
        NORTH_FACE,
        /**
         * Faces east and is located on the block face
         */
        EAST_FACE,
        /**
         * Faces north and is located in the center of the block
         */
        NORTH_CENTER,
        /**
         * Faces west and is located in the center of the block
         */
        WEST_CENTER,
        /**
         * Faces south and is located in the center of the block
         */
        SOUTH_CENTER,
        /**
         * Faces east and is located in the center of the block
         */
        EAST_CENTER;
    }

    /**
     * Creates a new hanging entity wrapper
     *
     * @param entity The hanging entity to wrap
     */
    public HangingEntity(EntityHanging entity) {
        super(entity);
    }

    public HangingEntity(net.canarymod.api.entity.hanging.HangingEntity entity) {
        super(entity);
    }

    /**
     * Places this entity in its world
     *
     */
    public void place() {
        this.spawn();
    }

    @Override
    public EntityHanging getEntity() {
        return (EntityHanging) super.getEntity();
    }

    /**
     * Returns the position of this hanging entity around its block
     *
     * @return
     */
    public Position getPosition() {
        return Position.values()[getRecodeHandle().getHangingDirection()];
    }

    /**
     * Sets the position of this hanging entity around its block
     *
     * @param position The position to set it to
     */
    public void setPosition(Position position) {
        getRecodeHandle().setHangingDirection(position.ordinal());
    }

    @Override
    public net.canarymod.api.entity.hanging.HangingEntity getRecodeHandle() {
        return (net.canarymod.api.entity.hanging.HangingEntity) super.getRecodeHandle();
    }
}
