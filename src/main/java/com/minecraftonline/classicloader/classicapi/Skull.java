package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.world.blocks.CanarySkull;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.tileentity.TileEntitySkull;

/**
 * Interface for the OTileEntitySkull class
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(net.canarymod.api.world.blocks.Skull.class)
public class Skull extends ComplexBlockBase<TileEntitySkull> implements ComplexBlock {

    public Skull(TileEntitySkull skullBase) {
        super(skullBase);
    }

    public Skull(net.canarymod.api.world.blocks.Skull skull) {
        super(skull);
    }

    /**
     * Returns the name of the skin this skull shows, if any
     *
     * @return
     */
    public String getPlayerName() {
        return getRecodeHandle().getExtraType();
    }

    /**
     * Sets the type of skull this is
     *
     * @param type the type of mob this belongs to (0-4)
     * @param playerName determines the skin of this head if it's a player head
     */
    public void setType(int type, String playerName) {
        getRecodeHandle().setSkullAndExtraType(type, playerName);
    }

    /**
     * Sets the orientation of this skull (0-15?)
     *
     * @param rot
     */
    public void setOrientation(int rot) {
        getRecodeHandle().setRotation(rot);
    }

    public TileEntitySkull getBaseSkull() {
        return ((CanarySkull) tileEntity).getTileEntity();
    }

    @Override
    public net.canarymod.api.world.blocks.Skull getRecodeHandle() {
        return (net.canarymod.api.world.blocks.Skull) super.getRecodeHandle();
    }
}
