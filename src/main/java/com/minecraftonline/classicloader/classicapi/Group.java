package com.minecraftonline.classicloader.classicapi;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Group.java - Group stuff.
 *
 * @author James
 */
public class Group {

    /**
     * Group ID - used for database transactions
     */
    public int ID;
    /**
     * Group Name
     */
    public String Name;
    /**
     * Group Prefix/Color
     */
    public String Prefix;
    /**
     * List of commands this group can use
     */
    public String[] Commands;
    /**
     * List of groups this group inherits/has control over
     */
    public String[] InheritedGroups;
    /**
     * Is true if it's the default group
     */
    public boolean DefaultGroup;
    /**
     * If true all players within this group ignore restrictions
     */
    public boolean IgnoreRestrictions;
    /**
     * If true all players within this group have administrator privileges
     */
    public boolean Administrator;
    /**
     * If false this player can not modify chests or furnaces and can not
     * destroy/create blocks
     */
    public boolean CanModifyWorld = true;

    public static Group fromRecode(net.canarymod.user.Group recode) {
        Group group = new Group();

        group.Administrator = recode.isAdministratorGroup();
        group.CanModifyWorld = recode.canBuild();
        group.DefaultGroup = recode.isDefaultGroup();
        group.IgnoreRestrictions = recode.canIgnorerestrictions();
        group.Name = recode.getName();
        group.Prefix = recode.getPrefix();

        List<String> permissions = recode.getPermissionProvider().getPermissionsAsStringList();
        Set<String> commands = new HashSet<String>();
        for (String permission : permissions) {
            if (permission.startsWith("com.minecraftonline.classicloader.command.")) {
                commands.add(permission.substring("com.minecraftonline.classicloader.command.".length()));
            }
        }
        group.Commands = commands.toArray(new String[0]);

        List<net.canarymod.user.Group> parents = recode.parentsToList();
        String[] inherited = new String[parents.size()];
        int i = 0;
        for (net.canarymod.user.Group parent : parents) {
            inherited[i++] = parent.getName();
        }
        group.InheritedGroups = inherited;


        return group;
    }
}
