package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.DyeColor;
import net.canarymod.api.entity.living.animal.CanarySheep;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/**
 * Wrap around an net.minecraft.server.EntitySheep to provide extra methods not
 * available to anything but sheep. Mostly wool options.
 *
 * @author Brian McCarthy
 *
 */
@RecodeProxy(net.canarymod.api.entity.living.animal.Sheep.class)
public class Sheep extends Mob {

    /**
     * Basic sheep constructor.
     *
     * @param entity An instance of net.minecraft.server.EntitySheep to wrap
     * around.
     */
    public Sheep(net.minecraft.entity.passive.EntitySheep entity) {
        super(entity);
    }

    public Sheep(net.canarymod.api.entity.living.animal.Sheep sheep) {
        super(sheep);
    }

    /**
     * Set if the sheep has wool.
     *
     * @param sheared Sheared or not
     */
    public void setSheared(boolean sheared) {
        getRecodeHandle().setSheared(sheared);
    }

    /**
     * Returns if the sheep has wool.
     *
     * @return If this sheep has wool.
     */
    public boolean isSheared() {
        return getRecodeHandle().isSheared();
    }

    /**
     * Set the color of the wool.
     *
     * @param color int value of the new color.
     */
    public void setWoolColor(int color) {
        getRecodeHandle().setColor(DyeColor.fromDyeColorCode(color));
    }

    /**
     * Returns an int of the color of the wool.
     *
     * @return int value of wool color.
     */
    public int getColor() {
        return getRecodeHandle().getColor().getDyeColorCode();
    }

    /**
     * Returns a Cloth.Color value of the wool.
     *
     * @return Cloth.Color of the wool. This is useful for getting the name of
     * the wool.
     */
    public Cloth.Color getClothColor() {
        return Cloth.Color.getColor(getRecodeHandle().getColor().getDyeColorCode());
    }

    @Override
    public net.minecraft.entity.passive.EntitySheep getEntity() {
        return (net.minecraft.entity.passive.EntitySheep) super.getEntity();
    }

    @Override
    public net.canarymod.api.entity.living.animal.Sheep getRecodeHandle() {
        return (net.canarymod.api.entity.living.animal.Sheep) super.getRecodeHandle();
    }
}
