package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.world.blocks.TileEntity;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author willem
 */
@RecodeProxy(TileEntity.class)
public abstract class ComplexBlockBase<T extends net.minecraft.tileentity.TileEntity> implements ComplexBlock {

    TileEntity tileEntity;

    protected ComplexBlockBase(T tileEntity) {
        this(tileEntity.complexBlock);
    }

    public ComplexBlockBase(TileEntity entity) {
        tileEntity = entity;
    }

    @Override
    public int getX() {
        return tileEntity.getX();
    }

    @Override
    public int getY() {
        return tileEntity.getY();
    }

    @Override
    public int getZ() {
        return tileEntity.getZ();
    }

    @Override
    public World getWorld() {
        return Wrapper.wrap(tileEntity.getWorld());
    }

    @Override
    public void update() {
        tileEntity.update();
    }

    @Override
    public Block getBlock() {
        return getWorld().getBlockAt(getX(), getY(), getZ());
    }

    @Override
    public NBTTagCompound getMetaTag() {
        return new NBTTagCompound(tileEntity.getMetaTag());
    }

    @Override
    public void readFromTag(NBTTagCompound tag) {
        tileEntity.readFromTag(tag.getRecodeHandle());
    }

    @Override
    public void writeToTag(NBTTagCompound tag) {
        tileEntity.writeToTag(tag.getRecodeHandle());
    }

    public TileEntity getRecodeHandle() {
        return tileEntity;
    }
}
