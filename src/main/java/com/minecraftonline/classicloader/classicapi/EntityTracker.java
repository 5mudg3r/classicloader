package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.CanaryEntityTracker;
import net.canarymod.api.packet.CanaryPacket;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.Packet;

/**
 * CanaryMod entity tracker wrapper. In order to manage your players per world
 *
 * @author Chris Ksoll
 *
 */
@RecodeProxy(net.canarymod.api.EntityTracker.class)
public class EntityTracker {

    private final net.canarymod.api.EntityTracker tracker;

    public EntityTracker(net.minecraft.entity.EntityTracker tracker) {
        this.tracker = tracker.getCanaryEntityTracker();
    }

    public EntityTracker(net.canarymod.api.EntityTracker tracker) {
        this.tracker = tracker;
    }

    /**
     * Add a player to this entity tracker
     *
     * @param player
     */
    public void trackPlayer(Player player) {
        this.trackEntity(player.getEntity());
    }

    /**
     * Track a new entity
     *
     * @param entity
     */
    public void trackEntity(Entity entity) {
        tracker.trackEntity(entity.getCanaryEntity());
    }

    /**
     * Remove a player from this entity tracker
     *
     * @param player
     */
    public void untrackPlayer(Player player) {
        this.untrackEntity(player.getEntity());
    }

    public void untrackEntity(Entity entity) {
        tracker.untrackEntity(entity.getCanaryEntity());
    }

    public void untrackPlayerSymmetrics(EntityPlayerMP player) {
        tracker.untrackPlayerSymmetrics(player.getPlayer());
    }

    public void updateTrackedEntities() {
        tracker.updateTrackedEntities();
    }

    public void sendPacketToPlayersAndEntity(Entity entity, Packet packet) {
        tracker.sendPacketToTrackedPlayer(((EntityPlayerMP) entity).getPlayer(), new CanaryPacket(packet));
    }

    public net.minecraft.entity.EntityTracker getTracker() {
        return ((CanaryEntityTracker) tracker).getHandle();
    }

    public net.canarymod.api.EntityTracker getRecodeHandle() {
        return tracker;
    }
}
