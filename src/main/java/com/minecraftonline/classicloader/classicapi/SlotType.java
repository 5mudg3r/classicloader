package com.minecraftonline.classicloader.classicapi;

import net.minecraft.inventory.*;
import net.minecraft.inventory.ContainerBeacon.BeaconSlot;
import net.minecraft.inventory.ContainerBrewingStand.Ingredient;
import net.minecraft.inventory.ContainerBrewingStand.Potion;

/**
 * Class to help define gui slots.
 *
 * @author m4411k4
 *
 */
public class SlotType {

    public static final int OUTSIDE = -999;

    public enum Type {

        DEFAULT,
        UNKNOWN_UPDATE_NEEDED,
        NULL,
        OUTSIDE,
        ARMOR,
        BEACON,
        BREWING_STAND_INGREDIENT,
        BREWING_STAND_POTION,
        CRAFTING,
        ENCHANTMENT,
        FURNACE,
        MERCHANT_RESULT,
        REPAIR,
        SADDLE,
        HORSE_ARMOUR,
    };

    public enum SpecificType {

        NULL,
        OUTSIDE,
        ARMOR,
        CONTAINER,
        CRAFT,
        ENCHANT,
        FUEL,
        INVENTORY,
        RESULT,
        PAYMENT,
        POTION,
        QUICKBAR,
        TRADE,
    };

    /**
     * A more reliable slot type. If a new slot type is introduced, it will
     * return that this needs to be updated.
     *
     * @return SlotType
     */
    public static Type getSlotType(net.minecraft.inventory.Container container, int slotIndex) {
        if (container == null) {
            return Type.NULL;
        }

        if (slotIndex == OUTSIDE) {
            return Type.OUTSIDE;
        }

        Slot slot = container.a(slotIndex);
        if (slot.getClass().isAnonymousClass()) {
            Class<?> containerClass = slot.getClass().getEnclosingClass();
            if (containerClass.equals(ContainerPlayer.class)) {
                return Type.ARMOR;
            } else if (containerClass.equals(ContainerEnchantment.class)) {
                return Type.ENCHANTMENT;
            } else if (containerClass.equals(ContainerRepair.class)) {
                return Type.REPAIR;
            } else if (containerClass.equals(ContainerHorseInventory.class)) {
                // WARNING CAUTION XXX ETCETERA: THIS DEPENDS ON CODE ORDER!!!
                if (slot.getClass().getName().equals("net.minecraft.inventory.ContainerHorseInventory$1")) {
                    return Type.SADDLE;
                } else if (slot.getClass().getName().equals("net.minecraft.inventory.ContainerHorseInventory$2")) {
                    return Type.HORSE_ARMOUR;
                } else {
                    return Type.UNKNOWN_UPDATE_NEEDED;
                }
            } else {
                return Type.UNKNOWN_UPDATE_NEEDED;
            }
        } else if (slot instanceof BeaconSlot) {
            return Type.BEACON;
        } else if (slot instanceof Ingredient) {
            return Type.BREWING_STAND_INGREDIENT;
        } else if (slot instanceof Potion) {
            return Type.BREWING_STAND_POTION;
        } else if (slot instanceof SlotCrafting) {
            return Type.CRAFTING;
        } else if (slot instanceof SlotFurnace) {
            return Type.FURNACE;
        } else if (slot instanceof SlotMerchantResult) {
            return Type.MERCHANT_RESULT;
        } else if (slot.getClass() != Slot.class) {
            return Type.UNKNOWN_UPDATE_NEEDED;
        } else {
            return Type.DEFAULT;
        }
    }

    /**
     * Attempts to define slots. Minecraft updates can break the definitions if
     * slots are added/removed/modified.
     *
     * @return
     */
    public static SpecificType getSpecificSlotType(net.minecraft.inventory.Container container, int slotIndex) {
        if (container == null) {
            return SpecificType.NULL;
        }

        if (slotIndex == OUTSIDE) {
            return SpecificType.OUTSIDE;
        }

        net.canarymod.api.inventory.Inventory inv = container.getInventory();
        if (inv == null) {
            return SpecificType.NULL;
        }

        if (slotIndex < inv.getSize()) {
            if (container instanceof ContainerBeacon) {
                switch (slotIndex) {
                    default:
                        return SpecificType.PAYMENT;
                }
            } else if (container instanceof ContainerBrewingStand) {
                switch (slotIndex) {
                    case 3:
                        return SpecificType.CRAFT;
                    default:
                        return SpecificType.POTION;
                }
            } else if (container instanceof ContainerChest) {
                switch (slotIndex) {
                    default:
                        return SpecificType.CONTAINER;
                }
            } else if (container instanceof ContainerDispenser) {
                switch (slotIndex) {
                    default:
                        return SpecificType.CONTAINER;
                }
            } else if (container instanceof ContainerEnchantment) {
                switch (slotIndex) {
                    default:
                        return SpecificType.ENCHANT;
                }
            } else if (container instanceof ContainerFurnace) {
                switch (slotIndex) {
                    case 0:
                        return SpecificType.CRAFT;
                    case 1:
                        return SpecificType.FUEL;
                    default:
                        return SpecificType.RESULT;
                }
            } else if (container instanceof ContainerMerchant) {
                switch (slotIndex) {
                    case 0:
                    case 1:
                        return SpecificType.TRADE;
                    default:
                        return SpecificType.RESULT;
                }
            } else if (container instanceof ContainerPlayer) {
                if (slotIndex == 0) {
                    return SpecificType.RESULT;
                }
                if (slotIndex <= 4) {
                    return SpecificType.CRAFT;
                }
            } else if (container instanceof ContainerRepair) {
                switch (slotIndex) {
                    case 0:
                    case 1:
                        return SpecificType.CRAFT;
                    default:
                        return SpecificType.RESULT;
                }
            } else if (container instanceof ContainerWorkbench) {
                if (slotIndex == 0) {
                    return SpecificType.RESULT;
                }
                if (slotIndex <= 9) {
                    return SpecificType.CRAFT;
                }
            }
        }

        int localSlot = slotIndex - inv.getSize();

        if (container instanceof ContainerPlayer) {
            if (localSlot < 4) {
                return SpecificType.ARMOR;
            }

            localSlot -= 4; //remove armor index
        }

        if (localSlot >= 27 && localSlot < 36) {
            return SpecificType.QUICKBAR;
        }

        return SpecificType.INVENTORY;
    }
}
