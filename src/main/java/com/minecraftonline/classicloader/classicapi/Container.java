package com.minecraftonline.classicloader.classicapi;

/**
 * Generic interface for containers of different types.
 * @param <T> The type of item contained.
 */
public interface Container<T> {

    /**
     * Returns the contents of this container.
     * @return this container's contents as an array.
     */
    public T[] getContents();

    /**
     * Sets the contents of this container.
     * @param values an array containing this container's new contents.
     */
    public void setContents(T[] values);

    /**
     * Get an item at a specified index.
     * @param index the index of the item you want to get.
     * @return The item at {@code index}.
     */
    public T getContentsAt(int index);

    /**
     * Set the item at a specified index.
     * @param index the index of the item you want to set.
     * @param value the item you want to set at {@code index}
     */
    public void setContentsAt(int index, T value);

    /**
     * Returns the size of this container.
     * @return this container's size.
     */
    public int getContentsSize();

    /**
     * Returns the name of this container.
     * @return this container's name.
     */
    public String getName();

    /**
     * Sets the name of this container.
     * @param value this container's new name.
     */
    public void setName(String value);
}
