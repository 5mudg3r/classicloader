package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.entity.throwable.ChickenEgg;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.projectile.EntityEgg;

import static net.canarymod.api.entity.EntityType.CHICKENEGG;

/**
 * Interface for the EntityEgg class
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(ChickenEgg.class)
public class Egg extends ThrowableProjectile {

    /**
     * Wraps an EntityEgg
     *
     * @param base the EntityEgg to wrap
     */
    public Egg(EntityEgg base) {
        super(base);
    }

    /**
     * Creates a new EntityEgg
     *
     * @param world the world to create it in
     */
    public Egg(World world) {
        this((ChickenEgg) factory.newThrowable(CHICKENEGG, world.getRecodeHandle()));
    }

    /**
     * Creates a new EntityEgg
     *
     * @param world the world to create it in
     * @param shooter the shooter of the egg
     */
    public Egg(World world, LivingEntity shooter) {
        this((ChickenEgg) factory.newThrowable(CHICKENEGG, world.getRecodeHandle()));
        setShooter(shooter);
    }

    /**
     * Creates a new EntityEgg
     *
     * @param world the world in which to create it
     * @param x the x coordinate at which to create it
     * @param y the y coordinate at which to create it
     * @param z the z coordinate at which to create it
     */
    public Egg(World world, double x, double y, double z) {
        this((ChickenEgg) factory.newThrowable(CHICKENEGG, new Location(world, x, y, z, 0, 0).getRecodeLocation()));
    }

    /**
     * Creates a new EntityEgg
     *
     * @param location the location at which to create it
     */
    public Egg(Location location) {
        this(location.getWorld(), location.x, location.y, location.z);
    }

    public Egg(ChickenEgg egg) {
        super(egg);
    }

    @Override
    public EntityEgg getEntity() {
        return (EntityEgg) super.getEntity();
    }

    @Override
    public ChickenEgg getRecodeHandle() {
        return (ChickenEgg) super.getRecodeHandle();
    }
}
