package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.entity.vehicle.CanaryBoat;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/**
 * Boat - Used for manipulating boats
 *
 * @author James
 */
@RecodeProxy(net.canarymod.api.entity.vehicle.Boat.class)
public class Boat extends BaseVehicle {

    /**
     * Interface for boats.
     *
     * @param boat
     */
    public Boat(net.minecraft.entity.item.EntityBoat boat) {
        super(boat);
    }

    /**
     * Create a new Boat at the given position.
     *
     * @param x
     * @param y
     * @param z
     * @deprecated Use {@link #Boat(World, double, double, double)} instead.
     */
    @Deprecated
    public Boat(double x, double y, double z) {
        this(etc.getServer().getDefaultWorld(), x, y, z);
    }

    /**
     * Create a new Boat at the given position.
     *
     * @param world The world for the new boat
     * @param x The x coordinate for the new boat
     * @param y The y coordinate for the new boat
     * @param z The z coordinate for the new boat
     */
    public Boat(World world, double x, double y, double z) {
        super(new net.minecraft.entity.item.EntityBoat(world.getWorld(), x, y, z));
        this.spawn();
    }

    public Boat(net.canarymod.api.entity.vehicle.Boat boat) {
        super(boat);
    }

    /**
     * Returns the entity we're wrapping.
     *
     * @return
     */
    @Override
    public net.minecraft.entity.item.EntityBoat getEntity() {
        return ((CanaryBoat) entity).getHandle();
    }

    @Override
    public net.canarymod.api.entity.vehicle.Boat getRecodeHandle() {
        return (net.canarymod.api.entity.vehicle.Boat) super.getRecodeHandle();
    }

    public static Boat[] fromRecodeArray(net.canarymod.api.entity.vehicle.Boat[] recode) {
        Boat[] classic = new Boat[recode.length];
        for (int i = 0; i < recode.length; i++) {
            classic[i] = Wrapper.wrap(recode[i]);
        }
        return classic;
    }

    public static net.canarymod.api.entity.vehicle.Boat[] toRecodeArray(Boat[] classic) {
        net.canarymod.api.entity.vehicle.Boat[] recode = new net.canarymod.api.entity.vehicle.Boat[classic.length];
        for (int i = 0; i < classic.length; i++) {
            recode[i] = classic[i].getRecodeHandle();
        }
        return recode;
    }
}
