package com.minecraftonline.classicloader.classicapi;

import net.canarymod.Canary;
import net.canarymod.api.nbt.FloatTag;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/**
 * Interface for the net.minecraft.server.NBTTagFloat class
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(FloatTag.class)
public class NBTTagFloat extends NBTBase {

    /**
     * Creates a wrapper around an net.minecraft.server.NBTTagFloat
     *
     * @param baseTag the tag to wrap
     */
    public NBTTagFloat(net.minecraft.nbt.NBTTagFloat baseTag) {
        super(baseTag);
    }

    /**
     * Creates a new NBTTagFloat
     *
     * @param name the name of the tag
     */
    public NBTTagFloat(String name) {
        this(name, 0);
    }

    /**
     * Creates a new NBTTagFloat
     *
     * @param name the name of the tag
     * @param value the value of the tag
     */
    public NBTTagFloat(String name, float value) {
        this(Canary.factory().getNBTFactory().newFloatTag(value));
    }

    public NBTTagFloat(FloatTag floatTag) {
        super(floatTag);
    }

    @Override
    public net.minecraft.nbt.NBTTagFloat getBaseTag() {
        return (net.minecraft.nbt.NBTTagFloat) super.getBaseTag();
    }

    /**
     * Returns the value of this tag
     *
     * @return
     */
    public float getValue() {
        return getRecodeHandle().getValue();
    }

    /**
     * Sets the value of this tag
     *
     * @param value the new value
     */
    public void setValue(float value) {
        getRecodeHandle().setValue(value);
    }

    @Override
    public String toString() {
        return String.format("NBTTag[type=%s, name=%s, value=%f]", getTagName(getType()), getName(), getValue());
    }

    @Override
    public FloatTag getRecodeHandle() {
        return (FloatTag) super.getRecodeHandle();
    }

    public static NBTTagFloat[] fromRecodeArray(FloatTag[] recode) {
        NBTTagFloat[] classic = new NBTTagFloat[recode.length];
        for (int i = 0; i < recode.length; i++) {
            classic[i] = new NBTTagFloat(recode[i]);
        }
        return classic;
    }

    public static FloatTag[] toRecodeArray(NBTTagFloat[] classic) {
        FloatTag[] recode = new FloatTag[classic.length];
        for (int i = 0; i < classic.length; i++) {
            recode[i] = classic[i].getRecodeHandle();
        }
        return recode;
    }
}
