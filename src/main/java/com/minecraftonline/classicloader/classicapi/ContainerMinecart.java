package com.minecraftonline.classicloader.classicapi;

import lombok.Delegate;
import com.minecraftonline.classicloader.classicapi.bridge.ContainerProxy;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.item.EntityMinecartContainer;

@RecodeProxy(net.canarymod.api.entity.vehicle.ContainerMinecart.class)
public abstract class ContainerMinecart extends Minecart implements Inventory {

    private final @Delegate(types=Inventory.class) ItemArray<ContainerProxy> ia;

    public ContainerMinecart(EntityMinecartContainer o) {
        super(o);
    }

    public ContainerMinecart(net.canarymod.api.entity.vehicle.ContainerMinecart cart) {
        super(cart);
    }

    {
        this.ia = new ItemArray<ContainerProxy>(ContainerProxy.of(getRecodeHandle())) {
            @Override
            public void update() {
                getRecodeHandle().update();
            }

            @Override
            public void setName(String value) {
                container.setName(value);
            }

            @Override
            public String getName() {
                return container.getName();
            }
        };
    }

    @Override
    public EntityMinecartContainer getEntity() {
        return (EntityMinecartContainer) super.getEntity();
    }

    @Override
    public net.canarymod.api.entity.vehicle.ContainerMinecart getRecodeHandle() {
        return (net.canarymod.api.entity.vehicle.ContainerMinecart) super.getRecodeHandle();
    }
}
