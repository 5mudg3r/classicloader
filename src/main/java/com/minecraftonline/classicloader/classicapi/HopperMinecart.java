package com.minecraftonline.classicloader.classicapi;

import net.canarymod.Canary;
import net.canarymod.api.entity.EntityType;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.item.EntityMinecartHopper;

@RecodeProxy(net.canarymod.api.entity.vehicle.HopperMinecart.class)
public class HopperMinecart extends ContainerMinecart implements Hopper {

    HopperMinecart(EntityMinecartHopper o) {
        super(o);
    }

    public HopperMinecart(net.canarymod.api.entity.vehicle.HopperMinecart cart) {
        super(cart);
    }

    /**
     * Create a new Hopper Minecart with the given position. Call
     * {@link #spawn()} to spawn it in the world.
     *
     * @param world The world for the new minecart
     * @param x The x coordinate for the new minecart
     * @param y The y coordinate for the new minecart
     * @param z The z coordinate for the new minecart
     */
    public HopperMinecart(World world, double x, double y, double z) {
        this((net.canarymod.api.entity.vehicle.HopperMinecart) Canary.factory().getEntityFactory()
                .newVehicle(EntityType.HOPPERMINECART,
                            new net.canarymod.api.world.position.Location(world.getRecodeHandle(), x, y, z, 0, 0)));
    }

    @Override
    public double getPosX() {
        return this.getX();
    }

    @Override
    public double getPosY() {
        return this.getY();
    }

    @Override
    public double getPosZ() {
        return this.getZ();
    }

    @Override
    public int getTranferCooldown() {
        return getRecodeHandle().getTranferCooldown();
    }

    @Override
    public void setTransferCooldown(int cooldown) {
        getRecodeHandle().setTransferCooldown(cooldown);
    }

    @Override
    public EntityMinecartHopper getEntity() {
        return (EntityMinecartHopper) super.getEntity();
    }

    /**
     * Get the blocked state of this hopper minecart. Blockages normally happen
     * with activator rails.
     *
     * @return <tt>true</tt> if this hopper minecart is blocked, <tt>false</tt>
     * otherwise
     */
    public boolean isBlocked() {
        return getRecodeHandle().isBlocked();
    }

    /**
     * Set the blocked state of this hopper minecart. Blockages normally happen
     * with activator rails.
     *
     * @param blocked <tt>true</tt> if this hopper minecart should be blocked,
     * <tt>false</tt> otherwise.
     */
    public void setBlocked(boolean blocked) {
        // SRG this.getEntity().func_96110_f(blocked);
        this.getEntity().f(blocked);
    }

    @Override
    public net.canarymod.api.entity.vehicle.HopperMinecart getRecodeHandle() {
        return (net.canarymod.api.entity.vehicle.HopperMinecart) super.getRecodeHandle();
    }
}
