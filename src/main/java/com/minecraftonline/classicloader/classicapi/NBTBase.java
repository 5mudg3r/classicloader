package com.minecraftonline.classicloader.classicapi;

import java.lang.reflect.Method;
import lombok.Lombok;
import net.canarymod.api.nbt.*;
import com.minecraftonline.classicloader.Util;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import lombok.NonNull;

/**
 * Interface for the net.minecraft.server.NBTBase class
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(BaseTag.class)
public class NBTBase {

    private final BaseTag baseTag;

    /**
     * Creates a wrapper around an net.minecraft.server.NBTBase.
     *
     * @param baseTag the net.minecraft.server.NBTBase tag to wrap
     */
    public NBTBase(net.minecraft.nbt.NBTBase baseTag) {
        try {
            Method wrapMethod = CanaryBaseTag.class.getDeclaredMethod("wrap", net.minecraft.nbt.NBTBase.class);
            wrapMethod.setAccessible(true);
            this.baseTag = (BaseTag) wrapMethod.invoke(null, baseTag);
        } catch (Exception e) {
            throw Lombok.sneakyThrow(e);
        }
    }

    public NBTBase(@NonNull BaseTag baseTag) {
        this.baseTag = baseTag;
    }

    /**
     * Returns the name of this tag.
     *
     * @return String
     */
    public String getName() {
        return "";
    }

    /**
     * Sets the name of this tag.
     *
     * @param name the new name for the tag
     */
    public void setName(String name) {
    }

    /**
     * Returns the net.minecraft.server.NBTBase this wraps.
     *
     * @return net.minecraft.server.NBTBase
     */
    public net.minecraft.nbt.NBTBase getBaseTag() {
        try {
            Method getHandleMethod = CanaryBaseTag.class.getDeclaredMethod("getHandle");
            getHandleMethod.setAccessible(true);
            return (net.minecraft.nbt.NBTBase) getHandleMethod.invoke(this.baseTag, new Object[0]);
        } catch (Exception e) {
            throw Lombok.sneakyThrow(e);
        }
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof NBTBase) {
            return getBaseTag().equals(((NBTBase) object).getBaseTag());
        } else if (object instanceof net.minecraft.nbt.NBTBase) { // EWW. Who wrote this >:(
            return getBaseTag().equals((net.minecraft.nbt.NBTBase) object);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.getBaseTag() != null ? this.getBaseTag().hashCode() : 0);
        return hash;
    }

    /**
     * Returns the type of tag this is.
     *
     * @return byte
     */
    public byte getType() {
        return getRecodeHandle().getTypeId();
    }

    @Override
    public String toString() {
        return String.format("NBTTag[type=%s]", getTagName(getType()));
    }

    /**
     * Returns the name of the type of tag this is.
     *
     * @param type
     * @return String
     */
    public static String getTagName(byte type) {
        switch (type) {
            case 0:
                return "TAG_End";

            case 1:
                return "TAG_Byte";

            case 2:
                return "TAG_Short";

            case 3:
                return "TAG_Int";

            case 4:
                return "TAG_Long";

            case 5:
                return "TAG_Float";

            case 6:
                return "TAG_Double";

            case 7:
                return "TAG_Byte_Array";

            case 8:
                return "TAG_String";

            case 9:
                return "TAG_List";

            case 10:
                return "TAG_Compound";

            case 11:
                return "TAG_Int_Array";

            case 99:
                return "Any Numeric Tag";

            default:
                return "UNKNOWN";
        }
    }

    /**
     * Wraps an NBTTag in its proper wrapper.
     *
     * @param tag The tag to wrap
     * @return NBTBase
     */
    @SuppressWarnings("unchecked")
    public static NBTBase wrap(net.minecraft.nbt.NBTBase tag) {
        try {
            BaseTag baseTag = Util.callMethod(CanaryBaseTag.class, null, "wrap", net.minecraft.nbt.NBTBase.class, tag, BaseTag.class);
            if (baseTag == null) {
                return null;
            }
            switch (baseTag.getTypeId()) {
                case 1:
                    return Wrapper.wrap((ByteTag) baseTag);
                case 2:
                    return Wrapper.wrap((ShortTag) baseTag);
                case 3:
                    return Wrapper.wrap((IntTag) baseTag);
                case 4:
                    return Wrapper.wrap((LongTag) baseTag);
                case 5:
                    return Wrapper.wrap((FloatTag) baseTag);
                case 6:
                    return Wrapper.wrap((DoubleTag) baseTag);
                case 7:
                    return Wrapper.wrap((ByteArrayTag) baseTag);
                case 8:
                    return Wrapper.wrap((StringTag) baseTag);
                case 9:
                    return Wrapper.wrap((ListTag<BaseTag>) baseTag);
                case 10:
                    return Wrapper.wrap((CompoundTag) baseTag);
                case 11:
                    return Wrapper.wrap((IntArrayTag) baseTag);
                default:
                    return Wrapper.wrap(baseTag);
            }
        } catch (Exception e) {
            throw Lombok.sneakyThrow(e);
        }
    }

    public String toPlainString() {
        return getName();
    }

    public BaseTag getRecodeHandle() {
        return this.baseTag;
    }

    public static NBTBase[] fromRecodeArray(BaseTag[] recode) {
        NBTBase[] classic = new NBTBase[recode.length];
        for (int i = 0; i < recode.length; i++) {
            classic[i] = new NBTBase(recode[i]);
        }
        return classic;
    }

    public static BaseTag[] toRecodeArray(NBTBase[] classic) {
        BaseTag[] recode = new BaseTag[classic.length];
        for (int i = 0; i < recode.length; i++) {
            recode[i] = classic[i].getRecodeHandle();
        }
        return recode;
    }
}
