package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.CanaryDamageSource;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.util.EntityDamageSource;

public enum DamageType {

    /**
     * Creeper explosion
     */
    // SRG CREEPER_EXPLOSION(new EntityDamageSource("explosion.player", new EntityCreeper(null)).func_76351_m().func_94540_d().getCanaryDamageSource()), //
    CREEPER_EXPLOSION(new EntityDamageSource("explosion.player", new EntityCreeper(null)).q().d().getCanaryDamageSource()), //
    /**
     * Damage dealt by another entity
     */
    // SRG ENTITY(net.minecraft.util.DamageSource.func_76358_a((EntityLivingBase) null).getCanaryDamageSource()), //
    ENTITY(net.minecraft.util.DamageSource.a((EntityLivingBase) null).getCanaryDamageSource()), //
    /**
     * Damage caused by explosion
     */
    EXPLOSION(CanaryDamageSource.getDamageSourceFromType(net.canarymod.api.DamageType.EXPLOSION)), //
    /**
     * Damage caused from falling (fall distance - 3.0)
     */
    // SRG FALL(net.minecraft.util.DamageSource.field_76379_h.getCanaryDamageSource()), //
    FALL(net.minecraft.util.DamageSource.h.getCanaryDamageSource()), //
    /**
     * Damage caused by fire (1)
     */
    // SRG FIRE(net.minecraft.util.DamageSource.field_76372_a.getCanaryDamageSource()), //
    FIRE(net.minecraft.util.DamageSource.a.getCanaryDamageSource()), //
    /**
     * Low periodic damage caused by burning (1)
     */
    // SRG FIRE_TICK(net.minecraft.util.DamageSource.field_76370_b.getCanaryDamageSource()), //
    FIRE_TICK(net.minecraft.util.DamageSource.b.getCanaryDamageSource()), //
    /**
     * Damage caused from lava (4)
     */
    // SRG LAVA(net.minecraft.util.DamageSource.field_76371_c.getCanaryDamageSource()), //
    LAVA(net.minecraft.util.DamageSource.c.getCanaryDamageSource()), //
    /**
     * Damage caused from drowning (2)
     */
    // SRG WATER(net.minecraft.util.DamageSource.field_76369_e.getCanaryDamageSource()), //
    WATER(net.minecraft.util.DamageSource.e.getCanaryDamageSource()), //
    /**
     * Damage caused by cactus (1)
     */
    // SRG CACTUS(net.minecraft.util.DamageSource.field_76367_g.getCanaryDamageSource()), //
    CACTUS(net.minecraft.util.DamageSource.g.getCanaryDamageSource()), //
    /**
     * Damage caused by suffocating(1)
     */
    // SRG SUFFOCATION(net.minecraft.util.DamageSource.field_76368_d.getCanaryDamageSource()), //
    SUFFOCATION(net.minecraft.util.DamageSource.d.getCanaryDamageSource()), //
    /**
     * Damage caused by lightning (5)
     */
    // SRG LIGHTNING(net.minecraft.util.DamageSource.field_76372_a.getCanaryDamageSource()), //
    LIGHTNING(net.minecraft.util.DamageSource.a.getCanaryDamageSource()), //
    /**
     * Damage caused by starvation (1)
     */
    // SRG STARVATION(net.minecraft.util.DamageSource.field_76366_f.getCanaryDamageSource()), //
    STARVATION(net.minecraft.util.DamageSource.f.getCanaryDamageSource()), //
    /**
     * Damage caused by poison (1) (Potions, Poison)
     */
    // SRG POTION(net.minecraft.util.DamageSource.field_76376_m.getCanaryDamageSource()), //
    POTION(net.minecraft.util.DamageSource.k.getCanaryDamageSource()), //
    /**
     * Damage caused by the "Wither" effect (1)
     */
    // SRG WITHER(net.minecraft.util.DamageSource.field_82727_n.getCanaryDamageSource()), //
    WITHER(net.minecraft.util.DamageSource.l.getCanaryDamageSource()), //
    /**
     * Damage caused by throwing an enderpearl (5)
     */
    // SRG ENDERPEARL(net.minecraft.util.DamageSource.field_76379_h.getCanaryDamageSource()), //
    ENDERPEARL(net.minecraft.util.DamageSource.h.getCanaryDamageSource()), //
    /**
     * Damage caused by falling anvil
     */
    // SRG ANVIL(net.minecraft.util.DamageSource.field_82728_o.getCanaryDamageSource()), //
    ANVIL(net.minecraft.util.DamageSource.m.getCanaryDamageSource()), //
    /**
     * Damage caused by falling block
     */
    // SRG FALLING_BLOCK(net.minecraft.util.DamageSource.field_82729_p.getCanaryDamageSource()), //
    FALLING_BLOCK(net.minecraft.util.DamageSource.n.getCanaryDamageSource()), //
    /**
     * Generic damage cause
     */
    // SRG GENERIC(net.minecraft.util.DamageSource.field_76377_j.getCanaryDamageSource());
    GENERIC(net.minecraft.util.DamageSource.j.getCanaryDamageSource());
    private final DamageSource source;

    private DamageType(net.canarymod.api.DamageSource source) {
        this.source = Wrapper.wrap(source);
    }

    public DamageSource getDamageSource() {
        return this.source;
    }

    /**
     * Returns the message that would be displayed in chat if a player died from
     * this.
     *
     * @param died The player who 'died'.
     * @return The death message.
     */
    public String getDeathMessage(Player died) {
        return source.getDeathMessage(died);
    }

    /**
     * Get <tt>DamageType</tt> from a given {@link DamageSource}.
     *
     * @param source The {@link DamageSource} to get the <tt>DamageType</tt>
     * for.
     * @return The <tt>DamageType</tt> corresponding to <tt>source</tt>
     */
    public static DamageType fromDamageSource(DamageSource source) {
        for (DamageType t : DamageType.values()) {
            if (t.getDamageSource().getName().equals(source.getName())) {
                return t;
            }
        }

        if (source.isEntityDamageSource()) {
            return ENTITY;
        }

        return GENERIC;
    }
}
