/*
 * Copyright (C) 2014 Willem Mulder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.minecraftonline.classicloader;

import com.minecraftonline.classicloader.classicapi.MessageReceiver;
import com.minecraftonline.classicloader.classicapi.PlayerCommands;
import com.minecraftonline.classicloader.classicapi.ServerConsoleCommands;
import com.minecraftonline.classicloader.classicapi.Wrapper;
import net.canarymod.hook.HookHandler;
import net.canarymod.hook.command.ConsoleCommandHook;
import net.canarymod.hook.command.PlayerCommandHook;
import net.canarymod.plugin.PluginListener;

/**
 * Pass commands through classic's new school command handler
 * @author Willem Mulder
 */
public class ClassicCommandListener implements PluginListener {

    private static MessageReceiver wrapMessageReceiver(final net.canarymod.chat.MessageReceiver receiver) {
        return new MessageReceiver() {
            @Override
            public String getName() {
                return receiver.getName();
            }

            @Override
            public void notify(String message) {
                receiver.notice(message);
            }
        };
    }

    @HookHandler(ignoreCanceled = true)
    public void handleServerCommand(final ConsoleCommandHook event) {
        // Strip leading slash
        if (event.getCommand()[0].startsWith("/")) {
            event.getCommand()[0] = event.getCommand()[0].substring(1);
        }
        if (ServerConsoleCommands.parseServerConsoleCommand(wrapMessageReceiver(event.getCaller()),
                                                            event.getCommand()[0], event.getCommand())) {
            event.setCanceled();
        }
    }

    @HookHandler(ignoreCanceled = true)
    public void handlePlayerCommand(final PlayerCommandHook event) {
        // Let it pass through the classic permission hook system, just to be sure
        if (!Wrapper.wrap(event.getPlayer()).canUseCommand(event.getCommand()[0])) {
            return;
        }

        if (PlayerCommands.parsePlayerCommand(Wrapper.wrap(event.getPlayer()),
                                              event.getCommand()[0].substring(1), event.getCommand())
                || ServerConsoleCommands.parseServerConsoleCommand(Wrapper.wrap(event.getPlayer()),
                                                                   event.getCommand()[0].substring(1),
                                                                   event.getCommand())) {
            event.setCanceled();
        }
    }

}
