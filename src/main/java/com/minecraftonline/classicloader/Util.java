/*
 * Copyright (C) 2013 Willem Mulder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.classicloader;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import lombok.Lombok;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.message.MessageFormatMessageFactory;

/**
 *
 * @author Willem Mulder
 */
public class Util {

    private static final Logger log = LogManager.getLogger(new MessageFormatMessageFactory());

    private Util() {
        throw new IllegalStateException("Utility class is not meant to be instantiated");
    }

    @SuppressWarnings("unchecked")
    public static <T> T getField(Class<?> defining, Object instance, String fieldName, Class<T> returnType) {
        Field f;
        try {
            f = defining.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            log.error("NoSuchFieldException while getting field - is this plugin outdated?", e);
            throw Lombok.sneakyThrow(e);
        }
        f.setAccessible(true);
        try {
            Object toRet = f.get(instance);
            if (toRet != null && !returnType.isAssignableFrom(toRet.getClass())) {
                throw new ClassCastException(java.text.MessageFormat.format("{0} is not assignable from {1}", returnType, toRet.getClass()));
            }
            return (T) toRet;
        } catch (IllegalAccessException e) {
            throw Lombok.sneakyThrow(e);
        } catch (ClassCastException e) {
            log.error("ClassCastException while getting field - is this plugin outdated?", e);
            throw Lombok.sneakyThrow(e);
        }
    }

    public static <T> void setField(Class<?> defining, Object instance, String fieldName, T value) {
        Util.<T>setField(defining, instance, fieldName, value, false);
    }

    public static <T> void setField(Class<?> defining, Object instance, String fieldName, T value, boolean overrideFinal) {
        Field f;
        try {
            f = defining.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            log.error("NoSuchFieldException while setting field - is this plugin outdated?", e);
            throw Lombok.sneakyThrow(e);
        }
        f.setAccessible(true);
        if (overrideFinal && (f.getModifiers() & Modifier.FINAL) != 0) {
            try {
                Field modifiers = Field.class.getDeclaredField("modifiers");
                modifiers.setAccessible(true);
                modifiers.set(f, f.getModifiers() & ~Modifier.FINAL);
            } catch (IllegalAccessException e) {
                throw Lombok.sneakyThrow(e);
            } catch (NoSuchFieldException e) {
                log.error("NoSuchFieldException - Only the Oracle and OpenJDK JVMs are supported!");
                throw Lombok.sneakyThrow(e);
            }
        }
        try {
            if (!f.getType().isAssignableFrom(value.getClass()) && !f.getType().isPrimitive() || !isWrapperOf(value.getClass(), f.getType())) {
                throw new ClassCastException(java.text.MessageFormat.format("{0} is not assignable from {1}", f.getType(), value.getClass()));
            }
            f.set(instance, value);
        } catch (IllegalAccessException e) {
            throw Lombok.sneakyThrow(e);
        } catch (ClassCastException e) {
            log.error("ClassCastException while getting field - is this plugin outdated?", e);
            throw Lombok.sneakyThrow(e);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T callMethod(Class<?> defining, Object instance, String methodName, Class<?>[] argTypes, Object[] args, Class<T> returnType) {
        Method m;
        try {
            m = defining.getDeclaredMethod(methodName, argTypes);
        } catch (NoSuchMethodException e) {
            log.error("NoSuchMethodException while calling method - is this plugin outdated?", e);
            throw Lombok.sneakyThrow(e);
        }
        m.setAccessible(true);
        try {
            Object toRet = m.invoke(instance, args);
            if (toRet != null && !returnType.isAssignableFrom(toRet.getClass())) {
                throw new ClassCastException(java.text.MessageFormat.format("{0} is not assignable from {1}", returnType, toRet.getClass()));
            }
            return (T) toRet;
        } catch (IllegalAccessException e) {
            throw Lombok.sneakyThrow(e);
        } catch (InvocationTargetException e) {
            throw Lombok.sneakyThrow(e);
        } catch (ClassCastException e) {
            log.error("ClassCastException while invoking method - is this plugin outdated?", e);
            throw Lombok.sneakyThrow(e);
        }
    }

    public static <T> T callMethod(Class<?> defining, Object instance, String methodName, Class<?> singleArgType, Object singleArg, Class<T> returnType) {
        return Util.<T>callMethod(defining, instance, methodName, new Class<?>[] {singleArgType}, new Object[] {singleArg}, returnType);
    }

    public static boolean calledFromFileLine(String file, int line) {
        for (StackTraceElement el : Thread.currentThread().getStackTrace()) {
            if (el.getFileName().equals(file) && el.getLineNumber() == line) {
                return true;
            }
        }
        return false;
    }

    public static boolean isWrapperOf(Class<? extends Object> wrapper, Class<?> primitive) {
        return primitive == Boolean.TYPE && Boolean.class.isAssignableFrom(wrapper)
                || primitive == Byte.TYPE && Byte.class.isAssignableFrom(wrapper)
                || primitive == Character.TYPE && Character.class.isAssignableFrom(wrapper)
                || primitive == Double.TYPE && Double.class.isAssignableFrom(wrapper)
                || primitive == Float.TYPE && Float.class.isAssignableFrom(wrapper)
                || primitive == Integer.TYPE && Integer.class.isAssignableFrom(wrapper)
                || primitive == Long.TYPE && Long.class.isAssignableFrom(wrapper)
                || primitive == Short.TYPE && Short.class.isAssignableFrom(wrapper);
    }
}
